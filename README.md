# C5

Facilitates filing and disclosing reporting requirements for out-of-state committees.

## Architecture

Aso of June 2023, the final architecture for this system is still under discussion. Current plans suggest that it will
consist of the following:

- A thin drupal module to facilitate authentication and authorization of users via drupal.
- A Quasar Vue application that provides access.
- AWS Gateway lambda application that facilitates inquiry into the backend database.
