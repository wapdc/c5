#!/usr/bin/env bash
set -e
export AWS_DEFAULT_REGION=us-west-2
export AWS_ACCESS_KEY_ID="$COMMERCIAL_AWS_ACCESS_KEY_ID"
export AWS_SECRET_ACCESS_KEY="$COMMERCIAL_AWS_SECRET_ACCESS_KEY"
export AWS_PAGER=""
build_env="demo"

if [[ "$CI_COMMIT_TAG" ]] ; then
  case "$CI_COMMIT_TAG" in
    v*)
      echo "Tag build detected..."
      echo "Tag: $CI_COMMIT_TAG"
      build_env=prod
      ;;
  esac
else
  echo "Branch Build Detected"
  echo "Branch: $CI_COMMIT_REF_NAME"
fi


project_dir=`pwd`
cd aws
mkdir -p local
echo "Installing dependencies"
npm install
echo "Building aws package"
sam build
echo "Deploying AWS package"
sam deploy --no-fail-on-empty-changeset --config-env $build_env
echo "Inspecting cloud formation stack"
aws cloudformation describe-stacks --stack-name "c5-$build_env" --output json >local/registerEvent.json
echo "Register Gateway URL"
aws lambda invoke --function-name "my-pdc-${build_env}-registerGatewayUrl" --cli-binary-format raw-in-base64-out --payload file://local/registerEvent.json local/registerResult.json
echo "Done."
cd "$project_dir"