#!/usr/bin/env bash
set -e
start_dir=`pwd`;

if [[ -f "/app/bin/ci_build_environment.sh" ]] ; then
. /app/bin/ci_build_environment.sh
fi

#Build Vue UI
cd ui
echo "Building UI Application"
npm install -q --yes
npm run build

#pull copy of apollo site
pull_site apollo
cp -r $start_dir/drupal/. web/modules/custom
commit_push





