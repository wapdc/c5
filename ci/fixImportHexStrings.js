// run the JavaScript below with a local rev script file of the commented code
// then run the updates in "generated_import_fix.sql" in production
// then delete the files c5_reports.json and generated_import_fix.sql to clean up
//
// ----------- begin rev script --------------------------
//
// */usr/bin/env bash
// set -e

// # from the sql query, output a file that has the json objects
// $PSQL_CMD <<EOF
// \t
// \a
// \o c5_reports.json
// select row_to_json(t) from (select report_id, user_data from report where report_type='C5' and user_data::json->>'has_resident_contribution_import' = 'true' or user_data::json->>'has_non_resident_contribution_import' = 'true') t;
// EOF

// # use the json objects to fix the import strings and generate a sql file that has updates for the reports
// node ci/fixImportHexStrings.js
//
// ----------- end rev script --------------------------


const pako = require('../ui/node_modules/pako')

function fromHexString(hexString) {
  return Uint8Array.from(hexString.match(/.{1,2}/g).map((byte) => parseInt(byte, 16)))
}

const fs = require('fs')
fs.readFile('c5_reports.json', 'utf8', function (err, data) {
  const rows = data.split('\n').filter(row => Boolean(row)).map(row => JSON.parse(row))

  if (err) throw err

  for (let row of rows) {
    const user_data = JSON.parse(row.user_data)

    if (! user_data.has_resident_contribution_import) {
      continue
    }

    const contribution_import = user_data.resident_contribution_import

    // from hex and normal to text --------------------------------------------------

    let decoded = atob(contribution_import.compressed_csv)
    if (decoded.includes(',')) {
      // if the string found is an int8 array
      decoded = new Uint8Array([...decoded.split(',')])
    }
    else {
      // if the string found is a hex string
      decoded = fromHexString(decoded)
    }

    // pako decompress
    const decompressed = pako.ungzip(decoded)

    // as a csv
    const as_text = new TextDecoder().decode(decompressed)


    // from text to correct binary -------------------------------------------------

    // to uint8 array
    const uint8_array = new TextEncoder().encode(as_text.trim())
    // to buffer and pako compress
    const buffer = Buffer.from(pako.gzip(uint8_array))
    // encode as base64
    const encoded = buffer.toString('base64')

    user_data.resident_contribution_import = {...user_data.resident_contribution_import, compressed_csv: encoded }

    row.user_data = JSON.stringify(user_data)
  }

  for (let row of rows) {
    const user_data = JSON.parse(row.user_data)

    if (! user_data.has_non_resident_contribution_import) {
      continue
    }

    const contribution_import = user_data.non_resident_contribution_import

    // from hex and normal to text --------------------------------------------------

    let decoded = atob(contribution_import.compressed_csv)
    if (decoded.includes(',')) {
      // if the string found is an int8 array
      decoded = new Uint8Array([...decoded.split(',')])
    }
    else {
      // if the string found is a hex string
      decoded = fromHexString(decoded)
    }
    // pako decompress
    const decompressed = pako.ungzip(decoded)
    // as a csv
    const as_text = new TextDecoder().decode(decompressed)

    // from text to correct binary -------------------------------------------------

    // to uint8 array
    const uint8_array = new TextEncoder().encode(as_text.trim())
    // to buffer and pako compress
    const buffer = Buffer.from(pako.gzip(uint8_array))
    // encode as base64
    const encoded = buffer.toString('base64')

    user_data.non_resident_contribution_import = {...user_data.non_resident_contribution_import, compressed_csv: encoded }

    row.user_data = JSON.stringify(user_data)
  }

  let return_string = ''
  for (let row of rows) {
    return_string += `update report set user_data = '${row.user_data.replaceAll("'", "''")}' where report_id = ${row.report_id};`
    return_string += '\n'
  }

  fs.writeFile('generated_import_fix.sql', return_string, function(err, data) {
    if (err) {
      return console.error(err);
    }
  })

})
