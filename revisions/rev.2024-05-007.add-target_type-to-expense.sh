#!/usr/bin/env bash
set -e
$PSQL_CMD <<EOF
  alter table c5_expense
  drop column if exists resident,
  add column if not exists target_type text,
  add column if not exists target_id int
  ;
EOF
