#!/usr/bin/env bash
set -e
$PSQL_CMD <<EOF
  alter table c5_expense
  add column inkind boolean default false,
  add column description text,
  add column vendor_name text,
  add column vendor_address text,
  add column vendor_city text,
  add column vendor_state text,
  add column vendor_postcode text
  ;
EOF
