set -e
$PSQL_CMD <<EOF
-- Insert link to the c5 in my-pdc admin screen
insert into menu_links(menu, category, title, abstract, url, permission)
  values('staff', 'Campaign finance', 'C5 Data Entry', 'Tool for data entry of scanned C5 reports.', '/c5/committees', 'enter wapdc data');
EOF