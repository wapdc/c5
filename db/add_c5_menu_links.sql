-- Public menu box for c5
INSERT INTO menu_links (category, menu, title, abstract, action, url, permission)
values (null, 'filer', 'Out-of-state-committees', 'Register as out-of-state committee and file out-of-state committee reports', 'Go', '/c5', 'access wapdc data');

-- Admin menu link for c5
INSERT INTO menu_links (category, menu, title, abstract, action, url, permission)
values ('Campaign finance', 'staff', 'C5 Out-of-state-committees', 'Administer out-of-state committee profiles and reports', 'Go', '/c5', 'access wapdc data');

