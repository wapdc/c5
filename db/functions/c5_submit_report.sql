drop function if exists c5_submit_report(p_report json, p_user_name text, p_committee_id int, p_report_id int);
create or replace function c5_submit_report(p_report json, p_user_name text, p_committee_id int, p_report_id int, latest_submitted_at_is_admin bool) returns int
    language plpgsql as
$$
DECLARE
    v_committee_id int := p_committee_id;
    v_fund_id int;
    v_report_id int := p_report_id;
    v_registration_id int;
    reporting_periods record;
    v_latest_report record;
BEGIN
    -- When developing this is was determined that we would not create amend functionality on the data entry side, so no need to keep a history.
    if p_report_id is not null then
        perform c5_delete_report(p_report_id, p_user_name);
        select committee_id into v_committee_id from committee where committee_id = p_committee_id;
    end if;

    if v_committee_id is null then
    -- If no committee was passed in create the committee and return the registration_id
    select c5_create_committee_registration(p_report, p_user_name) into v_registration_id;
    select committee_id into v_committee_id from registration where registration_id = v_registration_id;
    end if;

    -- Existing committee, new report case

    if latest_submitted_at_is_admin = true then
        if v_registration_id is null then
            -- create registration record for new report
            -- admin data will be updated at the end
            insert into registration(committee_id, name, source, verified, submitted_at, updated_at)
            values (
                    v_committee_id,
                    p_report->'committee'->>'name',
                    'c5',
                    true,
                    (p_report->>'submitted_at')::timestamptz,
                    now()
                )
            returning registration_id into v_registration_id;
        else
            update registration set
            name = p_report->'committee'->>'name',
            updated_at = now() where committee_id = v_committee_id;
        end if;
    else
        insert into registration(committee_id, name, source, verified, submitted_at, updated_at)
        values (
                v_committee_id,
                p_report->'committee'->>'name',
                'c5',
                true,
                (p_report->>'submitted_at')::timestamptz,
                now()
            )
        returning registration_id into v_registration_id;
    end if;

    -- grab an existing fund or create one
    select c5_get_committee_fund(v_committee_id, p_report->>'election_year') into v_fund_id;

    -- create report
    insert into report(
      report_id, report_type, submitted_at, fund_id, form_type, election_year,
      filer_name, user_data, version, metadata, source, ax_legacy_docid, filing_method,
      address1, city, state, postcode, treasurer_name
    )
    values (
             coalesce(v_report_id, nextval('report_report_id_seq')),
             'C5',
             (p_report->>'submitted_at')::timestamptz,
             v_fund_id,
             'C5',
             p_report->>'election_year',
             p_report->'committee'->>'name',
             p_report->>'user_data',
             p_report->>'version',
             p_report->>'metadata',
             'PDC',
             p_report->>'ax_legacy_docid',
             'P',
             p_report->'committee'->>'address',
             p_report->'committee'->>'city',
             p_report->'committee'->>'state',
             p_report->'committee'->>'postcode',
             p_report->>'treasurer_name'
           )
    returning report_id into v_report_id;

    -- repno assigned after report_id created
    update report set repno = v_report_id where report_id = v_report_id;

    -- insert candidate c5_expenses
    insert into c5_expense (c5_expense_id, report_id, type, expense_date, amount, name, address, city, state, postcode,
       email, office, party, ballot_number, for_or_against, purpose)
    select nextval('c5_expense_c5_expense_id_seq') as c5_expense_id,
      v_report_id as report_id, 'CA' as type, j.expense_date, j.amount, j.name, null as address, null as city, null as state,
      null as postcode, null as email, j.office, j.party::numeric, null as ballot_number, null as for_or_against, null as purpose
    from json_populate_recordset(null::c5_expense, (p_report->'expenses'->>'candidate')::json) j
    where j.amount is not null;

    -- insert ballot c5_expenses
    insert into c5_expense (c5_expense_id, report_id, type, expense_date, amount, name, address, city, state, postcode,
                            email, office, party, ballot_number, for_or_against, purpose)
    select nextval('c5_expense_c5_expense_id_seq') as c5_expense_id,
      v_report_id as report_id, 'PROP' as type, j.expense_date, j.amount, j.name, j.address, j.city, j.state, j.postcode,
      null as email, null as office, null as party, j.ballot_number, j.for_or_against, null as purpose
    from json_populate_recordset(null::c5_expense, (p_report->'expenses'->>'ballot')::json) j
    where j.amount is not null;

    -- insert other c5_expenses
    insert into c5_expense (c5_expense_id, report_id, type, expense_date, amount, name, address, city, state, postcode,
                            email, office, party, ballot_number, for_or_against, purpose)
    select nextval('c5_expense_c5_expense_id_seq') as c5_expense_id,
      v_report_id as report_id, 'OTH' as type, j.expense_date, j.amount, j.name, j.address, j.city, j.state, j.postcode,
      null as email, null as office, null as party, null as ballot_number, j.for_or_against, j.purpose
    from json_populate_recordset(null::c5_expense, (p_report->'expenses'->>'other')::json) j
    where j.amount is not null;

    -- update reporting periods
    select into reporting_periods
      case when min(e.expense_date) is not null
        then date_trunc('month', min(e.expense_date))::date
        end as period_start,
      case when max(e.expense_date) is not null
        then (date_trunc('month', max(e.expense_date)) + interval '1 month - 1 day')::date
        end as period_end
    from c5_expense e
    where e.report_id = v_report_id;

    update report
    set period_start = reporting_periods.period_start,
        period_end   = reporting_periods.period_end
    where report_id  = v_report_id;

    -- write to the committee_log about the creation or edit
    if p_report_id is null then
        insert into committee_log(committee_id, transaction_type, transaction_id, action, message, user_name, updated_at)
        values (
                    v_committee_id,
                    'committee',
                    v_registration_id,
                    'create c5 report',
                    'created c5 report number ' || v_report_id || ' for committee name ' || (p_report->'committee'->>'name'),
                    p_user_name,
                    now()
                );
    else
        insert into committee_log(committee_id, transaction_type, transaction_id, action, message, user_name, updated_at)
        values (
                   v_committee_id,
                   'committee',
                   v_registration_id,
                   'edit c5 report',
                   'edited c5 report number ' || v_report_id || ' for committee name ' || (p_report->'committee'->>'name'),
                   p_user_name,
                   now()
               );
    end if;

    --grab the latest report
    select r.* into v_latest_report from report r join fund f on f.fund_id = r.fund_id
    where f.committee_id = p_committee_id and r.submitted_at =
                                               (select max(r.submitted_at) from report r join fund f on f.fund_id = r.fund_id and f.committee_id = p_committee_id);

    --check if submitted report is later than latest report
    if (p_report->>'submitted_at')::timestamptz >= v_latest_report.submitted_at::timestamptz then
        -- update entity record
        update entity
        set name = p_report->'committee'->>'name',
            address = p_report->'committee'->>'address',
            city = p_report->'committee'->>'city',
            state = p_report->'committee'->>'state',
            postcode = p_report->'committee'->>'postcode',
            updated_at = now()
        where entity_id = (select person_id from committee where committee_id = v_committee_id);


        if latest_submitted_at_is_admin = false then
            update committee
            set name = p_report->'committee'->>'name',
                registration_id = v_registration_id,
                updated_at = now()
            where committee_id = v_committee_id;
        end if;

        --clear out the committee_contact table
        delete from committee_contact where committee_id = v_committee_id and role = 'officer';

        -- insert the committee_contact
        update committee_contact
        set email       = p_report->'committee'->>'email',
            name        = p_report->'committee'->>'name',
            address     = p_report->'committee'->>'address',
            city        = p_report->'committee'->>'city',
            state       = p_report->'committee'->>'state',
            postcode    = p_report->'committee'->>'postcode'
        where committee_id = v_committee_id;

        -- insert the officers
        insert into committee_contact(contact_id, committee_id, role, email, name, address, city, state, postcode, phone, title)
        select nextval('committee_contact_contact_id_seq'), v_committee_id as committee_id, 'officer' as role, j.email, j.name, j.address, j.city, j.state, j.postcode, j.phone, j.title
        from json_populate_recordset(null::committee_contact, (p_report->'admin_data'->>'officers')::json) j
        where j.name is not null;
    end if;

    -- admin_data object to contain the report_id
    update registration
    set admin_data = jsonb_set((p_report->'admin_data')::jsonb, '{report}', v_report_id::text::jsonb)
    where registration_id = v_registration_id;

    -- create logs
    insert into committee_log(committee_id, transaction_type, transaction_id, action, message, user_name, updated_at)
    values (
               v_committee_id,
               'committee',
               v_registration_id,
               'create c5 registration',
               'created c5 registration number ' || v_registration_id || ' for committee name ' || (p_report->'committee'->>'name'),
               p_user_name,
               now()
           );

    return v_report_id;
END
$$
