CREATE OR REPLACE FUNCTION c5_count_access_requests() returns integer language sql as $$
    select count(1) from filer_request
    as result
    where target_type = 'c5_committee';
$$
