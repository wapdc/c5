--drop function c5_amend_committee_registration(p_report json, p_committee_id int, claims json)
create or replace function c5_amend_committee_registration(p_report json, p_committee_id int, p_claims json) returns bool
    language plpgsql as
$$
DECLARE
    v_entity_id int;
    v_registration_id int;

BEGIN
    -- Get key values for upsert
    select person_id into v_entity_id from committee where committee_id = p_committee_id;

    -- Upsert changed entity records
    insert into entity (entity_id, name, address, city, state, postcode)
        VALUES (
            v_entity_id,
            p_report->'committee'->>'name',
            p_report->'committee'->>'address',
            p_report->'committee'->>'city',
            p_report->'committee'->>'state',
            p_report->'committee'->>'postcode')
        ON CONFLICT (entity_id)
    DO UPDATE
        SET
            name = EXCLUDED.name,
            address = EXCLUDED.address,
            city = EXCLUDED.city,
            state = EXCLUDED.state,
            postcode = EXCLUDED.postcode;

    -- Upsert changed committee data
    insert into committee (committee_id, name, person_id, election_code)
        VALUES (
            p_committee_id,
            p_report->'committee'->>'name',
            v_entity_id,
            p_report->'committee'->>'election_code')
        ON CONFLICT (committee_id) DO UPDATE
        SET
            name = EXCLUDED.name,
            election_code = EXCLUDED.election_code;

    -- INSERT committee officers. We found it's easier to just delete them all and insert new.
    delete from committee_contact where committee_contact.committee_id = p_committee_id and role = 'officer';
    delete from committee_contact where committee_contact.committee_id = p_committee_id and role = 'committee';

    insert into committee_contact (contact_id, committee_id, role, email, name, address, city, state, postcode, phone, title)
    select nextval('committee_contact_contact_id_seq'), p_committee_id as committee_id, 'officer' as role, j.email, j.name, j.address, j.city, j.state, j.postcode, j.phone, j.title
    from json_populate_recordset(null::committee_contact, (p_report->>'officers')::json) j;

    -- insert the committee_contact
    insert into committee_contact(committee_id, role, email, name, address, city, state, postcode)
    values (
               p_committee_id,
               'committee',
               p_report->'committee'->>'email',
               p_report->'committee'->>'name',
               p_report->'committee'->>'address',
               p_report->'committee'->>'city',
               p_report->'committee'->>'state',
               p_report->'committee'->>'postcode'
           );

    -- Insert new registration record
    insert into registration(committee_id, name, source, verified, submitted_at, updated_at, user_data)
    values (
               p_committee_id,
               p_report->'committee'->>'name',
               'c5',
               true,
               now(),
               now(),
               p_report::json
           )
    returning registration_id into v_registration_id;

    -- update committee with registration_id
    update committee set registration_id = v_registration_id where committee_id = p_committee_id;

    -- create logs
    insert into committee_log(committee_id, transaction_type, transaction_id, action, message, user_name, updated_at)
    values (
               p_committee_id,
               'committee',
               v_registration_id,
               'amend c5 registration',
               'created c5 registration number ' || v_registration_id || ' for committee name ' || (p_report->'committee'->>'name'),
               p_claims->>'user_name',
               now()
           );
    return true;
END
$$
