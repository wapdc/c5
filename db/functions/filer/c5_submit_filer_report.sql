-- drop function c5_submit_filer_report(p_report json, p_committee_id int, p_claims json);
create or replace function c5_submit_filer_report(p_report json, p_committee_id int, p_claims json) returns int
  language plpgsql as
$$
DECLARE
  v_committee_id int := p_committee_id;
  v_fund_id int;
  v_report_id int;
  v_registration_id int;

BEGIN
  -- Set fund
  select c5_get_committee_fund(v_committee_id, p_report->>'election_year') into v_fund_id;

  -- Get registration id
  select registration_id into v_registration_id from committee where committee_id = v_committee_id;

  -- Create report
  insert into report(
    report_id, report_type, submitted_at, fund_id, form_type, election_year,
    filer_name, user_data, version, source, filing_method,
    address1, city, state, postcode, treasurer_name, period_start, period_end
  )
  values (
    nextval('report_report_id_seq'),
    'C5',
    now(),
    v_fund_id,
    'C5',
    p_report->>'election_year',
    p_report->'registration'->'committee'->>'name',
    p_report->>'user_data',
    p_report->>'version',
    'PDC',
    'e',
    p_report->'registration'->'committee'->>'address',
    p_report->'registration'->'committee'->>'city',
    p_report->'registration'->'committee'->>'state',
    p_report->'registration'->'committee'->>'postcode',
    p_report->>'treasurer_name',
    (p_report->>'period_start')::date,
    (p_report->>'period_end')::date
  )
  returning report_id into v_report_id;

  -- Assign repno
  UPDATE report SET repno = v_report_id WHERE report_id = v_report_id;

  -- Candidate expenses
  insert into c5_expense (c5_expense_id, report_id, type, expense_date, amount, name, address, city, state, postcode, email, office, party, ballot_number, for_or_against, purpose, inkind, description, vendor_name, vendor_address, vendor_city, vendor_state, vendor_postcode, target_type, target_id)
  select nextval('c5_expense_c5_expense_id_seq') as c5_expense_id,
    v_report_id as report_id,
    'CA' as type,
    j.expense_date,
    j.amount,
    j.name,
    null as address,
    null as city,
    null as state,
    null as postcode,
    null as email,
    ca.office_code as office,
    ca.party_id::int as party,
    null as ballot_number,
    null as for_or_against,
    null as purpose,
    j.inkind,
    j.description,
    j.vendor_name,
    j.vendor_address,
    j.vendor_city,
    j.vendor_state,
    j.vendor_postcode,
    j.target_type,
    j.target_id
  from json_populate_recordset(null::c5_expense, (p_report->>'candidate_expenses')::json) j
  join candidacy ca on ca.candidacy_id = j.target_id
  where j.amount is not null;

  -- Committee expenses
  insert into c5_expense (c5_expense_id, report_id, type, expense_date, amount, name, email, office, party, ballot_number, for_or_against, purpose, inkind, description, vendor_name, vendor_address, vendor_city, vendor_state, vendor_postcode, target_type, target_id)
  select nextval('c5_expense_c5_expense_id_seq') as c5_expense_id,
    v_report_id as report_id,
    'CO' as type,
    j.expense_date,
    j.amount,
    j.name,
    null as email,
    null as office,
    null as party,
    j.ballot_number,
    j.for_or_against,
    null as purpose,
    j.inkind,
    j.description,
    j.vendor_name,
    j.vendor_address,
    j.vendor_city,
    j.vendor_state,
    j.vendor_postcode,
    j.target_type,
    j.target_id
  from json_populate_recordset(null::c5_expense, (p_report->>'committee_expenses')::json) j
  where j.amount is not null;

  -- Resident contributions
  if p_report->>'has_resident_contribution_import' is null or p_report->>'has_resident_contribution_import' = 'false' then
  insert into c5_contribution (c5_contribution_id, report_id, contribution_date, amount, aggregate_total, name, address, city, state, postcode, resident, email, employer, employer_address, employer_city, employer_state, employer_zip)
  select nextval('c5_contribution_c5_contribution_id_seq') as c5_contribution_id,
    v_report_id as report_id,
    j.contribution_date,
    j.amount,
    j.aggregate_total,
    j.name,
    j.address,
    j.city,
    j.state,
    j.postcode,
    j.resident,
    null as email,
    null as employer,
    null as employer_address,
    null as employer_city,
    null as employer_state,
    null as employer_zip
  from json_populate_recordset(null::c5_contribution, (p_report->>'resident_contributions')::json) j
  where j.amount is not null;
  end if;

  -- Non-resident contributions
  if p_report->>'has_non_resident_contribution_import' is null or p_report->>'has_non_resident_contribution_import' = 'false' then
  insert into c5_contribution (c5_contribution_id, report_id, contribution_date, amount, aggregate_total, name, address, city, state, postcode, resident, email, employer, employer_address, employer_city, employer_state, employer_zip)
  select nextval('c5_contribution_c5_contribution_id_seq') as c5_contribution_id,
    v_report_id as report_id,
    j.contribution_date,
    j.amount,
    j.aggregate_total,
    j.name,
    j.address,
    j.city,
    j.state,
    j.postcode,
    j.resident,
    j.email,
    j.employer,
    null as employer_address,
    j.employer_city,
    j.employer_state,
    j.employer_zip
  from json_populate_recordset(null::c5_contribution, (p_report->'non_resident_contributions')::json) j
  where j.amount is not null;
  end if;

  -- Logs
  insert into committee_log(committee_id, transaction_type, transaction_id, action, message, user_name, updated_at)
  values (
    v_committee_id,
    'committee',
    v_registration_id,
    'create c5 report',
    'created c5 report number ' || v_report_id || ' for committee name ' || (p_report->'registration'->'committee'->>'name'),
    p_claims->>'email',
    now()
  );

  return v_report_id;
END
$$
