create or replace function c5_merge_filer_registration(p_registration json, p_committee_id int, p_claims json, p_admin_claims json) returns bool
    language plpgsql as
$$
DECLARE
    v_entity_id int;
    v_registration_id int;

BEGIN
    -- Get key values for upsert
    select person_id into v_entity_id from committee where committee_id = p_committee_id;

    -- Upsert changed entity records
    insert into entity (entity_id, name, address, city, state, postcode)
        values (
            v_entity_id,
            p_registration->'committee'->>'name',
            p_registration->'committee'->>'address',
            p_registration->'committee'->>'city',
            p_registration->'committee'->>'state',
            p_registration->'committee'->>'postcode')
        on conflict (entity_id)
    do update
        set
            name = EXCLUDED.name,
            address = EXCLUDED.address,
            city = EXCLUDED.city,
            state = EXCLUDED.state,
            postcode = EXCLUDED.postcode;

    -- Upsert changed committee data
    insert into committee (committee_id, name, person_id, election_code)
        values (
          p_committee_id,
          p_registration->'committee'->>'name',
          v_entity_id,
          p_registration->'committee'->>'election_code')
        on conflict (committee_id) do update
        set
            name = EXCLUDED.name,
            election_code = EXCLUDED.election_code;


    -- Insert committee officers. We found it's easier to just delete them all and insert new.
    delete from wapdc.public.committee_contact where committee_contact.committee_id = p_committee_id and role = 'officer';

    insert into committee_contact (contact_id, committee_id, role, email, name, address, city, state, postcode, phone, title)
    select nextval('committee_contact_contact_id_seq'), p_committee_id as committee_id, 'officer' as role, j.email, j.name, j.address, j.city, j.state, j.postcode, j.phone, j.title
    from json_populate_recordset(null::committee_contact, (p_registration->>'officers')::json) j;

    -- Insert committee committee_contact. We found it's easier to just delete it and insert new.
    delete from wapdc.public.committee_contact where committee_contact.committee_id = p_committee_id and role = 'committee';

    insert into committee_contact(committee_id, role, email, name, address, city, state, postcode)
    values (
               p_committee_id,
               'committee',
               p_registration->'committee'->>'email',
               p_registration->'committee'->>'name',
               p_registration->'committee'->>'address',
               p_registration->'committee'->>'city',
               p_registration->'committee'->>'state',
               p_registration->'committee'->>'postcode'
           );

    -- Insert new registration record
    insert into registration(committee_id, name, source, verified, submitted_at, updated_at, user_data)
    values (
               p_committee_id,
               p_registration->'committee'->>'name',
               'c5',
               true,
               now(),
               now(),
               p_registration::json
           )
    returning registration_id into v_registration_id;

    -- Update committee with registration_id
    update committee set registration_id = v_registration_id where committee_id = p_committee_id;

    -- Give the user access
    insert into pdc_user_authorization(realm, uid, target_type, target_id)
    values (
        p_claims->>'realm',
        p_claims->>'uid',
        'committee',
        p_committee_id
    ) on conflict do nothing;

    -- Create logs
    insert into committee_log(committee_id, transaction_type, transaction_id, action, message, user_name, updated_at)
    values (
               p_committee_id,
               'committee',
               v_registration_id,
               'merge c5 registration',
               'created c5 registration number ' || v_registration_id || ' for committee name ' || (p_registration->'committee'->>'name'),
               p_admin_claims->>'user_name',
               now()
           );
    return true;
END
$$
