-- drop function if exists c5_submit_filer_registration(p_registration json, p_claims json);
create or replace function c5_submit_filer_registration(p_registration json, p_claims json, p_admin_claims json) returns int
  language plpgsql as
$$
DECLARE
  v_registration_id int;
  v_committee_id int;
  v_entity_id int;
  v_filer_id text;

BEGIN
  -- create new entity record
  insert into entity(name, email, address, city, state, postcode, is_person)
  values (
           p_registration->'committee'->>'name',
           p_registration->'committee'->>'email',
           p_registration->'committee'->>'address',
           p_registration->'committee'->>'city',
           p_registration->'committee'->>'state',
           p_registration->'committee'->>'postcode',
           false
         )
  returning entity_id into v_entity_id;

  -- create new filer_id with an E-(entity_id) then update the entity record
  v_filer_id = 'E-' || v_entity_id;
  update entity set filer_id = v_filer_id where entity_id = v_entity_id;

  -- create new committee with the entity_id
  insert into committee(name, filer_id, committee_type, pac_type, continuing, person_id, election_code, start_year)
  values (
           p_registration->'committee'->>'name',
           v_filer_id,
           'OS',
           'out-of-state',
           true,
           v_entity_id,
           to_char(now()::date, 'yyyy'),
           (to_char(now()::date, 'yyyy'))::int
         )
  returning committee_id into v_committee_id;

  -- insert the committee_contact
  insert into committee_contact(committee_id, role, email, name, address, city, state, postcode, phone, title)
  values (
           v_committee_id,
           'committee',
           p_registration->'committee'->>'email',
           p_registration->'committee'->>'name',
           p_registration->'committee'->>'address',
           p_registration->'committee'->>'city',
           p_registration->'committee'->>'state',
           p_registration->'committee'->>'postcode',
           p_registration->'committee'->>'phone',
           p_registration->'committee'->>'title'
         );

  -- insert the officers
  insert into committee_contact(contact_id, committee_id, role, email, name, address, city, state, postcode, phone, title)
  select nextval('committee_contact_contact_id_seq'),
         v_committee_id as committee_id,
         'officer' as role,
         j.email,
         j.name,
         j.address,
         j.city,
         j.state,
         j.postcode,
         j.phone,
         j.title
  from json_populate_recordset(null::committee_contact, (p_registration->>'officers')::json) j;

  -- create registration record for new report
  insert into registration(committee_id, name, source, verified, submitted_at, updated_at, user_data)
  values (
           v_committee_id,
           p_registration->'committee'->>'name',
           'c5',
           true,
           now(),
           now(),
           p_registration::json
         )
  returning registration_id into v_registration_id;

  -- update committee with registration_id
  update committee set registration_id = v_registration_id where committee_id = v_committee_id;

  -- create a fund
  insert into fund(filer_id, election_code, vendor, entity_id, target_id)
  values (
           v_filer_id,
           to_char(now()::date, 'yyyy'),
           'PDC',
           v_entity_id,
           v_committee_id
         );

  -- give the user access
  insert into pdc_user_authorization(realm, uid, target_type, target_id)
  values (
    p_claims->>'realm',
    p_claims->>'uid',
    'committee',
    v_committee_id
  );

  -- create logs
  insert into committee_log(committee_id, transaction_type, transaction_id, action, message, user_name, updated_at)
  values (
           v_committee_id,
           'committee',
           v_registration_id,
           'create c5 registration',
           'created c5 registration number ' || v_registration_id || ' for committee name ' || (p_registration->'committee'->>'name'),
           p_admin_claims->>'user_name',
           now()
         );

  return v_registration_id;
END
$$
