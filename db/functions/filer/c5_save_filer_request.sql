create or replace function c5_save_filer_request(p_registration json, p_claims json) returns int
    language plpgsql as
$$
DECLARE
    v_registration_id INT;
BEGIN
  INSERT INTO public.filer_request(uid, realm, possible_target_id, submitted_at, payload, agent_user_name, action_date,target_type)
    VALUES (
            p_claims->>'uid',
            p_claims->>'realm',
            null,
            current_timestamp,
            p_registration,
            null,
            null,
            'c5_committee'
           )
  returning id into v_registration_id;
  return v_registration_id;
  END
  $$