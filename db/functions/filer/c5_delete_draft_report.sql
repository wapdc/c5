CREATE OR REPLACE FUNCTION c5_delete_draft_report(p_draft_id integer) returns void as
$$
DECLARE
BEGIN
    --delete the draft report
    delete from private.draft_document where draft_id = p_draft_id;

END
$$ language plpgsql;