-- drop function c5_amend_filer_report(p_report json, p_old_report_id int, p_committee_id int, p_claims json);
create or replace function c5_amend_filer_report(p_report json, p_old_report_id int, p_committee_id int, p_claims json) returns int
  language plpgsql as
$$
DECLARE
  v_committee_id int := p_committee_id;
  v_fund_id int;
  v_new_report_id int;
  v_registration_id int;

BEGIN
  -- Set fund
  select c5_get_committee_fund(v_committee_id, p_report->>'election_year') into v_fund_id;

  -- Get registration id
  select registration_id into v_registration_id from committee where committee_id = v_committee_id;

  -- Insert new report
  insert into report(
    report_id, superseded_id, report_type, submitted_at, fund_id, form_type, election_year, filer_name, user_data,
    version, source, filing_method, address1, city, state, postcode, treasurer_name, period_start, period_end
  )
  values (
    nextval('report_report_id_seq'),
    p_old_report_id,
    'C5',
    now(),
    v_fund_id,
    'C5',
    p_report->>'election_year',
    p_report->'registration'->'committee'->>'name',
    p_report->>'user_data',
    p_report->>'version',
    'PDC',
    'e',
    p_report->'registration'->'committee'->>'address',
    p_report->'registration'->'committee'->>'city',
    p_report->'registration'->'committee'->>'state',
    p_report->'registration'->'committee'->>'postcode',
    p_report->>'treasurer_name',
    (p_report->>'period_start')::date,
    (p_report->>'period_end')::date
  )
  returning report_id into v_new_report_id;

  -- Assign repno
  UPDATE report SET repno = v_new_report_id, superseded_repno = p_old_report_id WHERE report_id = v_new_report_id;

  -- Upsert candidate expenses
  insert into c5_expense (
    select coalesce(j.c5_expense_id, nextval('c5_expense_c5_expense_id_seq')) as c5_expense_id,
      v_new_report_id as report_id,
      'CA' as type,
      j.expense_date,
      j.amount,
      j.name,
      null as address,
      null as city,
      null as state,
      null as postcode,
      null as email,
      ca.office_code as office,
      ca.party_id::int as party,
      null as ballot_number,
      null as for_or_against,
      null as purpose,
      j.inkind,
      j.description,
      j.vendor_name,
      j.vendor_address,
      j.vendor_city,
      j.vendor_state,
      j.vendor_postcode,
      j.target_type,
      j.target_id
    from json_populate_recordset(null::c5_expense, (p_report->>'candidate_expenses')::json) j
    join candidacy ca on ca.candidacy_id = j.target_id
  )
  on conflict (c5_expense_id) do update
  set
    report_id = excluded.report_id,
    expense_date = excluded.expense_date,
    amount = excluded.amount,
    name = excluded.name,
    target_type = excluded.target_type,
    target_id = excluded.target_id,
    inkind = excluded.inkind,
    description = excluded.description,
    vendor_name = excluded.vendor_name,
    vendor_address = excluded.vendor_address,
    vendor_city = excluded.vendor_city,
    vendor_state = excluded.vendor_state,
    vendor_postcode = excluded.vendor_postcode;

  -- Upsert committee expenses
  insert into c5_expense (
    select coalesce(j.c5_expense_id, nextval('c5_expense_c5_expense_id_seq')) as c5_expense_id,
      v_new_report_id as report_id,
      'CO' as type,
      j.expense_date,
      j.amount,
      j.name,
      null as address,
      null as city,
      null as state,
      null as postcode,
      null as email,
      null as office,
      null as party,
      j.ballot_number,
      j.for_or_against,
      null as purpose,
      j.inkind,
      j.description,
      j.vendor_name,
      j.vendor_address,
      j.vendor_city,
      j.vendor_state,
      j.vendor_postcode,
      j.target_type,
      j.target_id
    from json_populate_recordset(null::c5_expense, (p_report->>'committee_expenses')::json) j
  )
  on conflict (c5_expense_id) do update
  set
    report_id = excluded.report_id,
    expense_date = excluded.expense_date,
    amount = excluded.amount,
    name = excluded.name,
    address = excluded.address,
    city = excluded.city,
    state = excluded.state,
    postcode = excluded.postcode,
    ballot_number = excluded.ballot_number,
    for_or_against = excluded.for_or_against,
    inkind = excluded.inkind,
    description = excluded.description,
    vendor_name = excluded.vendor_name,
    vendor_address = excluded.vendor_address,
    vendor_city = excluded.vendor_city,
    vendor_state = excluded.vendor_state,
    vendor_postcode = excluded.vendor_postcode,
    target_type = excluded.target_type,
    target_id = excluded.target_id;

  -- Resident contributions
  if p_report->>'has_resident_contribution_import' <> 'true' then
  insert into c5_contribution (
    select coalesce(j.c5_contribution_id, nextval('c5_contribution_c5_contribution_id_seq')) as c5_contribution_id,
      v_new_report_id as report_id,
      j.contribution_date,
      j.amount,
      j.aggregate_total,
      j.name,
      j.address,
      j.city,
      j.state,
      j.postcode,
      j.resident,
      null as email,
      null as employer,
      null as employer_address,
      null as employer_city,
      null as employer_state,
      null as employer_zip
    from json_populate_recordset(null::c5_contribution, (p_report->>'resident_contributions')::json) j
  )
  on conflict (c5_contribution_id) do update
  set
    report_id = excluded.report_id,
    contribution_date = excluded.contribution_date,
    amount = excluded.amount,
    aggregate_total = excluded.aggregate_total,
    name = excluded.name,
    address = excluded.address,
    city = excluded.city,
    state = excluded.state,
    postcode = excluded.postcode,
    resident = excluded.resident;
  end if;

  -- Non-resident contributions
  if p_report->>'has_non_resident_contribution_import' <> 'true' then
  insert into c5_contribution (
    select coalesce(j.c5_contribution_id, nextval('c5_contribution_c5_contribution_id_seq')) as c5_contribution_id,
      v_new_report_id as report_id,
      j.contribution_date,
      j.amount,
      j.aggregate_total,
      j.name,
      j.address,
      j.city,
      j.state,
      j.postcode,
      j.resident,
      j.email,
      j.employer,
      null as employer_address,
      j.employer_city,
      j.employer_state,
      j.employer_zip
    from json_populate_recordset(null::c5_contribution, (p_report->>'non_resident_contributions')::json) j
  )
  on conflict (c5_contribution_id) do update
  set
    report_id = excluded.report_id,
    contribution_date = excluded.contribution_date,
    amount = excluded.amount,
    aggregate_total = excluded.aggregate_total,
    name = excluded.name,
    address = excluded.address,
    city = excluded.city,
    state = excluded.state,
    postcode = excluded.postcode,
    resident = excluded.resident,
    email = excluded.email,
    employer = excluded.employer,
    employer_city = excluded.employer_city,
    employer_state = excluded.employer_state,
    employer_zip = excluded.employer_zip;
  end if;

  -- Logs
  insert into committee_log(committee_id, transaction_type, transaction_id, action, message, user_name, updated_at)
  values (
    v_committee_id,
    'committee',
    v_registration_id,
    'amend c5 report',
    'amended c5 report number ' || p_old_report_id || ' for committee name ' || (p_report->'registration'->'committee'->>'name'),
    p_claims->>'email',
    now()
  );

  return v_new_report_id;
END
$$
