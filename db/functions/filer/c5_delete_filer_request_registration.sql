drop function if exists c5_delete_filer_request_registration(p_request_id integer);
create function c5_delete_filer_request_registration(p_request_id integer) returns void
    language plpgsql
as $$
DECLARE
BEGIN
    --delete the registration from the filer_request table
    delete from public.filer_request where id = p_request_id;

END
$$;


