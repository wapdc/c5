CREATE OR REPLACE FUNCTION c5_delete_report(p_report_id integer, p_user_name text) returns void as
$$
DECLARE
    v_latest_report record;
    v_next_latest_report record;
    v_fund_id int;
    v_registration_id int;
    v_committee_id int;
    v_committee_name text;
    v_entity int;
    i_rec committee_contact;
BEGIN
    -- grab the committee and fund info for this report
    select
        c.committee_id, c.name, c.person_id, f.fund_id
    into v_committee_id, v_committee_name, v_entity, v_fund_id
    from report r join fund f on r.fund_id = f.fund_id join committee c on f.committee_id = c.committee_id
    where r.report_id = p_report_id;

    --grab the latest report for the committee
    select r.* into v_latest_report from report r join fund f on f.fund_id = r.fund_id
    where f.election_code = (select max(f2.election_code) from fund f2 where f2.committee_id = v_committee_id)
      and r.submitted_at = (select max(r2.submitted_at) from report r2 join fund f3 on f3.fund_id = r2.fund_id where f3.committee_id = v_committee_id)
      and f.committee_id = v_committee_id;

    --delete the report
    delete from report where report_id = p_report_id;

    --delete the registration and return the registration_id deleted for logs
    delete from registration where admin_data::json->>'report' = cast(p_report_id as Text) and committee_id = v_committee_id
    returning registration_id into v_registration_id;

    --delete the corresponding contributions and expenses
    delete from c5_contribution where report_id = p_report_id;
    delete from c5_expense where report_id = p_report_id;

    --if last report of the fund, delete the fund
    if(select count(report_id) from report where fund_id = v_fund_id) = 0 then
        delete from fund where fund_id = v_fund_id;
    end if;

    --if this is the last fund of the committee delete the committee, committee_contact
    -- ***keep entity and logs***
    if(select count(*) from fund where committee_id = v_committee_id) = 0 then
        delete from committee where committee_id = v_committee_id;
        delete from committee_contact where committee_id = v_committee_id;

        --if this delete is the latest report and not the last one then update the committee_contact, entity record
        --with the next latest report
    else
        if p_report_id = v_latest_report.report_id then

            --grab the next report in line for the committee
            select r.* into v_next_latest_report from report r join fund f on f.fund_id = r.fund_id
            where f.election_code = (select max(f2.election_code) from fund f2 where f2.committee_id = v_committee_id)
              and r.submitted_at = (select max(r2.submitted_at) from report r2 join fund f3 on f3.fund_id = r2.fund_id where f3.committee_id = v_committee_id)
              and f.committee_id = v_committee_id;

            --update committee_contact
            update committee_contact
            set name = v_next_latest_report.filer_name,
                address = v_next_latest_report.address1,
                city = v_next_latest_report.city,
                state = v_next_latest_report.state,
                postcode = v_next_latest_report.postcode
            where contact_id = (select contact_id from committee_contact where committee_id = v_committee_id and role = 'committee');

            --delete, and then insert new officers
            delete from committee_contact where committee_id = v_committee_id and role = 'officer';

            for i_rec in select j.* from json_populate_recordset(i_rec , (select admin_data::json->'officers' from registration
                                                                          where source ilike 'C5' and admin_data is not null
                                                                            and (admin_data::json->>'report')::int = v_next_latest_report.report_id)) j
                loop
                    insert into committee_contact(committee_id, role, email, name, address, city, state, postcode, phone, title)
                    values(v_committee_id, 'officer', i_rec.email, i_rec.name, i_rec.address, i_rec.city, i_rec.state, i_rec.postcode, i_rec.phone, i_rec.title);
                end loop;

            --update entity
            update entity
             set name = v_next_latest_report.filer_name,
                address = v_next_latest_report.address1,
                city = v_next_latest_report.city,
                state = v_next_latest_report.state,
                postcode = v_next_latest_report.postcode
            where entity_id = v_entity;
        end if;
    end if;

    --write to the committee_logs
    insert into committee_log(committee_id, transaction_type, transaction_id, action, message, user_name, updated_at)
    values (
               v_committee_id,
               'committee',
               v_registration_id,
               'delete c5 report',
               'deleted c5 report number ' || p_report_id || ' for committee name ' || v_committee_name,
               p_user_name,
               now()
           );
END
$$ language plpgsql;