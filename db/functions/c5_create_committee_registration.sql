drop function if exists c5_create_committee_registration(p_report json);
create or replace function c5_create_committee_registration(p_report json, p_user_name text) returns int
    language plpgsql as
$$
DECLARE
    v_entity_id int;
    v_filer_id text;
    v_committee_id int;
    v_registration_id int;
BEGIN
    -- create new entity record
    insert into entity(name, address, city, state, postcode, is_person)
    values (
               p_report->'committee'->>'name',
               p_report->'committee'->>'address',
               p_report->'committee'->>'city',
               p_report->'committee'->>'state',
               p_report->'committee'->>'postcode',
               false
           )
    returning entity_id into v_entity_id;

    -- create new filer_id with an E-(entity_id) then update the entity record
    v_filer_id = 'E-' || v_entity_id;

    update entity
    set filer_id = v_filer_id
    where entity_id = v_entity_id;

    -- create new committee with the entity_id
    -- start year will be calculated and updated after period dates are calculated
    insert into committee(name, filer_id, committee_type, pac_type, continuing, person_id, election_code, start_year)
    values (
               p_report->'committee'->>'name',
               v_filer_id,
               'OS',
               'out-of-state',
               true,
               v_entity_id,
               p_report->>'election_year',
               (p_report->>'election_year')::int
           )
    returning committee_id into v_committee_id;

    -- insert the committee_contact
    insert into committee_contact(committee_id, role, email, name, address, city, state, postcode)
    values (
               v_committee_id,
               'committee',
               p_report->'committee'->>'email',
               p_report->'committee'->>'name',
               p_report->'committee'->>'address',
               p_report->'committee'->>'city',
               p_report->'committee'->>'state',
               p_report->'committee'->>'postcode'
           );

    -- insert the officers
    insert into committee_contact(contact_id, committee_id, role, email, name, address, city, state, postcode, phone, title)
    select nextval('committee_contact_contact_id_seq'), v_committee_id as committee_id, 'officer' as role, j.email, j.name, j.address, j.city, j.state, j.postcode, j.phone, j.title
    from json_populate_recordset(null::committee_contact, (p_report->'admin_data'->>'officers')::json) j;

    -- create registration record for new report
    -- admin data will be updated at the end
    insert into registration(committee_id, name, source, verified, submitted_at, updated_at)
    values (
               v_committee_id,
               p_report->'committee'->>'name',
               'c5',
               true,
               (p_report->>'submitted_at')::timestamptz,
               now()
           )
    returning registration_id into v_registration_id;

    insert into committee_log(committee_id, transaction_type, transaction_id, action, message, user_name, updated_at)
    values (
               v_committee_id,
               'committee',
               v_registration_id,
               'data-entered registration',
               'created c5 report number ' || v_registration_id || ' for committee name ' || (p_report->'committee'->>'name'),
               p_user_name,
               now()
           );

    -- update committee with registration_id
    update committee
    set registration_id = v_registration_id
    where committee_id = v_committee_id;

    return v_registration_id;
END
$$
