delete from menu_links where title = 'Unverified C5 profiles';
insert into menu_links(category, menu, title, abstract,  url, permission, count_function)
values (
  'C5',
  'staff',
  'Unverified C5 profiles',
  'Verify and match out-of-state committees',
  '/c5/#/admin/unverified-committees',
  'access wapdc data',
  'c5_count_access_requests'
);
delete from menu_links where title = 'C5 Data Entry';
insert into menu_links(category, menu, title, abstract, url, permission)
values (
  'C5',
  'staff',
  'C5 Data Entry',
  'Tool for data entry of scanned C5 reports.',
  '/c5/#/data-entry/committees',
  'enter wapdc data'
);
delete from menu_links where title = 'Out-of-state-committees';
insert into menu_links(menu, title, action, abstract, url)
values (
  'filer',
  'Out-of-state committees',
  'Go',
  'Set up your out-of-state committee profile and file out-of-state committee reports',
  '/c5/#/'
);
