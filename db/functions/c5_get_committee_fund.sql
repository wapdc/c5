-- drop function if exists c5_get_committee_fund(p_committee_id int, p_election_code text);
create or replace function c5_get_committee_fund(p_committee_id int, p_election_code text) returns int
    language plpgsql as
$$
DECLARE
    v_fund_id int;
    v_filer_id text;
    v_entity_id int;
BEGIN
    select f.fund_id into v_fund_id from fund f where f.committee_id = p_committee_id and f.election_code = p_election_code;

    if v_fund_id is null then
        select filer_id, person_id into v_filer_id, v_entity_id from committee where committee_id = p_committee_id;

        insert into fund(filer_id, election_code, vendor, entity_id, target_id)
        values (
                   v_filer_id,
                   p_election_code,
                   'PDC',
                   v_entity_id,
                   p_committee_id)
        returning fund_id into v_fund_id;
    end if;

    return v_fund_id;
END
$$
