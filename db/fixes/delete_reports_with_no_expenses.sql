-- find reports that have empty periods caused by having no expenses
with periods as (
    select
        report_id,
        min(e.expense_date) as period_start,
        max(e.expense_date) as period_end
    from c5_expense e group by e.report_id)
select * from report r left join periods p on p.report_id = r.report_id
where form_type = 'C5' and (r.period_start is null or r.period_end is null);

-- delete reports that have empty periods caused by having no expenses
with periods as (
    select
        report_id,
        min(e.expense_date) as period_start,
        max(e.expense_date) as period_end
    from c5_expense e group by e.report_id)
delete from report
where report_id in (
    select r.report_id
    from report r
             left join periods p on p.report_id = r.report_id
    where form_type = 'C5'
      and (r.period_start is null or r.period_end is null)
);