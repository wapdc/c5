delete from entity e where e.entity_id in
                           (select f.entity_id from fund f
                                                        join committee c on c.committee_id = f.committee_id
                            where c.committee_type = 'OS');
DELETE from committee_contact where committee_id in (SELECT committee_id from committee where committee_type = 'OS');
DELETE from c5_expense where c5_expense_id > 0;
DELETE from report where form_type = 'C5';
DELETE from registration r where r.committee_id in (
    select c.committee_id from committee c where c.committee_type = 'OS'
);
DELETE from fund where committee_id in (
    select c.committee_id from committee c where c.committee_type = 'OS'
);
DELETE from committee_log where committee_id in (
    select c.committee_id from committee c where c.committee_type = 'OS'
);
DELETE from committee where committee_type = 'OS';