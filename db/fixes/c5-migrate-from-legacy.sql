-- Fixes ---------------------------------------------------------------------------------------------------------------
update c5_reports_legacy set election_year = '2019' where repno = 1001301029;
update c5_reports_legacy set election_year = '2019' where repno = 1001301198;
update c5_reports_legacy set election_year = '2019' where repno = 1001301227;
update c5_reports_legacy set election_year = '2022' where repno = 1001301384;

update c5_filers_legacy set filer_id = 'COMPUR 125' where ident = 294;
update c5_reports_legacy set filer_id = 'COMPUR 125' where repno = 1001301143;

insert into c5_filers_legacy (filer_id, filer_name, address, city, st, zip, password1, password2)
values ('OFFPRO 036', 'OFFICE & PROFESSIONAL EMP INTL UNION LOCAL 8', '1661 L St NW, Suite 801', 'Washington', 'DC', '20036', '', '');
update c5_reports_legacy set filer_id = 'OFFPRO 036' where repno = 1001301198;

update c5_filers_legacy set filer_id = 'LCVVIC 005' where ident = 293;
update c5_reports_legacy set filer_id = 'LCVVIC 005' where repno = 1001301140;
update c5_reports_legacy set filer_id = 'LCVVIC 005' where repno = 1001301309;

update c5_filers_legacy set filer_id = 'TITUS  454' where ident = 296;
update c5_reports_legacy set filer_id = 'TITUS  454' where repno = 1001301172;

update c5_filers_legacy set filer_id = 'TRUBLU 407' where ident = 300;
update c5_reports_legacy set filer_id = 'TRUBLU 407' where repno = 1001301250;

DO
$$
    DECLARE
        v_rec                       RECORD;
        v_report                    RECORD;
        v_existing_committee        RECORD;
        v_officials                 RECORD;
        v_filer                     RECORD;
        v_expense                   RECORD;
        v_report_number             INT;
        v_entity_id                 INT;
        v_committee_id              INT;
        v_fund_id                   INT;
        v_count                     INT;
        v_registration_id           INT;
        v_latest_registration_id    INT;
        v_period_start              DATE;
        v_period_end                DATE;
        v_report_metadata           JSONB;
        v_registration_metadata     JSONB;
        v_officers                  JSONB;
        v_election_code             TEXT;

    BEGIN
        raise notice 'Deleting reports prior to January 1, 2008';
        delete from c5_reports_legacy where rpt_date < '2008-01-01';
        raise notice 'Deleting candidate contributions prior to January 1, 2008';
        delete from c5_cand_contr_legacy cl where not exists (select 1 from c5_reports_legacy rl where rl.repno = cl.repno);
        raise notice 'Deleting ballot measure contributions prior to January 1, 2008';
        delete from c5_ballot_measure_contr_legacy ml where not exists (select 1 from c5_reports_legacy rl where rl.repno = ml.repno);
        raise notice 'Deleting other contributions prior to January 1, 2008';
        delete from c5_other_contr_legacy ol where not exists (select 1 from c5_reports_legacy rl where rl.repno = ol.repno);
        raise notice 'STARTING MIGRATION';
        v_count = 0;
        FOR v_rec IN (
            --Get legacy C5 filer_ids with start year
            select f.*, rep.start_year, rep.registered_at, rep.rpt_date
            from c5_filers_legacy f
                     join (SELECT filer_id, min(election_year) as start_year, min(rpt_date) as registered_at, rpt_date
                           from c5_reports_legacy
                           where election_year is not null
                           group by filer_id, election_year, rpt_date
                           order by filer_id) rep on rep.filer_id = f.filer_id
            WHERE f.filer_id NOT IN ('','INVALID','invalid')
            )

            LOOP
                v_report_metadata := jsonb_build_object();
                v_registration_metadata := jsonb_build_object();
                --For each distinct filer_id is there a committee record. If not, create one.
                SELECT committee_id
                into v_existing_committee
                from committee c
                where c.committee_type = 'OS'
                  and c.filer_id = v_rec.filer_id;
                IF v_existing_committee IS NULL THEN
                    SELECT filer_id, filer_name, address, city, st, zip
                    into v_filer
                    from c5_filers_legacy
                    where filer_id = v_rec.filer_id;
                    --Create entity record with data from c5-filers
                    INSERT INTO entity (name, filer_id, updated_at, address, city, state, postcode, is_person)
                    VALUES (v_filer.filer_name, v_rec.filer_id, now(), v_filer.address, v_filer.city, v_filer.st, v_filer.zip, false)
                    RETURNING entity_id
                        INTO v_entity_id;
                    --Create committee record with new entity id
                    INSERT INTO committee (name, filer_id, updated_at, committee_type, pac_type, election_code, start_year,
                                           continuing, person_id)
                    VALUES (v_rec.filer_name, v_rec.filer_id, now(), 'OS', 'out-of-state', v_rec.start_year,
                            cast(v_rec.start_year AS INTEGER), true, v_entity_id)
                    RETURNING committee_id into v_committee_id;

                    FOR v_report in
                        (SELECT last_value(repno) over (PARTITION BY filer_id)                            as last_repno,
                                lead(first_rpt, 1)
                                over (PARTITION BY filer_id, election_year order by repno)                as last_report,
                                repno,
                                filer_id,
                                filer_name,
                                form_type,
                                rpt_date,
                                election_year,
                                first_rpt,
                                subsequent_rpt,
                                committee_purpose,
                                memo,
                                howfiled,
                                submitting_official,
                                officials_title,
                                tel_number,
                                email,
                                line8
                         FROM c5_reports_legacy
                         where filer_id = v_rec.filer_id and filer_id NOT IN ('','INVALID','invalid'))
                        LOOP

                            v_report_metadata := jsonb_set(v_report_metadata, '{total_contributions_and_expenditures}', to_jsonb(v_report.line8),true);

                            --coalesce over the variety of expenses, last resort submitted date
                            select coalesce(v_report.election_year::text,
                                            (select EXTRACT(year from min(contr_date))::text from c5_ballot_measure_contr_legacy where repno = v_report.repno),
                                            (select EXTRACT(year from min(contr_date))::text from c5_cand_contr_legacy where repno = v_report.repno),
                                            (select EXTRACT(year from min(date_rcvd))::text from c5_contr_rcvd_legacy where repno = v_report.repno),
                                            (select EXTRACT(year from min(contr_date))::text from c5_other_contr_legacy where repno = v_report.repno),
                                            (select EXTRACT(year from v_report.rpt_date))::text) into v_election_code;

                            --Create fund for each reporting year
                            SELECT fund_id
                                into v_fund_id
                            from fund f
                            where f.committee_id = v_committee_id and v_election_code = f.election_code;

                            IF v_fund_id IS NULL THEN
                                INSERT INTO fund (filer_id, election_code, updated_at, vendor, entity_id, target_id
                                )
                                VALUES (v_report.filer_id, v_election_code, now(), 'PDC', v_entity_id, v_committee_id
                                    )
                                RETURNING fund_id
                                    INTO v_fund_id;
                            END IF;

                            --set admin data
                            SELECT json_agg(o) into v_officers from (SELECT title, concat(first_name, last_name) as name, address, city, zip as postcode
                                                                     from c5_officers_legacy where repno = v_report.repno) o;
                            if v_officers is null then
                                select json_agg(o) into v_officers from (select officials_title as title, submitting_official as name from c5_reports_legacy where repno = v_report.repno) o;
                            end if;

                            v_registration_metadata := jsonb_set(v_registration_metadata, '{report}', to_jsonb(cast(v_report.repno as text)), true);
                            v_registration_metadata := jsonb_set(v_registration_metadata,
                                                                 '{officers}',
                                                                 to_jsonb(case when v_officers is not null then v_officers else '[]' end), true);
                            v_registration_metadata := jsonb_set(v_registration_metadata,
                                                                 '{committee_purpose}',
                                                                 to_jsonb(case when v_report.committee_purpose is not null then v_report.committee_purpose else '' end), true);

                            --Create registration record with new committee id
                            INSERT INTO registration (committee_id, name, admin_data, source, verified, submitted_at, updated_at)
                            VALUES (v_committee_id, v_report.filer_name, v_registration_metadata, 'c5', true, v_report.rpt_date, now())
                            RETURNING registration_id INTO v_registration_id;

                            raise notice 'Updating registration #% - % - %', v_registration_id, v_report.repno, v_report.committee_purpose;

                            UPDATE committee SET registration_id = v_registration_id where committee_id = v_committee_id;

                            --Write logging
                            INSERT INTO committee_log (committee_id, transaction_type, transaction_id, action, message, user_name, updated_at)
                                VALUES(v_committee_id,'registration',v_registration_id,'import legacy C5','creating new registration for each imported committee',v_report.filer_name,now());

                            --Add committee officials from the last report submitted (section 4)
                            SELECT last_value(registration_id) over (PARTITION BY committee_id) into v_latest_registration_id
                            from registration
                            where committee_id = v_committee_id;

                            --if the last report, we update the committee_contact table
                            IF v_report.repno = v_report.last_repno THEN
                                --Create committee address information record in committee_contact
                                INSERT INTO committee_contact (committee_id, name, role, address, city, state, postcode)
                                VALUES (v_committee_id, v_filer.filer_name, 'committee', v_filer.address, v_filer.city, v_filer.st, v_filer.zip);
                                --loop over officials
                                FOR v_officials IN
                                    (SELECT leg.repno,
                                            title,
                                            last_name,
                                            first_name,
                                            address,
                                            city,
                                            state,
                                            zip
                                     from c5_reports_legacy rep
                                        LEFT JOIN c5_officers_legacy leg  on rep.repno = leg.repno
                                     where rep.repno = v_report.last_repno)
                                    LOOP
                                        IF v_officials IS NOT NULL THEN
                                            INSERT INTO committee_contact (committee_id, role, title, name, address, city, postcode)
                                            VALUES (v_committee_id, 'officer', v_officials.title,
                                                    CONCAT(coalesce(v_officials.first_name, ''), ' ', coalesce(v_officials.last_name,'')),
                                                    v_officials.address, v_officials.city, v_officials.zip);

                                        END IF;
                                        IF v_officials IS NULL AND v_report.submitting_official IS NOT NULL THEN
                                            INSERT INTO committee_contact (committee_id, role, title, name)
                                            VALUES (v_committee_id, 'officer', v_report.officials_title, v_report.submitting_official);

                                        END IF;
                                    END LOOP;
                            END IF;

                            --Migrate reports
                            INSERT INTO report (report_id, repno, fund_id, report_type, form_type, election_year,
                                                filer_name, treasurer_name, treasurer_phone,
                                                filing_method, metadata, submitted_at)
                            VALUES (v_report.repno,
                                    v_report.repno,
                                    v_fund_id,
                                    v_report.form_type,
                                    v_report.form_type,
                                    v_report.election_year,
                                    v_report.filer_name,
                                    v_report.submitting_official,
                                    v_report.tel_number,
                                    v_report.howfiled,
                                    v_report_metadata::json,
                                    v_report.rpt_date
                                    )
                            RETURNING report_id into v_report_number;
                            v_count = v_count + 1;
                            raise notice 'Inserted report #% - %', v_count, v_report_number;

                            --Section 5 was never data entered
                            --Migrate candidate contribution expenses (section 6)
                            FOR v_expense IN
                                (SELECT repno, cand_lname, cand_fname, offs1, party, contr_date, amount
                                 from c5_cand_contr_legacy
                                 where repno = v_report.repno
                                   and amount IS NOT NULL)
                                LOOP
                                    INSERT INTO c5_expense (report_id, name, office, party, expense_date, amount, type)
                                    VALUES (v_report.repno, CONCAT(v_expense.cand_fname, ' ', v_expense.cand_lname),
                                            v_expense.offs1, CAST(v_expense.party AS INT),
                                            v_expense.contr_date, v_expense.amount, 'CA');

                                END LOOP;

                            --Migrate ballot measure committee contribution expenses (section 7)
                            FOR v_expense IN
                                (SELECT repno,
                                        recip_name,
                                        recip_address,
                                        recip_city,
                                        recip_st,
                                        recip_zip,
                                        for_against,
                                        contr_date,
                                        amount,
                                        bnum
                                 from c5_ballot_measure_contr_legacy
                                 where repno = v_report.repno
                                   and amount IS NOT NULL)
                                LOOP
                                    INSERT INTO c5_expense (report_id, name, address, city, state, postcode,
                                                            for_or_against, expense_date, amount, ballot_number, type)
                                    VALUES (
                                            v_report.repno,
                                            v_expense.recip_name,
                                            v_expense.recip_address,
                                            v_expense.recip_city,
                                            v_expense.recip_st,
                                            v_expense.recip_zip,
                                            CASE
                                                    WHEN  v_expense.for_against ILIKE 'FOR' THEN
                                                    'F'
                                                ELSE v_expense.for_against
                                            END,
                                            v_expense.contr_date,
                                            v_expense.amount,
                                            v_expense.bnum,
                                            'PROP'
                                           );
                                END LOOP;

                            --Migrate other contributions and expenditures (section 8)
                            FOR v_expense in
                                (SELECT repno,
                                         recip_name,
                                         recip_fname,
                                         recip_address,
                                         recip_city,
                                         recip_state,
                                         recip_zip,
                                         contr_date,
                                         amount,
                                         purpose
                                from c5_other_contr_legacy
                                where repno = v_report.repno
                                   and amount IS NOT NULL)
                                LOOP
                                    INSERT INTO c5_expense (report_id, name, address, city, state, postcode,
                                                            expense_date, amount, purpose, type)
                                    VALUES (v_report.repno,
                                            CASE
                                                WHEN v_expense.recip_name = v_expense.recip_fname THEN
                                                    v_expense.recip_name
                                                WHEN v_expense.recip_name IS NULL AND v_expense.recip_fname IS NULL THEN
                                                    v_expense.recip_name
                                                WHEN v_expense.recip_name IS NULL AND v_expense.recip_fname IS NOT NULL
                                                    THEN
                                                    v_expense.recip_fname
                                                WHEN v_expense.recip_name IS NOT NULL AND v_expense.recip_fname IS NOT NULL
                                                    THEN
                                                    CONCAT(v_expense.recip_fname, ' ', v_expense.recip_name)
                                                ELSE v_expense.recip_name
                                                END,
                                            v_expense.recip_address,
                                            v_expense.recip_city,
                                            v_expense.recip_state,
                                            v_expense.recip_zip,
                                            v_expense.contr_date,
                                            v_expense.amount,
                                            v_expense.purpose,
                                            'OTH'
                                            );
                                END LOOP;

                            --There are 129 legacy reports that have no expenses or contributions, either because
                            --they were filed that way or due to data entry errors. We verified both of these scenarios
                            --and approached Kim Bradford, who made the decision to include them and noted that they will
                            --need to be fixed in the future to any degree possible. For now, these reports will display online
                            --with no period start or end dates.
                            select date_trunc('month', min(expense_date))::date,
                                   (date_trunc('month', max(expense_date)) + interval '1 month - 1 day')::date
                            into v_period_start, v_period_end
                            from c5_expense
                            where report_id = v_report_number
                            group by v_report.rpt_date;

                            UPDATE report
                            SET period_start = v_period_start,
                                period_end = v_period_end
                            WHERE repno = v_report_number;

                            --Sections 10-13 were never data entered

                        END LOOP;
                END IF;
            END LOOP;
    END
$$ language plpgsql;