CREATE TABLE c5_expense
(
    c5_expense_id  INT PRIMARY KEY,
    report_id      INT,
    type           VARCHAR,
    expense_date   DATE,
    amount         numeric(16, 2) default 0 not null,
    name           VARCHAR,
    address        VARCHAR,
    city           VARCHAR,
    state          VARCHAR,
    postcode       VARCHAR,
    resident       BOOLEAN,
    email          VARCHAR,
    office         INT,
    party          INT,
    ballot_number  VARCHAR,
    for_or_against VARCHAR,
    purpose        VARCHAR
);

CREATE TABLE c5_contribution
(
    c5_contribution_id INT PRIMARY KEY,
    report_id          INT,
    contribution_date  DATE,
    amount             numeric(16, 2) default 0 not null,
    aggregate_total    numeric(16, 2) default 0 not null,
    name               VARCHAR,
    address            VARCHAR,
    city               VARCHAR,
    state              VARCHAR,
    postcode           VARCHAR,
    resident           BOOLEAN,
    email              VARCHAR,
    employer           VARCHAR,
    employer_address   VARCHAR,
    employer_city      VARCHAR,
    employer_state     VARCHAR,
    employer_zip       VARCHAR
);

ALTER TABLE report ADD ax_legacy_docid VARCHAR;