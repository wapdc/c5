<?php
namespace Drupal\c5\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;

/**
 * Controller for C5 module
 */
class C5Controller extends ControllerBase{

  /**
   * Fully qualified path to single page application being served.
   *
   * @return string
   */
  protected function getApplicationDirectory(): string {
    // Get the module handler service from the service container.
    $moduleHandler = \Drupal::service('module_handler');
    $modulePath = $moduleHandler->getModule('c5')->getPath();
    return $modulePath . '/ui';
  }

  /**
   * Return mime type of a given file.
   *
   * @param string $filename
   * @return string
   */
  protected function getMimeType(string $filename): string {
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    $mimeTypes = [
      'txt' => 'text/plain',
      'htm' => 'text/html',
      'html' => 'text/html',
      'php' => 'text/html',
      'css' => 'text/css',
      'js' => 'application/javascript',
      'json' => 'application/json',
      'xml' => 'application/xml',
      'swf' => 'application/x-shockwave-flash',
      'flv' => 'video/x-flv',

      // fonts
      'eot' => 'application/vnd.ms-fontobject',
      'ttf' => 'application/font-sfnt',
      'otf' => 'application/font-sfnt',
      'woff' => 'application/font-woff',
      'woff2' => 'application/font-woff2',

      // images
      'png' => 'image/png',
      'jpe' => 'image/jpeg',
      'jpeg' => 'image/jpeg',
      'jpg' => 'image/jpeg',
      'gif' => 'image/gif',
      'bmp' => 'image/bmp',
      'ico' => 'image/vnd.microsoft.icon',
      'tiff' => 'image/tiff',
      'tif' => 'image/tiff',
      'svg' => 'image/svg+xml',
      'svgz' => 'image/svg+xml',

      // archives
      'zip' => 'application/zip',
      'rar' => 'application/x-rar-compressed',
      'exe' => 'application/x-msdownload',
      'msi' => 'application/x-msdownload',
      'cab' => 'application/vnd.ms-cab-compressed',

      // audio/video
      'mp3' => 'audio/mpeg',
      'qt' => 'video/quicktime',
      'mov' => 'video/quicktime',

      // adobe
      'pdf' => 'application/pdf',
      'psd' => 'image/vnd.adobe.photoshop',
      'ai' => 'application/postscript',
      'eps' => 'application/postscript',
      'ps' => 'application/postscript',

      // ms office
      'doc' => 'application/msword',
      'rtf' => 'application/rtf',
      'xls' => 'application/vnd.ms-excel',
      'ppt' => 'application/vnd.ms-powerpoint',
      'docx' => 'application/msword',
      'xlsx' => 'application/vnd.ms-excel',
      'pptx' => 'application/vnd.ms-powerpoint',

      // open office
      'odt' => 'application/vnd.oasis.opendocument.text',
      'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
    ];

    if (array_key_exists($ext, $mimeTypes)) {
      return $mimeTypes[$ext];
    } else {
      $guesser = new FileinfoMimeTypeGuesser();
      if ($guesser->isSupported()) {
        return $guesser->guess($filename) ?: 'application/octet-stream';
      }
    }

    return 'application/octet-stream';
  }


  /**
   * Serves the requested asset from the application directory.
   *
   * @param string $directory The directory of the requested asset.
   * @param string $asset The filename of the requested asset.
   *
   * @return Response The response containing the asset content.
   */
  public function serve(string $directory = "", string $asset = ""): Response {
    $response = new Response();
    $dir = $this->getApplicationDirectory();

    // Determine the requested file.
    if ($asset) {
      $file = $dir . '/' . $directory . '/' . $asset;
    } elseif ($directory === 'favicon.ico') {
      $file = $dir . '/favicon.ico';
    } else {
      $file = $dir . '/index.html';
    }

    // If the file exists, get its contents and set the response.
    if (file_exists($file)) {
      $contents = file_get_contents($file);
      if ($contents !== false) {
        $response->setContent($contents);
        $response->headers->set('Content-Type', $this->getMimeType($file));
      }
    } else {
      \Drupal::logger('c5')->notice('File does not exist: ' . $file);
      // Handle SPA routes
      // We fall back to serving index.html when the requested asset doesn't exist.
      $indexFile = $dir . '/index.html';
      if (file_exists($indexFile)) {
        $contents = file_get_contents($indexFile);
        if ($contents !== false) {
          $response->setContent($contents);
          $response->headers->set('Content-Type', $this->getMimeType($indexFile));
        }
      } else {
        $response->setContent('File not found.');
        $response->setStatusCode(Response::HTTP_NOT_FOUND);
      }
    }

    return $response;
  }
}
