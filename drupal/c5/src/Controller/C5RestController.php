<?php

namespace Drupal\c5\Controller;
use Drupal\wapdc_core\Controller\CoreControllerBase;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use WAPDC\Core\CoreDataService;
use WAPDC\Core\TokenProcessor;
use stdClass;

class C5RestController extends CoreControllerBase {

    /**
     * Get user
     * @throws Exception
     */
    public function getC5Api() {
        try {
            $pdc_user = $this->getCurrentUser();
            $db = CoreDataService::service()->getDataManager()->db;
            $userManager = CoreDataService::service()->getUserManager($pdc_user->realm);
            $pdc_user = $userManager->getRegisteredUser($pdc_user->uid);
            $drupal_user = $this->currentUser();
            $settings = $db->fetchAssociative(
                "SELECT wapdc_setting('apollo_url') as apollo_url, wapdc_setting('c5_aws_gateway') as c5_aws_gateway"
            );
            $response = new stdClass();
            $response->appInfo = new stdClass();
            $response->appInfo->apollo_url = $settings['apollo_url'];
            $response->appInfo->c5_aws_gateway = $settings['c5_aws_gateway'];
            $response->user = new stdClass();
            $response->user->user_name = $pdc_user->user_name;
            $response->user->uid = $pdc_user->uid;
            $response->user->realm = $pdc_user->realm;
            $response->user->admin = $drupal_user->hasPermission('enter wapdc data');

            $claims = [
                'uid' => $response->user->uid,
                'realm' => $response->user->realm,
                'admin' => $response->user->admin,
            ];
            $token_expires = 900;
            $token_processor = new TokenProcessor();
            $response->appInfo->token = "Bearer " . $token_processor->generateToken($claims, $token_expires);
            $response->appInfo->expires = $token_expires;

            return new JsonResponse($response);
        } catch (Exception $e) {
            \Drupal::logger('c5')->error($e->getMessage());
            return new JsonResponse("Error generating api token", 500);
        }
    }
}
