import {useGateway} from '@wapdc/pdc-ui/application'
import {ref} from 'vue'

let awsGateway
const committee = ref({})

async function getAwsGateway() {
  return await useGateway('c5_aws_gateway', 3020)
}

async function initGateway() {
  if (!awsGateway) {
    awsGateway = await getAwsGateway()
  }
}

async function getOutOfStateCommittees() {
  return await awsGateway.get("admin/committees")
}

//create load function and return in use function
async function loadCommitteeInfo(committee_id) {
  const committee_info = await awsGateway.get(`admin/committees/${committee_id}`, !!committee.value.committee_id)
  return committee_info

}

async function deleteCommitteeReport(report_id, committee_id) {
  return await awsGateway.delete(`admin/committees/${committee_id}/reports/${report_id}`)
}

export const useOutOfStateCommittee = async(committee_id) => {
  await initGateway();

 return { deleteCommitteeReport, loadCommitteeInfo, getOutOfStateCommittees, awsGateway}
}





