import { useGateway } from '@wapdc/pdc-ui/application'

let awsGateway

async function getAwsGateway() {
  return await useGateway('c5_aws_gateway', 3020)
}

async function initGateway() {
  if (!awsGateway) {
    awsGateway = await getAwsGateway()
  }
}

async function loadUnverifiedCommittees() {
  return await awsGateway.get(`admin/committees/unverified-committees`)
}

async function revertRequest(request_id) {
  return await awsGateway.post(`admin/requests/${request_id}/revert`)
}

async function verifyNonMatch(request_id) {
  return await awsGateway.post(`admin/requests/${request_id}/verify-non-match`)
}

async function updateAccessRequest(request_id, agent_user_name = null, action_date = null) {
  return await awsGateway.put(`admin/requests/${request_id}/update`, {agent_user_name, action_date})
}
async function verifyMatch(request_id, committee_id) {
  return await awsGateway.post(`admin/requests/${request_id}/verify-match`, { committee_id })
}

async function getMatchCommittees(name) {
  const response = await awsGateway.post(`admin/match-committees`, { name })
  return response.map(response_committee => {
    const json_data = JSON.parse(response_committee.admin_data)
    return {
      committee: {
        committee_id: response_committee.committee_id,
        committee_name: response_committee.committee_name,
        address: response_committee.entity_address,
        city: response_committee.entity_city,
        email: response_committee.entity_email,
        name: response_committee.committee_email,
        postcode: response_committee.entity_postcode,
        state: response_committee.entity_state,
      },
      committee_purpose: json_data?.committee_purpose,
      officers: [...(json_data?.officers ?? [])],
    }
  })
}

export const useC5Verification = async() => {
  await initGateway()
  return {
    awsGateway,
    loadUnverifiedCommittees,
    revertRequest,
    verifyNonMatch,
    updateAccessRequest,
    verifyMatch,
    getMatchCommittees,
  }
}
