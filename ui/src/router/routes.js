const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {path: '', name: 'my-committees', component: () => import('pages/C5CommitteesIFileFor.vue')},
      { path: '/registration/section/:section?', name: 'edit-registration', component: () => import('pages/filer/EditRegistration.vue') },
      { path: '/registration/section/:section?/report/:report_id?', name: 'edit-committee-registration', component: () => import('pages/filer/EditRegistration.vue') },
    ]
  },
  {
    path: '/public',
    component: () => import('layouts/PublicLayout.vue'),
    children: [
      { path: 'report/:report_id', component: () => import('pages/PublicReportView.vue')},
    ]
  },
  {
    path: '/committee/:committee_id',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'committee-reports', component: () => import('pages/filer/C5Reports.vue')},
      { path: 'section/:section?/report/:report_id?', name: 'edit-committee-registration', component: () => import('pages/filer/EditRegistration.vue') },
      { path: 'registrations/:registration_id?', name: 'filer-committee-overview', props: true, component: () => import('pages/filer/C5Committee.vue')},
      { path: 'history', name: 'filer-committee-history', component: () => import('pages/filer/C5CommitteeHistory.vue')},
      { path: 'edit-report/:report_id', name: 'edit-report', component: () => import('pages/filer/EditReport.vue')},
      { path: 'preview-report/:report_id', name: 'preview-report', component: () => import('pages/filer/PreviewReport.vue')},
      { path: 'view-report/:report_id',  name: 'view-report', component: () => import('pages/filer/ViewReport.vue')},
      { path: 'history-report/:report_id/period-month/:period_month', name: 'history-report', component: () => import('pages/filer/HistoryReport.vue')},
    ]
  },
  {
    path: '/data-entry',
    component: () => import('layouts/DataEntryLayout.vue'),
    children: [
      // admins data entry ----------------------------------------------------------------------------------
      { path: 'committees', name: 'committee-search', component: () => import('pages/data-entry/C5Committees.vue')},
      { path: 'report', name: 'new-committee-report', component: () => import('pages/data-entry/C5Report.vue')},
      //This is the report route for a new report, without an existing committee
      { path: 'committee/:committee_id/report', name: 'new-report', component: () => import('pages/data-entry/C5Report.vue')},
      { path: 'committees/:committee_id/logs', name: 'committee-logs', component: () => import('pages/data-entry/C5CommitteeLogs.vue')},
      { path: 'committees/:committee_id', name: 'committee-overview', component: () => import('pages/data-entry/C5Committee.vue') },
      { path: 'committees/:committee_id/reports/:report_id', name: 'edit-committee-report', component: () => import('pages/data-entry/C5Report.vue')},
    ]
  },
  {
      path: '/admin',
      component: () => import('layouts/AdminLayout.vue'),
      children: [
      // admins verification --------------------------------------------------------------------------------
      { path: 'unverified-committees', name: 'unverified-committees', component: () => import('pages/admin/UnverifiedCommittees.vue')},
      { path: 'verify-committee/request/:request_id', name: 'verify-committee', component: () => import('pages/admin/VerifyCommittee.vue')},
    ]
  },
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
