import similarity from "similarity"
import moment from 'moment'
/**
 * Determines similarity between query and text
 * Priority is given in this order:
 *   - exact matches with regard to position,
 *   - exact matches
 *   - similarity
 * @param {string} query
 * @param {string} text
 * @returns {{score: number, match: boolean}}
 */
export const isSimilar = (query, text) => {
  let score = 0.0;
  let match = false;
  const queryLower = query.toLowerCase();
  const textLower = text.toLowerCase();

  if (query.length > 0) {
    if (textLower === queryLower) {
      score = 100;
    } else if (textLower.startsWith(queryLower)) {
      score = 90;
    } else {
      score = similarity(queryLower, textLower) * 50;
      if (textLower.includes(queryLower)) {
        score += 20;
      }
    }
    match = score > 20;
  }
  return { score, match };
};
export function formatDate(date_param) {
  if (!date_param) {
    return ''
  }
  if (moment(date_param, 'YYYY-MM-DD', true).isValid()) {
    return (new moment(date_param, 'YYYY-MM-DD')).format('MM/DD/YYYY')
  }
  else if(moment(date_param, 'MM/DD/YYYY', true).isValid()) {
    return (new moment(date_param)).format('MM/DD/YYYY')
  }
  else {
    return (new moment(date_param)).format('MM/DD/YYYY h:mma')
  }
}

const {format: moneyFormat} = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  maximumFractionDigits: 2
});

export function formatMoney(amount, defaultValue='$0.00') {
  try {
    if(typeof amount !== "undefined" && amount && !isNaN(amount)) {
      return addParenthesis(moneyFormat(amount))
    } else {
      return defaultValue
    }
  } catch(e) {
    console.error(e);
  }
}

export function today() {
  return moment(new Date()).format('YYYY-MM-DD');
}

function addParenthesis(val) {
  const temp = val.replace('$', '')
  if( temp < 0 ){
    return "$(" + temp + ")"
  } else {
    return val
  }
}

export function getMonthYearLabel(period_month) {
  if (! period_month) {
    return ''
  }

  if (typeof period_month === 'string') {
    period_month = dateStringToDateDate(period_month)
  }

  const month_label = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
  return month_label[period_month.getMonth()] + ' ' + period_month.getFullYear()
}

export function dateStringToDateDate(date_string) {
  return new Date(date_string.replace(/-/g, '\/').replace(/T.+/, ''))
}
