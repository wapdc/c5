import { reactive } from 'vue'
import { useGateway } from '@wapdc/pdc-ui/application'

let awsGateway
async function getAwsGateway() {
  return await useGateway('c5_aws_gateway', 3020)
}

async function initGateway() {
  if (!awsGateway) {
    awsGateway = await getAwsGateway()
  }
}

const fixedC5Report = reactive({
    fund: {},
    ax_legacy_docid: null,
    submitted_at: null,
    election_year: null,
    user_data: null,
    version: '1.0',
    report_type: 'C5',
    superseded_id: null,
    treasurer_name: null,
    committee: {},
    admin_data: {
      report: null,
      committee_purpose: '',
      officers: [],
      reporting_states: [],
    },
    metadata: {
      total_contributions_and_expenditures: null,
      aggregate_contributions: {}
    },
    expenses: {
      other: [],
      ballot: [],
      candidate: []
    },
    contributions: {
      resident: [],
      non_resident: []
    }
})

function jsonClone(object) {
  return JSON.parse(JSON.stringify(object))
}

const c5Report = reactive(jsonClone(fixedC5Report))

function saveC5ToSessionStorage() {
    sessionStorage.setItem('c5_report', JSON.stringify(c5Report))
}

function retrieveC5FromSessionStorage() {
    const session_storage_report = JSON.parse(sessionStorage.getItem('c5_report'))
    Object.assign(c5Report, session_storage_report)
}

/*
*  For creating a new report for existing committee
*/
async function getLatestCommitteeReportInfo(committee_id) {
  const committee_report_info = await awsGateway.get(`admin/committees/${committee_id}/report`)
  Object.assign(c5Report, committee_report_info)
}

export async function submitReport(committee_id, report_id) {
  if(committee_id && !report_id) {
    await awsGateway.post(`admin/committees/${committee_id}/report`, c5Report)
  } else if(report_id) {
    await awsGateway.post(`admin/committees/${committee_id}/reports/${report_id}`, c5Report)
  } else {
    await awsGateway.post('admin/report', c5Report)
  }
}

/*
*  For editing an existing committee report
*/
async function loadReport(committee_id, report_id) {
  const { report } = await awsGateway.get(`admin/committees/${committee_id}/reports/${report_id}`)
  Object.assign(c5Report, report)
}

function clearC5() {
    Object.assign(c5Report, jsonClone(fixedC5Report))
    saveC5ToSessionStorage()
}

export const useC5Report = async() => {
    await initGateway()
    return {
        awsGateway,
        c5Report,
        loadReport,
        saveC5ToSessionStorage,
        retrieveC5FromSessionStorage,
        clearC5,
        getLatestCommitteeReportInfo
    }
}
