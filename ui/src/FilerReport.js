import {reactive, ref} from 'vue'
import { useGateway } from '@wapdc/pdc-ui/application'
import { useC5Registration } from 'src/C5Registration'
import pako from 'pako'

let awsGateway
let c5Registration
let loadCommittee

const officeTypes = ref([])
const partyTypes = ref([])
const committees = ref([])
const candidates = ref([])

async function getAwsGateway() {
  return await useGateway('c5_aws_gateway', 3020)
}

async function initGateway() {
  if (!awsGateway) {
    awsGateway = await getAwsGateway()
  }
}

const fixedFilerReport = reactive({
  version: '1.0',
  report_type: 'C5',
  period_month: '',
  period_start: '',
  previous_amount: null,
  previous_gt_twenty_percent: null,
  candidate_expenses: [],
  committee_expenses: [],
  resident_contributions: [],
  non_resident_contributions: [],
  resident_contribution_import: {},
  non_resident_contribution_import: {},
  has_resident_contribution_import: null,
  has_non_resident_contribution_import: null,
  report_id: null,
  has_candidate_expenses: null,
  has_committee_expenses: null,
  has_resident_contributions: null,
  has_non_resident_contributions: null,
  registration: null,
  validations: [],
  certification: {
    name: null,
    statement: null,
    text: null,
  }
})

function jsonClone(object) {
  return JSON.parse(JSON.stringify(object))
}

const filerReport = reactive(jsonClone(fixedFilerReport))
let savedReport = jsonClone(fixedFilerReport)

async function getOffices() {
  const { request_office_types, success } = await awsGateway.get('offices')
  if (success) {
    officeTypes.value = request_office_types
  }
}

async function getParties() {
  const { request_party_types, success } = await awsGateway.get('parties')
  if (success) {
    partyTypes.value = request_party_types
  }
}

async function createDraftReport(committee_id, period_month) {
  filerReport.period_month = period_month
  return await awsGateway.post(`committees/${committee_id}/new-draft-report`, filerReport)
}

async function createAmendDraftReport(committee_id, report_id) {
  return await awsGateway.post(`committees/${committee_id}/new-amend-draft-report/${report_id}`)
}

async function getCommitteeReports(committee_id) {
  return await awsGateway.get(`/committees/${committee_id}/draft-reports`)
}

async function getCommitteeHistory(committee_id) {
  return  await awsGateway.get(`/committees/${committee_id}/history`)
}

async function deleteReport(committee_id, draft_id) {
  return await awsGateway.delete(`/committees/${committee_id}/draft-reports/${draft_id}`)
}

async function submitDraftReport(committee_id, report_id) {
  filerReport.user_data = {
    previous_amount: filerReport.previous_amount,
    previous_gt_twenty_percent: filerReport.previous_gt_twenty_percent,
    candidate_expenses: filerReport.candidate_expenses,
    committee_expenses: filerReport.committee_expenses,
    resident_contributions: filerReport.resident_contributions,
    non_resident_contributions: filerReport.non_resident_contributions,
    resident_contribution_import: filerReport.resident_contribution_import,
    has_resident_contribution_import: filerReport.has_resident_contribution_import,
    non_resident_contribution_import: filerReport.non_resident_contribution_import,
    has_non_resident_contribution_import: filerReport.has_non_resident_contribution_import,
    certification: filerReport.certification,
    registration: filerReport.registration,
    period_start: filerReport.period_start,
  }

  if(!filerReport.period_start) throw new Error('Period start is required')
  if(!filerReport.user_data) throw new Error('User data is required')

  if (!filerReport.user_data.certification.statement) {
    throw new Error('Certification is missing or null');
  }
  if (!filerReport.user_data.certification.name) {
    throw new Error('Certification name is missing or null');
  }
  if (filerReport.user_data.certification.statement.toLowerCase().trim() !== "i certify") {
    throw new Error('Certification is invalid');
  }
  if (!filerReport.user_data.certification.text || !filerReport.user_data.certification.text.trim()) {
    throw new Error('Certification text is empty or null');
  }
  return await awsGateway.post(`/committees/${committee_id}/submit-draft-report/${report_id}`, filerReport)
}

function bytesToBase64(bytes) {
  const binString = Array.from(bytes, (byte) =>
    String.fromCodePoint(byte),
  ).join("");
  return btoa(binString);
}

async function saveDraftReport(committee_id, report_id){
  if (JSON.stringify(filerReport) !== JSON.stringify(savedReport)) {
    const { save_count, validations } = await awsGateway.put(
      `/committees/${committee_id}/save-draft-report/${report_id}`, { report: filerReport },
    )
    if (typeof save_count === 'number') {
      filerReport.validations = validations
      filerReport.save_count = save_count
      savedReport = jsonClone(filerReport)
    }
    return save_count
  }
  // A transaction was not necessary here as there is nothing to save but we still have to return something so we are returning the previous save_count.
  return filerReport.save_count
}

function clearFilerReport() {
  Object.assign(filerReport, jsonClone(fixedFilerReport))
  Object.assign(savedReport, jsonClone(fixedFilerReport))
}

function handlePeriodStartEndElectionYear(report) {
  const period_array = (new Date(report.period_month.replace(/-/g, "/"))).toISOString("en-US", {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
  }).split('T')[0].split('-')
  const last_day_of_the_month = (new Date(period_array[0], period_array[1], 0)).getDate()
  report.period_start = period_array[0] + '-' + period_array[1] + '-' + '01'
  report.period_end = period_array[0] + '-' + period_array[1] + '-' + last_day_of_the_month
  report.election_year = period_array[0]
}

function handlePeriodStart(report) {
  const period_array = report.period_start.split('-')
  report.period_month = `${period_array[1]}-01-${period_array[0]}`
}

function handleRestOfUserData(report) {
  report.previous_amount = report.user_data.previous_amount
  report.previous_gt_twenty_percent = report.user_data.previous_gt_twenty_percent
  report.certification = report.user_data.certification
  report.candidate_expenses = report.user_data.candidate_expenses
  report.committee_expenses = report.user_data.committee_expenses
  report.resident_contributions = report.user_data.resident_contributions
  report.non_resident_contributions = report.user_data.non_resident_contributions
  report.has_candidate_expenses = report.user_data.candidate_expenses.length > 0
  report.has_committee_expenses = report.user_data.committee_expenses.length > 0
  report.has_resident_contributions = report.user_data.resident_contributions.length > 0
  report.has_non_resident_contributions = report.user_data.non_resident_contributions.length > 0
  report.resident_contribution_import = report.user_data.resident_contribution_import
  report.has_resident_contribution_import = report.user_data.has_resident_contribution_import
  report.non_resident_contribution_import = report.user_data.non_resident_contribution_import
  report.has_non_resident_contribution_import = report.user_data.has_non_resident_contribution_import
}

async function viewReport(committee_id, report_id) {
  let report = await awsGateway.get(`/committees/${committee_id}/view-report/${report_id}`)
  await loadCommittee(committee_id)
  report.report_id = report_id
  report.committee_id = committee_id
  report.registration = jsonClone(c5Registration)
  handlePeriodStart(report)
  handleRestOfUserData(report)
  Object.assign(filerReport, report)
}

async function loadReport(committee_id, report_id) {
  clearFilerReport()
  const report = await awsGateway.get(`/committees/${committee_id}/draft-report/${report_id}`)
  await loadCommittee(committee_id)
  report.committee_id = committee_id
  report.registration = jsonClone(c5Registration)
  handlePeriodStartEndElectionYear(report)
  Object.assign(filerReport, report)
  // for import
  if (report.has_resident_contribution_import) {
    filerReport.resident_contributions = [{amount: filerReport.resident_contribution_import.total_amount}]
    filerReport.has_resident_contributions = true
  }
  if (report.has_non_resident_contribution_import) {
    filerReport.non_resident_contributions = [{amount: filerReport.non_resident_contribution_import.total_amount}]
    filerReport.has_non_resident_contributions = true
  }
}

async function getReportHistory(committee_id, report_id, period_month) {
  return await awsGateway.get(`/committees/${committee_id}/history-report/${report_id}/period-month/${period_month}`)
}

async function getPeriodCommittees(committee_id, period_start) {
  const response_committees = await awsGateway.get(`/committees/${committee_id}/period-committees/${period_start}`)
  if (response_committees.length > 0) {
    committees.value = response_committees
  }
}

async function getPeriodCandidates(committee_id, period_start) {
  const response_candidates = await awsGateway.get(`/committees/${committee_id}/period-candidates/${period_start}`)
  if (response_candidates.length > 0) {
    candidates.value = response_candidates
  }
}

export const useFilerReport = async() => {
  await initGateway()

  const response = (await useC5Registration())
  loadCommittee = response.loadCommittee
  c5Registration = response.c5Registration

  return {
    awsGateway,
    filerReport,
    saveDraftReport,
    createDraftReport,
    getCommitteeReports,
    deleteReport,
    clearFilerReport,
    loadReport,
    getOffices,
    getParties,
    submitDraftReport,
    officeTypes,
    partyTypes,
    viewReport,
    getCommitteeHistory,
    createAmendDraftReport,
    getReportHistory,
    getPeriodCommittees,
    getPeriodCandidates,
    candidates,
    committees,
  }
}
