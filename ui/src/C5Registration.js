import {reactive} from 'vue'
import {useGateway} from '@wapdc/pdc-ui/application'

let awsGateway

async function getAwsGateway() {
  return await useGateway('c5_aws_gateway', 3020)
}

async function initGateway() {
  if (!awsGateway) {
    awsGateway = await getAwsGateway()
  }
}

const fixedC5Registration = reactive({
  committee_purpose: '',
  officers: [],
  reporting_states: [],
  committee: {
    name: '',
    email: '',
    address: '',
    city: '',
    state: '',
    postcode: '',
  },
})

function jsonClone(object) {
  return JSON.parse(JSON.stringify(object))
}

const c5Registration = reactive(jsonClone(fixedC5Registration))

async function createRegistration() {
  return await awsGateway.post(`/create-request-registration`, c5Registration)
}

async function getCommitteeInfo(committee_id) {
  return await awsGateway.get(`/committees/${committee_id}`)
}

async function getHistoricalCommitteeInfo(committee_id, registration_id) {
  return await awsGateway.get(`/committees/${committee_id}/${registration_id}`)
}

async function getFilerRequestRegistrationData(request_id) {
  return await awsGateway.get(`/admin/committees/verify/${request_id}`)
}

async function amendCommittee(committee_id){
  return await awsGateway.post(`/committees/${committee_id}/amend`, c5Registration)
}

async function loadCommittee(committee_id) {
  const { committee } = await awsGateway.get(`/committees/${committee_id}`, committee_id)
  Object.assign(c5Registration, committee.user_data)
  c5Registration.registration_id = committee.registration_id;
}

function clearRegistration() {
  Object.assign(c5Registration, jsonClone(fixedC5Registration))
}

export const useC5Registration = async() => {
  await initGateway()
  return {
    awsGateway,
    c5Registration,
    getCommitteeInfo,
    getHistoricalCommitteeInfo,
    amendCommittee,
    createRegistration,
    loadCommittee,
    clearRegistration,
    getFilerRequestRegistrationData,
  }
}
