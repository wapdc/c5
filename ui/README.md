# Out-of-State Committee Reporting (c5)

Out-of-State Committee Reporting

## Installation

**IMPORTANT**:
Make sure you build & deploy the module as the built files are not included in git.

1. Clone the repository from GitLab
2. Run `npm install`
3. Run `npm run build`
4. Add a deployment setting of `/Users/<your_name>/dev/c5/drupal/` module to your local apollo's `web/modules/custom/` directory in your PHP Storm settings for the project.
5. For UI deployment add `/Users/<your_name>/dev/c5/ui/dist/spa/` deployed to `web/modules/custom/c5/ui/`
6. deploy the `c5` module folder to apollo
7. Clear caches on apollo
8. Enable the module on apollo (under Extend tab)
9. Visit `https://apollod8.lndo.site/c5` to see the module in action.
