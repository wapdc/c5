import {runRevScripts} from '@wapdc/common/revision-manager'
import path from 'path'
import * as url from 'url'
const __dirname = url.fileURLToPath(new URL('.', import.meta.url))
const scriptDirectory = path.join(__dirname, '..', '..', 'revisions')
console.log(`scriptDirectory: ${scriptDirectory}`)
const appName = 'c5'
import * as db from '@wapdc/common/wapdc-db'

await db.connect()
await db.beginTransaction()
await runRevScripts(scriptDirectory, appName)
    .then(() => {
        console.log('Revision scripts ran successfully')
    })
    .catch(error => {
        console.error(`Failed to run revision scripts: ${error.message}`)
        throw error
    })
await db.commit()
await db.close()
