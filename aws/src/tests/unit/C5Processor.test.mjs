import { jest, beforeEach, afterEach, beforeAll, afterAll, expect, test, describe } from '@jest/globals'
import { beginTransaction, rollback, connect, close, query, fetch } from '@wapdc/common/wapdc-db'
import { c5Processor } from '../../C5Processor.js'
import { filerC5Processor } from '../../FilerC5Processor.js'
import { readFile } from 'fs/promises'
import env from 'dotenv'
import sgMail from '@sendgrid/mail'

env.config()
jest.setTimeout(2000000000)

beforeAll(async () => {
  await connect()
})

afterAll(async () => {
  await close()
})

beforeEach(async () => {
  await beginTransaction()
})

afterEach(async () => {
  await rollback()
})

/**
 *
 * @param reportId
 * @returns
 * @private
 */

async function _getReportInfo(reportId) {
  return await fetch(
      `select
             c.committee_id, f.fund_id, r.report_id, rg.registration_id
         from report r
            join fund f on r.fund_id = f.fund_id
            join committee c on f.committee_id = c.committee_id
            join registration rg on rg.committee_id = c.committee_id and (rg.admin_data::json->>'report')::int = r.report_id
         where r.report_id = $1`, [reportId])
}

describe('Test C5', () => {
  test('Test submit C5 report validation', async () => {
    const fixed_c5_report = await readFile(`src/tests/data/test-c5-submit.json`, 'ascii')
    let c5_validations = await readFile(`src/tests/data/test-c5-submit-validations.json`, 'ascii')
    c5_validations = JSON.parse(c5_validations)

    for (const expected_validation of c5_validations) {
      let c5_report = JSON.parse(fixed_c5_report)
      eval(expected_validation.modification)
      const resulting_validation = c5Processor.validateReport(c5_report)
      expect(resulting_validation).toBe(expected_validation.error)
    }

  })
  test('Test submit C5 report', async () => {
    let c5_report = await readFile(`src/tests/data/test-c5-submit.json`, 'ascii')
    c5_report = JSON.parse(c5_report)

    await c5Processor.submitReport(c5_report, 'test@test.com')

    // test entity
    const c5_entity = await fetch(`select * from entity where name = $1`, [c5_report.committee.name])
    const filer_id = 'E-' + c5_entity.entity_id

    expect(c5_entity.name).toBe(c5_report.committee.name)
    expect(c5_entity.address).toBe(c5_report.committee.address)
    expect(c5_entity.city).toBe(c5_report.committee.city)
    expect(c5_entity.state).toBe(c5_report.committee.state)
    expect(c5_entity.postcode).toBe(c5_report.committee.postcode)
    expect(c5_entity.is_person).toBe(false)
    expect(c5_entity.filer_id).toBe(filer_id)

    // test committee
    const c5_committee = await fetch(`select * from committee where name = $1`, [c5_report.committee.name])
    expect(c5_committee.name).toBe(c5_report.committee.name)
    expect(c5_committee.filer_id).toBe(filer_id)
    expect(c5_committee.committee_type).toBe('OS')
    expect(c5_committee.pac_type).toBe('out-of-state')
    expect(c5_committee.continuing).toBe(true)
    expect(c5_committee.person_id).toBe(c5_entity.entity_id)
    // start_year is an integer while election_code is a varchar, even though they contain the same value
    expect(c5_committee.start_year).toBe(parseInt(c5_report.election_year))
    expect(c5_committee.election_code).toBe(c5_report.election_year)

    // test committee_contact - committee
    const c5_committee_contact_committee = await fetch(
        `select * from committee_contact where committee_id = $1 and role = 'committee'`, [c5_committee.committee_id]
    )
    expect(c5_committee_contact_committee.email).toBe(c5_report.committee.email)
    expect(c5_committee_contact_committee.name).toBe(c5_report.committee.name)
    expect(c5_committee_contact_committee.address).toBe(c5_report.committee.address)
    expect(c5_committee_contact_committee.city).toBe(c5_report.committee.city)
    expect(c5_committee_contact_committee.state).toBe(c5_report.committee.state)
    expect(c5_committee_contact_committee.postcode).toBe(c5_report.committee.postcode)

    // test committee_contact - officer
    const c5_committee_contact_officer = await fetch(
        `select * from committee_contact where committee_id = $1 and role ='officer'`, [c5_committee.committee_id]
    )
    expect(c5_committee_contact_officer.email).toBe(c5_report.admin_data.officers[0].email)
    expect(c5_committee_contact_officer.name).toBe(c5_report.admin_data.officers[0].name)
    expect(c5_committee_contact_officer.title).toBe(c5_report.admin_data.officers[0].title)
    expect(c5_committee_contact_officer.address).toBe(c5_report.admin_data.officers[0].address)
    expect(c5_committee_contact_officer.city).toBe(c5_report.admin_data.officers[0].city)
    expect(c5_committee_contact_officer.state).toBe(c5_report.admin_data.officers[0].state)
    expect(c5_committee_contact_officer.postcode).toBe(c5_report.admin_data.officers[0].postcode)
    expect(c5_committee_contact_officer.phone).toBe(c5_report.admin_data.officers[0].phone)

    // test fund
    const c5_fund = await fetch(`select * from fund where filer_id = $1`, [filer_id])
    expect(c5_fund.election_code).toBe(c5_report.election_year)
    expect(c5_fund.vendor).toBe('PDC')
    expect(c5_fund.entity_id).toBe(c5_entity.entity_id)
    expect(c5_fund.target_id).toBe(c5_committee.committee_id)

    // test report
    const c5_submitted_report = await fetch(`select * from report where filer_name = $1`, [c5_report.committee.name])
    expect(c5_submitted_report.superseded_id).toBe(c5_report.superseded_id)
    expect(c5_submitted_report.report_type).toBe(c5_report.report_type)
    expect((c5_submitted_report.submitted_at).toISOString().slice(0,10)).toBe(c5_report.submitted_at)
    expect(c5_submitted_report.fund_id).toBe(c5_fund.fund_id)
    expect(c5_submitted_report.form_type).toBe('C5')
    expect(c5_submitted_report.election_year).toBe(c5_report.election_year)
    expect(c5_submitted_report.treasurer_name).toBe(c5_report.treasurer_name)
    expect(c5_submitted_report.treasurer_phone).toBe(null)
    expect(c5_submitted_report.user_data).toBe(null)
    expect(c5_submitted_report.source).toBe('PDC')
    expect(c5_submitted_report.version).toBe(c5_report.version)
    expect(c5_submitted_report.metadata).toBe(JSON.stringify(c5_report.metadata))
    expect(c5_submitted_report.ax_legacy_docid).toBe(c5_report.ax_legacy_docid)
    expect(c5_submitted_report.period_start).toBe('2023-12-01')
    expect(c5_submitted_report.period_end).toBe('2023-12-31')
    expect(c5_submitted_report.filing_method).toBe('P')
    expect(c5_submitted_report.address1).toBe(c5_report.committee.address)
    expect(c5_submitted_report.city).toBe(c5_report.committee.city)
    expect(c5_submitted_report.state).toBe(c5_report.committee.state)
    expect(c5_submitted_report.postcode).toBe(c5_report.committee.postcode)

    // test registration
    const c5_registration = await fetch(`select * from registration where committee_id = $1`, [c5_committee.committee_id])
    expect(c5_registration.name).toBe(c5_report.committee.name)
    expect(c5_registration.source).toBe('c5')
    expect(c5_registration.verified).toBe(true)
    expect(c5_registration.submitted_at.toISOString().slice(0,10)).toBe(c5_report.submitted_at)
    c5_report.admin_data.report = c5_submitted_report.report_id
    expect(JSON.parse(c5_registration.admin_data)).toStrictEqual(c5_report.admin_data)

    // test committee_log
    const c5_committee_log = await fetch(`select * from committee_log where committee_id = $1 and action='data-entered registration'`, [c5_committee.committee_id])
    expect(c5_committee_log.transaction_type).toBe('committee')
    expect(c5_committee_log.transaction_id).toBe(c5_registration.registration_id)
    expect(c5_committee_log.message).toBe(`created c5 report number ${c5_registration.registration_id} for committee name Marlaaaaa`)
    expect(c5_committee_log.user_name).toBe('test@test.com')

    // test expense - CA
    const c5_expense_ca = await fetch(`select * from c5_expense where report_id = $1 and type = 'CA'`, [c5_submitted_report.report_id])
    expect(c5_expense_ca.name).toBe(c5_report.expenses.candidate[0].name)
    expect(c5_expense_ca.office).toBe(c5_report.expenses.candidate[0].office)
    expect(c5_expense_ca.party).toBe(c5_report.expenses.candidate[0].party)
    expect(c5_expense_ca.expense_date).toBe(c5_report.expenses.candidate[0].expense_date)
    expect(c5_expense_ca.amount).toBe(c5_report.expenses.candidate[0].amount)
    expect(c5_expense_ca.type).toBe('CA')

    // test expense - PROP
    const c5_expense_prop = await fetch(`select * from c5_expense where report_id = $1 and type = 'PROP'`, [c5_submitted_report.report_id])
    expect(c5_expense_prop.name).toBe(c5_report.expenses.ballot[0].name)
    expect(c5_expense_prop.ballot_number).toBe(c5_report.expenses.ballot[0].ballot_number)
    expect(c5_expense_prop.for_or_against).toBe(c5_report.expenses.ballot[0].for_or_against)
    expect(c5_expense_prop.expense_date).toBe(c5_report.expenses.ballot[0].expense_date)
    expect(c5_expense_prop.amount).toBe(c5_report.expenses.ballot[0].amount)
    expect(c5_expense_prop.address).toBe(c5_report.expenses.ballot[0].address)
    expect(c5_expense_prop.city).toBe(c5_report.expenses.ballot[0].city)
    expect(c5_expense_prop.state).toBe(c5_report.expenses.ballot[0].state)
    expect(c5_expense_prop.postcode).toBe(c5_report.expenses.ballot[0].postcode)
    expect(c5_expense_prop.type).toBe('PROP')

    // test expense - OTH
    const c5_expense_oth = await fetch(`select * from c5_expense where report_id = $1 and type = 'OTH'`, [c5_submitted_report.report_id])
    expect(c5_expense_oth.name).toBe(c5_report.expenses.other[0].name)
    expect(c5_expense_oth.address).toBe(c5_report.expenses.other[0].address)
    expect(c5_expense_oth.city).toBe(c5_report.expenses.other[0].city)
    expect(c5_expense_oth.state).toBe(c5_report.expenses.other[0].state)
    expect(c5_expense_oth.postcode).toBe(c5_report.expenses.other[0].postcode)
    expect(c5_expense_oth.purpose).toBe(c5_report.expenses.other[0].purpose)
    expect(c5_expense_oth.expense_date).toBe(c5_report.expenses.other[0].expense_date)
    expect(c5_expense_oth.amount).toBe(c5_report.expenses.other[0].amount)
    expect(c5_expense_oth.type).toBe('OTH')
  })
})

describe('Test Submit Report with existing committee', () => {
  test('Test submit latest C5 report same fund', async () => {

    //Create committee report grab committee_id and report_id
    let c5_report = await readFile(`src/tests/data/test-c5-submit.json`, 'ascii')
    c5_report = JSON.parse(c5_report)
    const original_report_id = await c5Processor.submitReport(c5_report, 'test@test.com')
    const {committee_id} = await fetch(`select c.committee_id from report r join fund f on r.fund_id = f.fund_id join committee c on f.committee_id = c.committee_id where r.report_id = $1`, [original_report_id])

    //New submission changed (committee address, officer name)
    let new_submission = await readFile(`src/tests/data/test-c5-latest-submit.json`, 'ascii')
    new_submission = JSON.parse(new_submission)
    const new_report_id = await c5Processor.submitReport(new_submission, 'test@test.com', committee_id)

    //check for the same fund
    const old_report = await fetch(`select * from report where report_id = $1`, [original_report_id])
    const new_report = await fetch(`select * from report where report_id = $1`, [new_report_id])
    expect(new_report.fund_id).toBe(old_report.fund_id)

    //check for changes in the committee (registration_id)
    const new_registration = await fetch(`select * from registration where committee_id = $1 and admin_data::json->>'report' = $2 and admin_data is not null`, [committee_id, new_report_id])
    const committee_info = await fetch(`select c.registration_id, cc.address from committee c join committee_contact cc on c.committee_id = cc.committee_id where c.committee_id = $1 and role='committee'`, [committee_id])
    expect(committee_info.registration_id).toBe(new_registration.registration_id)
    expect(committee_info.address).toBe(new_report.address1)

    //changes in committee_contact (Changes to officers)
    const officers_info = await query(`select c.* from committee_contact c where c.committee_id = $1 and c.role = 'officer'`, [committee_id])
    expect(officers_info.length).toBeGreaterThanOrEqual(1)
    expect(officers_info[0].name).toBe(new_submission.admin_data.officers[0].name)

    //change in entity (name)
    const entity_info = await fetch(`select e.* from entity e join committee c on c.person_id = e.entity_id where c.committee_id = $1`, [committee_id])
    expect(entity_info.address).toBe(new_report.address1)
  })

  test('Test submit non-latest C5 report different fund', async () => {
    //Create committee report grab committee_id and report_id
    let c5_report = await readFile(`src/tests/data/test-c5-submit.json`, 'ascii')
    c5_report = JSON.parse(c5_report)
    const original_report_id = await c5Processor.submitReport(c5_report, 'test@test.com')
    const {committee_id} = await fetch(`select c.committee_id from report r join fund f on r.fund_id = f.fund_id join committee c on f.committee_id = c.committee_id where r.report_id = $1`, [original_report_id])
    const original_officers_info = await query(`select c.contact_id from committee_contact c where c.committee_id = $1 and c.role = 'officer'`, [committee_id])
    const original_entity_info = await fetch(`select e.* from entity e join committee c on c.person_id = e.entity_id where c.committee_id = $1`, [committee_id])

    //New submission changed (committee address, officer name)
    let new_submission = await readFile(`src/tests/data/test-c5-earlier-submit.json`, 'ascii')
    new_submission = JSON.parse(new_submission)
    const new_report_id = await c5Processor.submitReport(new_submission, 'test@test.com', committee_id)

    //check for the same fund
    const old_report = await fetch(`select * from report where report_id = $1`, [original_report_id])
    const new_report = await fetch(`select * from report where report_id = $1`, [new_report_id])
    expect(new_report.fund_id).not.toBe(old_report.fund_id)

    //check for changes in the committee (registration_id)
    const new_registration = await fetch(`select * from registration where committee_id = $1 and admin_data::json->>'report' = $2 and admin_data is not null`, [committee_id, new_report_id])
    const committee_info = await fetch(`select c.*, cc.* from committee c join committee_contact cc on c.committee_id = cc.committee_id where c.committee_id = $1 and cc.role = 'committee'`, [committee_id])
    expect(committee_info.registration_id).not.toBe(new_registration.registration_id)
    expect(committee_info.address).not.toBe(new_report.address1)

    //No changes made to the officer due to being an earlier report
    const new_officers_info = await query(`select c.contact_id from committee_contact c where c.committee_id = $1 and c.role = 'officer'`, [committee_id])
    expect(original_officers_info[0].contact_id).toBe(new_officers_info[0].contact_id)

    //No change to the entity
    const new_entity_info = await fetch(`select e.* from entity e join committee c on c.person_id = e.entity_id where c.committee_id = $1`, [committee_id])
    expect(new_entity_info.address).toBe(original_entity_info.address)
  })
})

describe('Test C5', () => {
  test('Test delete the only C5 report for a fund', async () => {

    const report = JSON.parse(await readFile(`src/tests/data/test-c5-submit.json`, 'ascii'))
    const savedReport = await c5Processor.submitReport(report)
    expect(savedReport).toBeTruthy()

    /**
     * @type {{committee_id: int, fund_id: int, report_id: int, registration_id: int}}
     */
    const reportInfo = await _getReportInfo(savedReport)

    // Delete report and ensure related registration, officials and expense records are also deleted
    // as this is the only report for the fund
    await c5Processor.deleteReport(reportInfo.report_id, 'test@test.com')
    const deleted_report = await fetch(`select *
                                          from report
                                          where report_id = $1`, [reportInfo.report_id])
    expect(deleted_report).toBeNull()

    const deleted_committee = await fetch(`select *
        from committee where committee_id = $1`, [reportInfo.committee_id])
    expect(deleted_committee).toBeNull()

    const deleted_fund = await fetch(`select *
                                      from fund
                                      where fund_id = $1`, [reportInfo.fund_id])
    expect(deleted_fund).toBeNull()

    const deleted_registration = await fetch(`select *
                                              from registration
                                              where registration_id = $1`, [reportInfo.registration_id])
    expect(deleted_registration).toBeNull()

    const deleted_official = await fetch(`select *
                                          from committee_contact
                                          where committee_id = $1`, [reportInfo.committee_id])
    expect(deleted_official).toBeNull()

    const deleted_expense = await fetch(`select *
                                          from c5_expense
                                          where report_id = $1`, [reportInfo.report_id])
    expect(deleted_expense).toBeNull()

    const expectedMessage = `%deleted c5 report number ${savedReport}%`
    const committee_log = await fetch(
      `select *
       from committee_log
       where committee_id = $1 and message like $2`, [reportInfo.committee_id, expectedMessage])
    expect(committee_log).toBeTruthy()
  })
})

/**
 * This test is for two reports on the same fund where the most recent report is deleted. It ensures that the committee
 * information, including registration and contacts are updated to reflect the information that was provided on the
 * previous (new most recent) report.
 */
test('Test delete the latest C5 report for a fund', async () => {
  let reportOne = JSON.parse(await readFile(`src/tests/data/test-c5-submit.json`, 'ascii'))
  const savedReportOne = await c5Processor.submitReport(reportOne)
  /**
   * @type {{committee_id: int, fund_id: int, report_id: int, registration_id: int}}
   */
  const reportInfoOne = await _getReportInfo(savedReportOne)

  let reportTwo = JSON.parse(await readFile(`src/tests/data/test-c5-submit.json`, 'ascii'))
  reportTwo.committee.name += 'Two'
  reportTwo.admin_data.officers[0].name += 'Two'
  const savedReportTwo = await c5Processor.submitReport(reportTwo)

  await c5Processor.deleteReport(savedReportTwo, 'test@test.com')

  // Fund should still exist
  const fund = await fetch
  (`select fund_id
    from fund
    where committee_id = $1`, [reportInfoOne.committee_id])
  expect(fund).toBeTruthy()

  // Committee registration should point at first registration
  const registration = await fetch
  (`select registration_id
    from committee
    where committee_id = $1`, [reportInfoOne.committee_id])
  expect(registration.registration_id).toEqual(reportInfoOne.registration_id)

  // Committee name should be original name
  const committeeName = (await fetch(
      `select name from committee where committee_id = $1`, [reportInfoOne.committee_id])).name
  expect(committeeName).toEqual(reportOne.committee.name)

  // Committee contact name should be original contact name
  const officerName = (await fetch(
      `select name from committee_contact where committee_id = $1 and role = 'officer'`,
      [reportInfoOne.committee_id])).name
  expect(officerName).toEqual(reportOne.admin_data.officers[0].name)
})

/**
 * Given a committee/fund that has more than one report, when a report that is not the last report is deleted, the
 * report and the registration should be deleted but the committee, officers and fund should remain the same.
 */
test('Test delete the next-to-last C5 report for a fund', async () => {
  let reportOne = JSON.parse(await readFile(`src/tests/data/test-c5-submit.json`, 'ascii'))
  const savedReportOne = await c5Processor.submitReport(reportOne)
  /**
   * @type {{committee_id: int, fund_id: int, report_id: int, registration_id: int}}
   */
  const reportInfoOne = await _getReportInfo(savedReportOne)

  let reportTwo = JSON.parse(await readFile(`src/tests/data/test-c5-submit.json`, 'ascii'))
  reportTwo.committee.name += 'Two'
  reportTwo.admin_data.officers[0].name += 'Two'
  const savedReportTwo = await c5Processor.submitReport(reportTwo)
  /**
   * @type {{committee_id: int, fund_id: int, report_id: int, registration_id: int}}
   */
  const reportInfoTwo = await _getReportInfo(savedReportTwo)

  await c5Processor.deleteReport(savedReportOne, 'test@test.com')

  // Fund should still exist
  const fund = await fetch
  (`select fund_id
    from fund
    where committee_id = $1`, [reportInfoTwo.committee_id])
  expect(fund).toBeTruthy()

  // Committee registration should point at second registration
  const registration = await fetch
  (`select registration_id
    from committee
    where committee_id = $1`, [reportInfoTwo.committee_id])
  expect(registration.registration_id).toEqual(reportInfoTwo.registration_id)

  // Committee name should be maintained from second report
  const committeeName = (await fetch(
      `select name from committee where committee_id = $1`, [reportInfoTwo.committee_id])).name
  expect(committeeName).toEqual(reportTwo.committee.name)

  // Committee contact name should be maintained from second report
  const officerName = (await fetch(
      `select name from committee_contact where committee_id = $1 and role = 'officer'`,
      [reportInfoTwo.committee_id])).name
  expect(officerName).toEqual(reportTwo.admin_data.officers[0].name)
})

describe('Test edit existing report', () => {
  test('Test edit report to earlier fund', async () => {
    //create two reports
    //Create committee report grab committee_id and report_id
    let original_report = await readFile(`src/tests/data/test-c5-submit.json`, 'ascii')
    original_report = JSON.parse(original_report)
    const original_report_id = await c5Processor.submitReport(original_report, 'test@test.com')
    const {committee_id} = await fetch(`select c.committee_id from report r join fund f on r.fund_id = f.fund_id join committee c on f.committee_id = c.committee_id where r.report_id = $1`, [original_report_id])
    let second_report = await readFile(`src/tests/data/test-c5-earlier-same-fund-submit.json`, 'ascii')
    second_report = JSON.parse(second_report)
    const second_report_id = await c5Processor.submitReport(second_report, 'test@test.com', committee_id)
    //grab original fund count
    const funds1 = await query(`select f.* from fund f where f.committee_id = $1`, [committee_id])
    const original_report_info = await fetch(`select * from report where report_id = $1`, [second_report_id])

    //Edit the second report, change the name, fund to an earlier year
    let edit_earlier_report = await readFile(`src/tests/data/test-c5-edit-earlier-submit.json`, 'ascii')
    edit_earlier_report = JSON.parse(edit_earlier_report)
    await c5Processor.submitReport(edit_earlier_report, 'test@test.com', committee_id, second_report_id)
    //grab the total reports and new fund count
    const reports = await query(`select r.* from report r join fund f on f.fund_id = r.fund_id where f.committee_id = $1`, [committee_id])
    const funds2 = await query(`select f.* from fund f where f.committee_id = $1`, [committee_id])
    const edit_report_info = await fetch(`select * from report where report_id = $1`, [second_report_id])
    const committee_info = await fetch(`select e.name as entity_name, cc.name as committee_contact_name, c.* from committee c 
                                            join committee_contact cc on cc.committee_id = c.committee_id and cc.role = 'committee' 
                                            join entity e on e.entity_id = c.person_id 
                                            where c.committee_id = $1`, [committee_id])

    //should still only be two
    expect(reports.length).toBe(2)
    //now have two funds
    expect(funds2.length).not.toBe(funds1.length)
    expect(funds2.length).toBe(2)
    //second report should have different fund, with new election_code
    const new_fund = await fetch(`select * from fund where fund_id = $1`, [edit_report_info.fund_id])
    expect(edit_report_info.fund_id).not.toBe(original_report_info.fund_id)
    expect(new_fund.election_code).toBe(edit_earlier_report.election_year)
    //check for changes in the report but not in the committee, entity, or committee_contact
    expect(edit_report_info.filer_name).not.toBe(original_report_info.filer_name)
    expect(edit_report_info.treasurer_name).not.toBe(original_report_info.treasurer_name )
    expect(committee_info.name).not.toBe(edit_report_info.filer_name)
    expect(committee_info.name).toBe(original_report_info.filer_name)
    expect(committee_info.entity_name).not.toBe(edit_report_info.filer_name)
    expect(committee_info.entity_name).toBe(original_report_info.filer_name)
    expect(committee_info.committee_contact_name).not.toBe(edit_report_info.filer_name)
    expect(committee_info.committee_contact_name).toBe(original_report_info.filer_name)
  })

  test('Test edit report latest C5 report', async () => {
    //create two reports
    //Create committee report grab committee_id and report_id
    let original_report = await readFile(`src/tests/data/test-c5-submit.json`, 'ascii')
    original_report = JSON.parse(original_report)
    const original_report_id = await c5Processor.submitReport(original_report, 'test@test.com')
    const {committee_id} = await fetch(`select c.committee_id from report r join fund f on r.fund_id = f.fund_id join committee c on f.committee_id = c.committee_id where r.report_id = $1`, [original_report_id])
    let second_report = await readFile(`src/tests/data/test-c5-latest-submit.json`, 'ascii')
    second_report = JSON.parse(second_report)
    const second_report_id = await c5Processor.submitReport(second_report, 'test@test.com', committee_id)
    //grab original fund count
    const funds1 = await query(`select f.* from fund f where f.committee_id = $1`, [committee_id])
    const original_report_info = await fetch(`select * from report where report_id = $1`, [second_report_id])

    //Edit the second report, change the name, fund to an earlier year
    let edit_latest_report = await readFile(`src/tests/data/test-c5-edit-latest-submit.json`, 'ascii')
    edit_latest_report = JSON.parse(edit_latest_report)
    await c5Processor.submitReport(edit_latest_report, 'test@test.com', committee_id, second_report_id)
    //grab the total reports and new fund count
    const reports = await query(`select r.* from report r join fund f on f.fund_id = r.fund_id where f.committee_id = $1`, [committee_id])
    const funds2 = await query(`select f.* from fund f where f.committee_id = $1`, [committee_id])
    const edit_report_info = await fetch(`select * from report where report_id = $1`, [second_report_id])
    const committee_info = await fetch(`select e.name as entity_name, cc.name as committee_contact_name, c.* from committee c 
                                            join committee_contact cc on cc.committee_id = c.committee_id and cc.role = 'committee' 
                                            join entity e on e.entity_id = c.person_id 
                                            where c.committee_id = $1`, [committee_id])

    //should still only be two
    expect(reports.length).toBe(2)
    //still only have 1 fund
    expect(funds2.length).toBe(funds1.length)
    //second report still have same fund
    expect(edit_report_info.fund_id).toBe(original_report_info.fund_id)
    //check for changes in the report, committee, entity, and committee_contact
    expect(edit_report_info.filer_name).not.toBe(original_report_info.filer_name)
    expect(committee_info.name).toBe(edit_report_info.filer_name)
    expect(committee_info.name).not.toBe(original_report_info.filer_name)
    expect(committee_info.entity_name).toBe(edit_report_info.filer_name)
    expect(committee_info.entity_name).not.toBe(original_report_info.filer_name)
    expect(committee_info.committee_contact_name).toBe(edit_report_info.filer_name)
    expect(committee_info.committee_contact_name).not.toBe(original_report_info.filer_name)
  })
})

describe('C5 verification request', () => {
  test('Test revert a C5 verification request', async () => {
    // create the filer_request
    const fake_claims = {uid: '-2', realm: '-3', email: 'test@test.com'}
    let c5_registration = await readFile(`src/tests/data/filer/test-c5-draft-registration.json`, 'ascii')
    c5_registration = JSON.parse(c5_registration)
    expect(c5_registration).toBeTruthy()
    const id = await filerC5Processor.createRegistration(c5_registration, fake_claims)
    const draft_registration = await filerC5Processor.getFilerRequestData(id)
    expect(draft_registration).toStrictEqual(c5_registration)

    // revert the filer_request
    await c5Processor.revertRequest(id)
    const request = await query(`select * from filer_request where id = $1`, [id])
    expect(request.length).toBe(0)
  })
  test('Test sending email', async () => {
    const fake_claims = {uid: '-2', realm: '-3', email: 'test@test.com', user_name: 'test@test.com'}

    jest.mock('@sendgrid/mail')
    let last_message
    sgMail.send = jest.fn().mockImplementation((message) => {
      last_message = message;
    })

    await filerC5Processor.sendVerificationEmail('pdc@pdc.wa.gov')

    expect(last_message).toMatchObject({
      from: 'pdc@pdc.wa.gov',
      to: 'pdc@pdc.wa.gov',
      templateId: 'd-9848aaef94464cc4b16e89bf7d6f2aca',
      dynamicTemplateData: {}
    })
  })
})
