import env from 'dotenv'
import { readFile } from 'fs/promises'
import { beginTransaction, rollback, connect, close, fetch, execute, query } from '@wapdc/common/wapdc-db'
import { jest, beforeEach, afterEach, beforeAll, afterAll, expect, test, describe } from '@jest/globals'
import { filerC5Processor } from '../../FilerC5Processor.js'
import { validateFilerReport, getImportRows } from '../../ValidateFilerReport.js'
import sgMail from '@sendgrid/mail'

env.config()
jest.setTimeout(2000000000)

beforeAll(async () => {
  await connect()
})

afterAll(async () => {
  await close()
})

beforeEach(async () => {
  await beginTransaction()
})

afterEach(async () => {
  await rollback()
})

describe('Test C5 registration', () => {
  describe('Test C5 draft registration', () => {
    test('Test create C5 registration draft', async () => {
      const fake_claims = {uid: '-2', realm: '-3', email: 'test@test.com', user_name: 'test_user'}
      let c5_registration = await readFile(`src/tests/data/filer/test-c5-draft-registration.json`, 'ascii')
      c5_registration = JSON.parse(c5_registration)
      expect(c5_registration).toBeTruthy()
      const id = await filerC5Processor.createRegistration(c5_registration, fake_claims)
      const draft_registration = await filerC5Processor.getFilerRequestData(id)
      expect(draft_registration).toStrictEqual(c5_registration)
    })
    test('Test delete C5 registration filer request', async () => {
      let c5_registration = await readFile(`src/tests/data/filer/test-c5-draft-registration.json`, 'ascii')
      c5_registration = JSON.parse(c5_registration)
      expect(c5_registration).toBeTruthy()
      const draft_id = await filerC5Processor.createRegistration(c5_registration, '-2', '-3')
      await filerC5Processor.deleteUnverifiedRegistration(draft_id)
      const deleted_registration = await fetch(`select *
                                              from filer_request
                                              where id = $1`, [draft_id])
      expect(deleted_registration).toBeNull()
    })
  })
  test('Test submit C5 registration validation', async () => {
    const fixed_c5_registration = await readFile(`src/tests/data/filer/test-c5-draft-registration.json`, 'ascii')
    let c5_registration = JSON.parse(fixed_c5_registration)

    let c5_registration_validations = await readFile('src/tests/data/filer/test-c5-submit-registration-validations.json', 'ascii')
    c5_registration_validations = JSON.parse(c5_registration_validations)

    let registration_draft_id = await filerC5Processor.createRegistration(c5_registration, '-2', '-3')

    for (const expected_validation of c5_registration_validations) {
      c5_registration = JSON.parse(fixed_c5_registration)
      eval(expected_validation.modification)
      const resulting_validation = filerC5Processor.validateRegistration(c5_registration, registration_draft_id)
      expect(resulting_validation).toBe(expected_validation.error)
    }
    c5_registration = JSON.parse(fixed_c5_registration)
  })
  test('Test submit C5 registration', async () => {
    // create a draft
    const fake_claims = {uid: '-2', realm: '-3', email: 'test@test.com', user_name: 'test_user'}
    const fake_admin_claims = {uid: '-3', realm: '-4', email: 'test@admin.com', user_name: 'test_admin'}
    let c5_registration = await readFile(`src/tests/data/filer/test-c5-draft-registration.json`, 'ascii')
    c5_registration = JSON.parse(c5_registration)

    // mock sendgrid mail
    jest.mock('@sendgrid/mail')
    let last_message
    sgMail.send = jest.fn().mockImplementation((message) => {
      last_message = message;
    })

    const draft_id = await filerC5Processor.createRegistration(c5_registration, fake_claims)
    const draft_registration = await filerC5Processor.getFilerRequestData(draft_id)
    await execute(`insert into pdc_user (uid, realm, user_name) values ('-2', '-3', 'pdc@pdc.wa.gov')`)

    // load the draft and submit the registration
    c5_registration = draft_registration
    await filerC5Processor.submitRegistration(draft_id, fake_admin_claims)

    // test entity
    const c5_entity = await fetch(`select * from entity where name = $1`, [c5_registration.committee.name])
    const filer_id = 'E-' + c5_entity.entity_id

    expect(c5_entity.name).toBe(c5_registration.committee.name)
    expect(c5_entity.address).toBe(c5_registration.committee.address)
    expect(c5_entity.city).toBe(c5_registration.committee.city)
    expect(c5_entity.state).toBe(c5_registration.committee.state)
    expect(c5_entity.postcode).toBe(c5_registration.committee.postcode)
    expect(c5_entity.is_person).toBe(false)
    expect(c5_entity.filer_id).toBe(filer_id)

    // test committee
    const c5_committee = await fetch(`select * from committee where name = $1`, [c5_registration.committee.name])
    expect(c5_committee.name).toBe(c5_registration.committee.name)
    expect(c5_committee.filer_id).toBe(filer_id)
    expect(c5_committee.committee_type).toBe('OS')
    expect(c5_committee.pac_type).toBe('out-of-state')
    expect(c5_committee.continuing).toBe(true)
    expect(c5_committee.person_id).toBe(c5_entity.entity_id)
    expect(c5_committee.election_code).toBe(new Date().toISOString().substring(0,4))
    expect(c5_committee.start_year).toBe(parseInt(new Date().toISOString().substring(0,4)))

    // test committee_contact - committee
    const c5_committee_contact_committee = await fetch(
      `select * from committee_contact where committee_id = $1 and role = 'committee'`, [c5_committee.committee_id]
    )
    expect(c5_committee_contact_committee.email).toBe('test@test.com')
    expect(c5_committee_contact_committee.name).toBe(c5_registration.committee.name)
    expect(c5_committee_contact_committee.address).toBe(c5_registration.committee.address)
    expect(c5_committee_contact_committee.city).toBe(c5_registration.committee.city)
    expect(c5_committee_contact_committee.state).toBe(c5_registration.committee.state)
    expect(c5_committee_contact_committee.postcode).toBe(c5_registration.committee.postcode)

    // test committee_contact - officer
    const c5_committee_contact_officer = await fetch(
      `select * from committee_contact where committee_id = $1 and role = 'officer'`, [c5_committee.committee_id]
    )
    expect(c5_committee_contact_officer.email).toBe(c5_registration.officers[0].email)
    expect(c5_committee_contact_officer.name).toBe(c5_registration.officers[0].name)
    expect(c5_committee_contact_officer.title).toBe(c5_registration.officers[0].title)
    expect(c5_committee_contact_officer.address).toBe(c5_registration.officers[0].address)
    expect(c5_committee_contact_officer.city).toBe(c5_registration.officers[0].city)
    expect(c5_committee_contact_officer.state).toBe(c5_registration.officers[0].state)
    expect(c5_committee_contact_officer.postcode).toBe(c5_registration.officers[0].postcode)
    expect(c5_committee_contact_officer.phone).toBe(c5_registration.officers[0].phone)

    // test fund
    const c5_fund = await fetch(`select * from fund where filer_id = $1`, [filer_id])
    expect(c5_fund.election_code).toBe(new Date().toISOString().substring(0,4))
    expect(c5_fund.vendor).toBe('PDC')
    expect(c5_fund.entity_id).toBe(c5_entity.entity_id)
    expect(c5_fund.target_id).toBe(c5_committee.committee_id)

    // test registration
    const c5_filer_registration = await fetch(`select * from registration where committee_id = $1`, [c5_committee.committee_id])
    expect(c5_filer_registration.name).toBe(c5_registration.committee.name)
    expect(c5_filer_registration.source).toBe('c5')
    expect(c5_filer_registration.verified).toBe(true)
    expect(c5_filer_registration.submitted_at.toISOString().slice(0,10)).toBe(new Date().toISOString().substring(0,10))
    expect(JSON.parse(c5_filer_registration.user_data)).toStrictEqual(c5_registration)

    // test committee_log
    const c5_committee_log = await fetch(`select * from committee_log where committee_id = $1`, [c5_committee.committee_id])
    expect(c5_committee_log.transaction_type).toBe('committee')
    expect(c5_committee_log.transaction_id).toBe(c5_filer_registration.registration_id)
    expect(c5_committee_log.action).toBe('create c5 registration')
    expect(c5_committee_log.message).toBe(`created c5 registration number ${c5_filer_registration.registration_id} for committee name Nebraska Distilleries for Moderates`)
    expect(c5_committee_log.user_name).toBe('test_admin')

    // test that the draft was deleted
    const deleted_draft = await fetch(`select * from filer_request where id = $1`, [draft_id])
    expect(deleted_draft).toBeNull()
  })

  test('Test amend C5 registration', async () => {
    // create a draft
    const fake_claims = {uid: '-2', realm: '-3', email: 'test@test.com', user_name: 'test_user'}
    const fake_admin_claims = {uid: '-3', realm: '-4', email: 'test@admin.com', user_name: 'test_admin'}
    let c5_registration = await readFile(`src/tests/data/filer/test-c5-draft-registration.json`, 'ascii')
    c5_registration = JSON.parse(c5_registration)

    // mock sendgrid mail
    jest.mock('@sendgrid/mail')
    let last_message
    sgMail.send = jest.fn().mockImplementation((message) => {
      last_message = message;
    })

    let draft_id = await filerC5Processor.createRegistration(c5_registration, fake_claims)
    let draft_registration = await filerC5Processor.getFilerRequestData(draft_id)
    await execute(`insert into pdc_user (uid, realm, user_name) values ('-2', '-3', 'pdc@pdc.wa.gov')`)

    // load the draft and submit the registration
    c5_registration = draft_registration
    await filerC5Processor.submitRegistration(draft_id, fake_admin_claims)

    // test entity
    let c5_entity = await fetch(`select * from entity where name = $1`, [c5_registration.committee.name])
    let filer_id = 'E-' + c5_entity.entity_id
    expect(c5_entity.name).toBe(c5_registration.committee.name)
    expect(c5_entity.address).toBe(c5_registration.committee.address)

    // test committee
    let c5_committee = await fetch(`select * from committee where name = $1`, [c5_registration.committee.name])
    expect(c5_committee.name).toBe(c5_registration.committee.name)
    expect(c5_committee.start_year).toBe(parseInt(new Date().toISOString().substring(0,4)))

    // test committee_contact - committee
    let c5_committee_contact_committee = await fetch(
      `select * from committee_contact where committee_id = $1 and role = 'committee'`, [c5_committee.committee_id]
    )
    expect(c5_committee_contact_committee.email).toBe('test@test.com')
    expect(c5_committee_contact_committee.address).toBe(c5_registration.committee.address)

    // test committee_contact - officer
    let c5_committee_contact_officer = await fetch(
      `select * from committee_contact where committee_id = $1 and role = 'officer'`, [c5_committee.committee_id]
    )
    expect(c5_committee_contact_officer.name).toBe(c5_registration.officers[0].name)
    expect(c5_committee_contact_officer.title).toBe(c5_registration.officers[0].title)

    // test fund
    let c5_fund = await fetch(`select * from fund where filer_id = $1`, [filer_id])
    expect(c5_fund.entity_id).toBe(c5_entity.entity_id)
    expect(c5_fund.target_id).toBe(c5_committee.committee_id)

    // test registration
    let c5_filer_registration = await fetch(`select * from registration where committee_id = $1`, [c5_committee.committee_id])
    expect(c5_filer_registration.name).toBe(c5_registration.committee.name)
    expect(JSON.parse(c5_filer_registration.user_data)).toStrictEqual(c5_registration)

    // test committee_log
    let c5_committee_log = await fetch(`select * from committee_log where committee_id = $1`, [c5_committee.committee_id])
    expect(c5_committee_log.message).toBe(`created c5 registration number ${c5_filer_registration.registration_id} for committee name Nebraska Distilleries for Moderates`)
    expect(c5_committee_log.user_name).toBe('test_admin')

    // test that the draft was deleted
    let deleted_draft = await fetch(`select * from filer_request where id = $1`, [draft_id])
    expect(deleted_draft).toBeNull()

    c5_registration.committee.name = 'Some other name'
    c5_registration.committee.address = '1234 Cool Street'
    c5_registration.officers[0].title = 'CEO maybe?'
    c5_registration.officers[0].name = 'Billy Bob Joe'

    // create an amendment draft
    draft_id = await filerC5Processor.createRegistration(c5_registration, fake_claims)
    draft_registration = await filerC5Processor.getFilerRequestData(draft_id)

    // load the draft and submit the registration
    c5_registration = draft_registration
    await filerC5Processor.submitRegistration(draft_id, fake_admin_claims)

    // test entity
    c5_entity = await fetch(`select * from entity where name = $1`, [c5_registration.committee.name])
    filer_id = 'E-' + c5_entity.entity_id
    expect(c5_entity.name).toBe('Some other name')
    expect(c5_entity.address).toBe('1234 Cool Street')

    // test committee
    c5_committee = await fetch(`select * from committee where name = $1`, [c5_registration.committee.name])
    expect(c5_committee.name).toBe('Some other name')

    // test committee_contact - committee
    c5_committee_contact_committee = await fetch(
      `select * from committee_contact where committee_id = $1 and role = 'committee'`, [c5_committee.committee_id]
    )
    expect(c5_committee_contact_committee.address).toBe('1234 Cool Street')

    // test committee_contact - officer
    c5_committee_contact_officer = await fetch(
      `select * from committee_contact where committee_id = $1 and role = 'officer'`, [c5_committee.committee_id]
    )
    expect(c5_committee_contact_officer.name).toBe('Billy Bob Joe')
    expect(c5_committee_contact_officer.title).toBe('CEO maybe?')

    // test registration
    c5_filer_registration = await fetch(`select * from registration where committee_id = $1`, [c5_committee.committee_id])
    expect(c5_filer_registration.name).toBe('Some other name')
    expect(JSON.parse(c5_filer_registration.user_data)).toStrictEqual(c5_registration)
  })

  test('Test merge C5 registration', async () => {
    // create a draft
    const fake_claims = {uid: '-2', realm: '-3', email: 'test@test.com', user_name: 'test_user'}
    const fake_admin_claims = {uid: '-3', realm: '-4', email: 'test@admin.com', user_name: 'test_admin'}
    let c5_registration = await readFile(`src/tests/data/filer/test-c5-draft-registration.json`, 'ascii')
    c5_registration = JSON.parse(c5_registration)

    // mock sendgrid mail
    jest.mock('@sendgrid/mail')
    let last_message
    sgMail.send = jest.fn().mockImplementation((message) => {
      last_message = message;
    })

    let draft_id = await filerC5Processor.createRegistration(c5_registration, fake_claims)
    let draft_registration = await filerC5Processor.getFilerRequestData(draft_id)
    await execute(`insert into pdc_user (uid, realm, user_name) values ('-2', '-3', 'pdc@pdc.wa.gov')`)

    // load the draft and submit the registration
    c5_registration = draft_registration
    await filerC5Processor.submitRegistration(draft_id, fake_admin_claims)

    // test entity
    let c5_entity = await fetch(`select * from entity where name = $1`, [c5_registration.committee.name])
    let filer_id = 'E-' + c5_entity.entity_id
    expect(c5_entity.name).toBe(c5_registration.committee.name)
    expect(c5_entity.address).toBe(c5_registration.committee.address)

    // test committee
    let c5_committee = await fetch(`select * from committee where name = $1`, [c5_registration.committee.name])
    expect(c5_committee.name).toBe(c5_registration.committee.name)
    expect(c5_committee.start_year).toBe(parseInt(new Date().toISOString().substring(0,4)))

    // test committee_contact - committee
    let c5_committee_contact_committee = await fetch(
      `select * from committee_contact where committee_id = $1 and role = 'committee'`, [c5_committee.committee_id]
    )
    expect(c5_committee_contact_committee.email).toBe('test@test.com')
    expect(c5_committee_contact_committee.address).toBe(c5_registration.committee.address)

    // test committee_contact - officer
    let c5_committee_contact_officer = await fetch(
      `select * from committee_contact where committee_id = $1 and role = 'officer'`, [c5_committee.committee_id]
    )
    expect(c5_committee_contact_officer.name).toBe(c5_registration.officers[0].name)
    expect(c5_committee_contact_officer.title).toBe(c5_registration.officers[0].title)

    // test fund
    let c5_fund = await fetch(`select * from fund where filer_id = $1`, [filer_id])
    expect(c5_fund.entity_id).toBe(c5_entity.entity_id)
    expect(c5_fund.target_id).toBe(c5_committee.committee_id)

    // test registration
    let c5_filer_registration = await fetch(`select * from registration where committee_id = $1`, [c5_committee.committee_id])
    expect(c5_filer_registration.name).toBe(c5_registration.committee.name)
    expect(JSON.parse(c5_filer_registration.user_data)).toStrictEqual(c5_registration)

    // test committee_log
    let c5_committee_log = await fetch(`select * from committee_log where committee_id = $1`, [c5_committee.committee_id])
    expect(c5_committee_log.message).toBe(`created c5 registration number ${c5_filer_registration.registration_id} for committee name Nebraska Distilleries for Moderates`)
    expect(c5_committee_log.user_name).toBe('test_admin')

    // test that the draft was deleted
    let deleted_draft = await fetch(`select * from filer_request where id = $1`, [draft_id])
    expect(deleted_draft).toBeNull()

    c5_registration.committee.name = 'Some other name'
    c5_registration.committee.address = '1234 Cool Street'
    c5_registration.officers[0].title = 'CEO maybe?'
    c5_registration.officers[0].name = 'Billy Bob Joe'

    // create an amendment draft
    draft_id = await filerC5Processor.createRegistration(c5_registration, fake_claims)
    draft_registration = await filerC5Processor.getFilerRequestData(draft_id)

    // load the draft and submit the registration
    c5_registration = draft_registration
    await filerC5Processor.mergeRegistration(c5_committee.committee_id, draft_id, fake_admin_claims)

    // test entity
    c5_entity = await fetch(`select * from entity where name = $1`, [c5_registration.committee.name])
    filer_id = 'E-' + c5_entity.entity_id
    expect(c5_entity.name).toBe('Some other name')
    expect(c5_entity.address).toBe('1234 Cool Street')

    // test committee
    c5_committee = await fetch(`select * from committee where committee_id = $1`, [c5_committee.committee_id])
    expect(c5_committee.name).toBe('Some other name')

    // test committee_contact - committee
    c5_committee_contact_committee = await fetch(
      `select * from committee_contact where committee_id = $1 and role = 'committee'`, [c5_committee.committee_id]
    )
    expect(c5_committee_contact_committee.address).toBe('1234 Cool Street')

    // test committee_contact - officer
    c5_committee_contact_officer = await fetch(
      `select * from committee_contact where committee_id = $1 and role = 'officer'`, [c5_committee.committee_id]
    )
    expect(c5_committee_contact_officer.name).toBe('Billy Bob Joe')
    expect(c5_committee_contact_officer.title).toBe('CEO maybe?')

    // test registration
    c5_filer_registration = await fetch(`select * from registration where registration_id = $1`, [c5_committee.registration_id])
    expect(c5_filer_registration.name).toBe('Some other name')
    expect(JSON.parse(c5_filer_registration.user_data)).toStrictEqual(c5_registration)
  })

  test('Test create C5 report draft', async () => {
    const fake_claims = {uid: '-2', realm: '-3', email: 'test@test.com', user_name: 'test_user'}
    const fake_committee_id = 5
    let c5_report = await readFile(`src/tests/data/filer/test-c5-draft-report.json`, 'ascii')
    c5_report = JSON.parse(c5_report)
    expect(c5_report).toBeTruthy()
    const draft_id = await filerC5Processor.createDraftReport(c5_report, fake_committee_id, null, fake_claims)
    const draft_report = await filerC5Processor.getDraftReport(draft_id, fake_committee_id)
    expect(draft_report).toStrictEqual(c5_report)
  })
  test('Test delete C5 report draft', async () => {
    const fake_claims = {uid: '-2', realm: '-3', email: 'test@test.com', user_name: 'test_user'}
    const fake_committee_id = 5
    let c5_report = await readFile(`src/tests/data/filer/test-c5-draft-report.json`, 'ascii')
    c5_report = JSON.parse(c5_report)
    expect(c5_report).toBeTruthy()
    const draft_id = await filerC5Processor.createDraftReport(c5_report, fake_committee_id, null, fake_claims)
    await filerC5Processor.deleteDraftReport(draft_id)
    const deleted_draft = await fetch(`select * from private.draft_document where draft_id = $1`, [draft_id])
    expect(deleted_draft).toBeNull()
  })
  test('Test edit C5 report draft',  async() => {
    //create two reports
    //Create committee report grab committee_id and report_id
    const fake_claims = {uid: '-2', realm: '-3', email: 'test@test.com', user_name: 'test_user'}
    const fake_committee_id = 5
    let original_report = await readFile(`src/tests/data/filer/test-c5-draft-report.json`, 'ascii')
    original_report = JSON.parse(original_report)
    const original_report_id = await filerC5Processor.createDraftReport(original_report, fake_committee_id, null, fake_claims)
    let second_report = await readFile(`src/tests/data/filer/test-c5-edited-draft-report.json`, 'ascii')
    second_report = JSON.parse(second_report)
    await filerC5Processor.saveDraftReport(second_report, original_report_id)
    expect(second_report.candidate_expenses[0].name).toBe("The late Robert Paulson")
  })

  test('Test save C5 report draft', async () => {
    const fake_claims = {uid: '-2', realm: '-3', email: 'test@test.com', user_name: 'test_user'}
    const fake_committee_id = 5
    let c5_report = await readFile(`src/tests/data/filer/test-c5-draft-report.json`, 'ascii')
    c5_report = JSON.parse(c5_report)
    expect(c5_report).toBeTruthy()
    const report_draft_id = await filerC5Processor.createDraftReport(c5_report, fake_committee_id, null, fake_claims)
    let amended_c5_report = await readFile(`src/tests/data/filer/test-c5-amended-draft-report.json`, 'ascii')
    amended_c5_report = JSON.parse(amended_c5_report)
    await filerC5Processor.saveDraftReport(amended_c5_report, report_draft_id)
    const response = await filerC5Processor.getDraftReport(report_draft_id, fake_committee_id)
    expect(response.candidate_expenses[0]['name']).toEqual("The late Robert Paulson")
  })

  test('Test submit C5 report validation', async () => {
    const fake_claims = {uid: '-2', realm: '-3', email: 'test@test.com', user_name: 'test_user'}
    const fake_committee_id = 5
    const fixed_filer_report = await readFile(`src/tests/data/filer/test-c5-draft-report.json`, 'ascii')
    let filer_report = JSON.parse(fixed_filer_report)

    let filer_report_validations = await readFile('src/tests/data/filer/test-c5-submit-report-validations.json', 'ascii')
    filer_report_validations = JSON.parse(filer_report_validations)

    const report_draft_id = await filerC5Processor.createDraftReport(filer_report, fake_committee_id, null, fake_claims)
    const report_to_submit = await filerC5Processor.getDraftReport(report_draft_id, fake_committee_id)
    const report_id = await filerC5Processor.submitDraftReport(report_to_submit, fake_committee_id, report_draft_id, fake_claims)

    for (const expected_validation of filer_report_validations) {
      filer_report = JSON.parse(fixed_filer_report)
      eval(expected_validation.modification)
      const resulting_validations = validateFilerReport(filer_report, report_draft_id)
      expect(resulting_validations[0]).toBe(expected_validation.error)
    }
  })

  test('Test submit C5 report draft', async () => {
    // create a draft registration
    let c5_registration = await readFile(`src/tests/data/filer/test-c5-draft-registration.json`, 'ascii')
    c5_registration = JSON.parse(c5_registration)

    // mock sendgrid mail
    jest.mock('@sendgrid/mail')
    let last_message
    sgMail.send = jest.fn().mockImplementation((message) => {
      last_message = message;
    })

    const fake_claims = {uid: '-2', realm: '-3', email: 'test@test.com', user_name: 'test_user'}
    const fake_admin_claims = {uid: '-3', realm: '-4', email: 'test@admin.com', user_name: 'test_admin'}
    const draft_id = await filerC5Processor.createRegistration(c5_registration, fake_claims)
    const draft_registration = await filerC5Processor.getFilerRequestData(draft_id)
    await execute(`insert into pdc_user (uid, realm, user_name) values ('-2', '-3', 'pdc@pdc.wa.gov')`)

    // load the draft registration and submit
    c5_registration = draft_registration
    await filerC5Processor.submitRegistration(draft_id, fake_admin_claims)
    const c5_committee = await fetch(`select * from committee where name = $1`, [draft_registration.committee.name])

    // create a draft report
    let c5_report = await readFile(`src/tests/data/filer/test-c5-draft-report.json`, 'ascii')

    c5_report = JSON.parse(c5_report)
    expect(c5_report).toBeTruthy()

    // load the draft report and submit
    const report_draft_id = await filerC5Processor.createDraftReport(c5_report, c5_committee.committee_id, null, fake_claims)
    const report_to_submit = await filerC5Processor.getDraftReport(report_draft_id, c5_committee.committee_id)
    report_to_submit.user_data = {
      previous_amount: report_to_submit.previous_amount,
      previous_gt_twenty_percent: report_to_submit.previous_gt_twenty_percent,
      candidate_expenses: report_to_submit.candidate_expenses,
      committee_expenses: report_to_submit.committee_expenses,
      other_expenses: report_to_submit.other_expenses,
      resident_contributions: report_to_submit.resident_contributions,
      non_resident_contributions: report_to_submit.non_resident_contributions,
      has_resident_contribution_import: report_to_submit.has_resident_contribution_import,
      has_non_resident_contribution_import: report_to_submit.has_non_resident_contribution_import,
      certification: report_to_submit.certification,
    }
    report_to_submit.registration = {...c5_registration}

    const period_array = (new Date(report_to_submit.period_month)).toISOString("en-US", {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
    }).split('T')[0].split('-')
    const last_day_of_the_month = (new Date(period_array[0], period_array[1], 0)).getDate()
    report_to_submit.period_start = period_array[0] + '-' + period_array[1] + '-' + '01'
    report_to_submit.period_end = period_array[0] + '-' + period_array[1] + '-' + last_day_of_the_month
    report_to_submit.election_year = period_array[0]

    const report_id = await filerC5Processor.submitDraftReport(report_to_submit, c5_committee.committee_id, report_draft_id, fake_claims)

    const submitted_report = await fetch(`select * from report where report_id = ${report_id}`)
    expect(submitted_report.period_start).toBe('2024-07-01')
    expect(submitted_report.period_end).toBe('2024-07-31')
    expect(submitted_report.election_year).toBe('2024')

    const candidate_expense = await fetch(`select * from c5_expense where report_id = ${report_id} and type = 'CA' and name = 'Robert Paulson'`)
    expect(candidate_expense.expense_date).toBe('2023-12-10')
    expect(candidate_expense.amount).toBe('60.43')
    expect(candidate_expense.office).toBe('49')

    const committee_expense = await fetch(`select * from c5_expense where report_id = ${report_id} and type = 'CO'`)
    expect(committee_expense.ballot_number).toBe('42')
    expect(committee_expense.for_or_against).toBe('F')
    expect(committee_expense.expense_date).toBe('2023-12-12')

    const resident_contribution = await fetch(`select * from c5_contribution where report_id = ${report_id} and resident = true`)
    expect(resident_contribution.amount).toBe('100.01')
    expect(resident_contribution.aggregate_total).toBe('100.01')
    expect(resident_contribution.name).toBe('Commander Shepard')

    const non_resident_contribution = await fetch(`select * from c5_contribution where report_id = ${report_id} and resident = false`)
    expect(non_resident_contribution.employer).toBe('Commander Shepard')
    expect(non_resident_contribution.name).toBe("Tali'Zorah")
    expect(non_resident_contribution.aggregate_total).toBe('4200.00')

    // test committee_log
    const c5_committee_log = await fetch(`select * from committee_log where committee_id = $1 and action = 'create c5 report'`, [c5_committee.committee_id])
    expect(c5_committee_log.transaction_type).toBe('committee')
    expect(c5_committee_log.message).toBe(`created c5 report number ${report_id} for committee name ${draft_registration.committee.name}`)
    expect(c5_committee_log.user_name).toBe('test@test.com')

    // test that the draft was deleted
    const deleted_draft = await fetch(`select * from private.draft_document where draft_id = $1`, [report_draft_id])
    expect(deleted_draft).toBeNull()
  })

  test('Test amend C5 report', async () => {
    // create a draft registration
    let c5_registration = await readFile(`src/tests/data/filer/test-c5-draft-registration.json`, 'ascii')
    c5_registration = JSON.parse(c5_registration)

    // mock sendgrid mail
    jest.mock('@sendgrid/mail')
    let last_message
    sgMail.send = jest.fn().mockImplementation((message) => {
        last_message = message;
    })

    const fake_claims = {uid: '-2', realm: '-3', email: 'test@test.com', user_name: 'test_user'}
    const fake_admin_claims = {uid: '-3', realm: '-4', email: 'test@admin.com', user_name: 'test_admin'}
    const draft_id = await filerC5Processor.createRegistration(c5_registration, fake_claims)
    const draft_registration = await filerC5Processor.getFilerRequestData(draft_id)
    await execute(`insert into pdc_user (uid, realm, user_name) values ('-2', '-3', 'pdc@pdc.wa.gov')`)

    // load the draft registration and submit
    c5_registration = draft_registration
    await filerC5Processor.submitRegistration(draft_id, fake_admin_claims)

    let c5_committee = await fetch(`select * from committee where name = $1`, [c5_registration.committee.name])

    // create a draft report
    let c5_report = await readFile(`src/tests/data/filer/test-c5-draft-report.json`, 'ascii')

    c5_report = JSON.parse(c5_report)
    expect(c5_report).toBeTruthy()

    // load the draft report and submit
    const report_draft_id = await filerC5Processor.createDraftReport(c5_report, c5_committee.committee_id, null, fake_claims)
    const report_to_submit = await filerC5Processor.getDraftReport(report_draft_id, c5_committee.committee_id)
    report_to_submit.user_data = {
      previous_amount: report_to_submit.previous_amount,
      previous_gt_twenty_percent: report_to_submit.previous_gt_twenty_percent,
      candidate_expenses: report_to_submit.candidate_expenses,
      committee_expenses: report_to_submit.committee_expenses,
      other_expenses: report_to_submit.other_expenses,
      resident_contributions: report_to_submit.resident_contributions,
      non_resident_contributions: report_to_submit.non_resident_contributions,
      has_resident_contribution_import: report_to_submit.has_resident_contribution_import,
      has_non_resident_contribution_import: report_to_submit.has_non_resident_contribution_import,
      certification: report_to_submit.certification,
    }
    report_to_submit.registration = {...c5_registration}

    let period_array = (new Date(report_to_submit.period_month)).toISOString("en-US", {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit',
    }).split('T')[0].split('-')
    let last_day_of_the_month = (new Date(period_array[0], period_array[1], 0)).getDate()
    report_to_submit.period_start = period_array[0] + '-' + period_array[1] + '-' + '01'
    report_to_submit.period_end = period_array[0] + '-' + period_array[1] + '-' + last_day_of_the_month
    report_to_submit.election_year = period_array[0]

    const report_id = await filerC5Processor.submitDraftReport(report_to_submit, c5_committee.committee_id, report_draft_id, fake_claims)

    // create a draft to amend
    const amend_draft_id = await filerC5Processor.createDraftReport({}, c5_committee.committee_id, report_id, fake_claims)
    const amend_draft = await fetch(
      `select * from private.draft_document where draft_id = $1`, [amend_draft_id]
    )

    const amend_draft_user_data = amend_draft.user_data

    amend_draft_user_data.previous_amount = '319.00'
    amend_draft_user_data.candidate_expenses[0].amount = '52.00'
    amend_draft_user_data.candidate_expenses[1].amount = '71.00'
    amend_draft_user_data.committee_expenses[0].amount = '82.00'
    amend_draft_user_data.resident_contributions[0].amount = '80.00'
    amend_draft_user_data.non_resident_contributions[0].amount = '34.00'
    amend_draft_user_data.user_data = {
      previous_amount: amend_draft_user_data.previous_amount,
      candidate_expenses: amend_draft_user_data.candidate_expenses,
      committee_expenses: amend_draft_user_data.committee_expenses,
      resident_contributions: amend_draft_user_data.resident_contributions,
      non_resident_contributions: amend_draft_user_data.non_resident_contributions,
      has_resident_contribution_import: amend_draft_user_data.has_resident_contribution_import,
      has_non_resident_contribution_import: amend_draft_user_data.has_non_resident_contribution_import,
    }

    amend_draft_user_data.registration = {...c5_registration}

    // submit the amended draft
    const new_report_id = await filerC5Processor.submitDraftReport(amend_draft_user_data, c5_committee.committee_id, amend_draft_id, fake_claims)

    const submitted_report = await fetch(`select * from report where report_id = ${new_report_id}`)
    expect(JSON.parse(submitted_report.user_data).previous_amount).toBe('319.00')
    expect(JSON.parse(submitted_report.user_data).candidate_expenses[0].amount).toBe('52.00')

    const candidate_expenses = await fetch(`select * from c5_expense where report_id = ${new_report_id} and type = 'CA'`)
    expect(candidate_expenses.amount).toBe('52.00')

    const committee_expenses = await query(`select * from c5_expense where report_id = ${new_report_id} and type = 'CO'`)
    expect(committee_expenses[0].amount).toBe('82.00')

    const resident_contribution = await fetch(`select * from c5_contribution where report_id = ${new_report_id} and resident = true`)
    expect(resident_contribution.amount).toBe('80.00')

    const non_resident_contribution = await fetch(`select * from c5_contribution where report_id = ${new_report_id} and resident = false`)
    expect(non_resident_contribution.amount).toBe('34.00')
  })

  test('Test get import rows', async () => {
    const encoded_string = 'H4sIAAAAAAAAAy3KQQrCMBBG4b3gHXKAwU7HFnRZunOhXiGaIQSaTEn+Lnp7u3D14OM9fVaaQqjaGs0JO72t4WtBabaCmj4bkhUXPI4v21ZAU4xV4wEOBr+cTw9T6uU6uIaqCnote16Tp/tt5J6kGzthGUiYL8z//ABtQ5cVeQAAAA=='

    const returned_rows = []

    const error = getImportRows(
      encoded_string,
      { name: 'Joe', address: '1234 street', city: 'Olympia', postcode: '98051', contribution_date: '02/05/2024', amount: '200.00', aggregate_total: '200.00' },
      { resident: true, name: null, address: null, city: null, state: 'WA', postcode: null, contribution_date: null, amount: null, aggregate_total: null },
      returned_rows
    )

    expect(error).toBe(undefined)
    expect(returned_rows).toStrictEqual([
      {
        resident: true,
        name: 'Joe',
        address: '1234 street',
        city: 'Olympia',
        state: 'WA',
        postcode: '98501',
        contribution_date: '2/5/2024',
        amount: '200.00',
        aggregate_total: '200.00'
      }
    ])
  })

})
