import Papa from 'papaparse'
import pako from 'pako'

export function validateFilerReport(report, validate_import = false) {
  const return_validations = []
  const validations = [
    {
      condition: () => ! report.period_month,
      code: `filer_report.period_month.incomplete`
    },
    {
      condition: () => ! report.previous_amount,
      code: 'filer_report.previous_amount.incomplete'
    },
    {
      condition: () => ! report.previous_gt_twenty_percent,
      code: 'filer_report.previous_gt_twenty_percent.incomplete',
    },
    {
      condition: () => report.has_candidate_expenses === null,
      code: 'filer_report.has_candidate_expenses.incomplete',
    },
    {
      condition: () => report.has_committee_expenses === null,
      code: 'filer_report.has_committee_expenses.incomplete',
    },
    {
      condition: () => report.has_resident_contributions === null,
      code: 'filer_report.has_resident_contributions.incomplete',
    },
    {
      condition: () => report.has_non_resident_contributions === null,
      code: 'filer_report.has_non_resident_contributions.incomplete',
    },
  ]
  const candidate_expense_validations = [
    {
      condition: (candidate_expense) => ! candidate_expense.name,
      code: (index) => `filer_report.candidate_expenses[${index}].name.incomplete`,
    },
    {
      condition: (candidate_expense) => ! candidate_expense.target_type,
      code: (index) => `filer_report.candidate_expenses[${index}].target_type.incomplete`,
    },
    {
      condition: (candidate_expense) => ! candidate_expense.target_id,
      code: (index) => `filer_report.candidate_expenses[${index}].target_id.incomplete`,
    },
    {
      condition: (candidate_expense) => ! candidate_expense.expense_date,
      code: (index) => `filer_report.candidate_expenses[${index}].expense_date.incomplete`,
    },
    {
      condition: (candidate_expense) => ! candidate_expense.amount,
      code: (index) => `filer_report.candidate_expenses[${index}].amount.incomplete`,
    },
    {
      condition: (candidate_expense) => (candidate_expense.inkind) ? (! candidate_expense.description) : false,
      code: (index) => `filer_report.candidate_expenses[${index}].description.incomplete`,
    },
    {
      condition: (candidate_expense) => (candidate_expense.inkind) ? (! candidate_expense.vendor_name) : false,
      code: (index) => `filer_report.candidate_expenses[${index}].vendor_name.incomplete`,
    },
    {
      condition: (candidate_expense) => (candidate_expense.inkind) ? (! candidate_expense.vendor_address) : false,
      code: (index) => `filer_report.candidate_expenses[${index}].vendor_address.incomplete`,
    },
    {
      condition: (candidate_expense) => (candidate_expense.inkind) ? (! candidate_expense.vendor_city) : false,
      code: (index) => `filer_report.candidate_expenses[${index}].vendor_city.incomplete`,
    },
    {
      condition: (candidate_expense) => (candidate_expense.inkind) ? (! candidate_expense.vendor_state) : false,
      code: (index) => `filer_report.candidate_expenses[${index}].vendor_state.incomplete`,
    },
    {
      condition: (candidate_expense) => (candidate_expense.inkind) ? (! candidate_expense.vendor_postcode) : false,
      code: (index) => `filer_report.candidate_expenses[${index}].vendor_postcode.incomplete`,
    },
  ]
  const committee_expense_validations = [
    {
      condition: (committee_expense) => ! committee_expense.name,
      code: (index) => `filer_report.committee_expenses[${index}].name.incomplete`,
    },
    {
      condition: (committee_expense) => ! committee_expense.expense_date,
      code: (index) => `filer_report.committee_expenses[${index}].expense_date.incomplete`,
    },
    {
      condition: (committee_expense) => ! committee_expense.amount,
      code: (index) => `filer_report.committee_expenses[${index}].amount.incomplete`,
    },
    {
      condition: (committee_expense) => (committee_expense.related_to_ballot) ? (! committee_expense.for_or_against) : false,
      code: (index) => `filer_report.committee_expenses[${index}].for_or_against.incomplete`,
    },
    {
      condition: (committee_expense) => (committee_expense.related_to_ballot) ? (! committee_expense.ballot_number) : false,
      code: (index) => `filer_report.committee_expenses[${index}].ballot_number.incomplete`,
    },
    {
      condition: (committee_expense) => (committee_expense.inkind) ? (! committee_expense.description) : false,
      code: (index) => `filer_report.committee_expenses[${index}].description.incomplete`,
    },
    {
      condition: (committee_expense) => (committee_expense.inkind) ? (! committee_expense.vendor_name) : false,
      code: (index) => `filer_report.committee_expenses[${index}].vendor_name.incomplete`,
    },
    {
      condition: (committee_expense) => (committee_expense.inkind) ? (! committee_expense.vendor_address) : false,
      code: (index) => `filer_report.committee_expenses[${index}].vendor_address.incomplete`,
    },
    {
      condition: (committee_expense) => (committee_expense.inkind) ? (! committee_expense.vendor_city) : false,
      code: (index) => `filer_report.committee_expenses[${index}].vendor_city.incomplete`,
    },
    {
      condition: (committee_expense) => (committee_expense.inkind) ? (! committee_expense.vendor_state) : false,
      code: (index) => `filer_report.committee_expenses[${index}].vendor_state.incomplete`,
    },
    {
      condition: (committee_expense) => (committee_expense.inkind) ? (! committee_expense.vendor_postcode) : false,
      code: (index) => `filer_report.committee_expenses[${index}].vendor_postcode.incomplete`,
    },
  ]
  const resident_contribution_validations = [
    {
      condition: (resident_contribution) => ! resident_contribution.name,
      code: (index) => `filer_report.resident_contributions[${index}].name.incomplete`,
    },
    {
      condition: (resident_contribution) => ! resident_contribution.address,
      code: (index) => `filer_report.resident_contributions[${index}].address.incomplete`,
    },
    {
      condition: (resident_contribution) => ! resident_contribution.city,
      code: (index) => `filer_report.resident_contributions[${index}].city.incomplete`,
    },
    {
      condition: (resident_contribution) => ! resident_contribution.state,
      code: (index) => `filer_report.resident_contributions[${index}].state.incomplete`,
    },
    {
      condition: (resident_contribution) => resident_contribution.state !== 'WA',
      code: (index) => `filer_report.resident_contributions[${index}].state.invalid`,
    },
    {
      condition: (resident_contribution) => ! resident_contribution.postcode,
      code: (index) => `filer_report.resident_contributions[${index}].postcode.incomplete`,
    },
    {
      condition: (resident_contribution) => ! resident_contribution.aggregate_total,
      code: (index) => `filer_report.resident_contributions[${index}].aggregate_total.incomplete`,
    },
    {
      condition: (resident_contribution) => ! resident_contribution.contribution_date,
      code: (index) => `filer_report.resident_contributions[${index}].contribution_date.incomplete`,
    },
    {
      condition: (resident_contribution) => ! resident_contribution.amount,
      code: (index) => `filer_report.resident_contributions[${index}].amount.incomplete`,
    },
    {
      condition: (resident_contribution) => parseFloat(resident_contribution.amount) > parseFloat(resident_contribution.aggregate_total),
      code: (index) => `filer_report.resident_contributions[${index}].amount.invalid`,
    },
  ]
  const non_resident_contribution_validations = [
    {
      condition: (non_resident_contribution) => ! non_resident_contribution.name,
      code: (index) => `filer_report.non_resident_contributions[${index}].name.incomplete`,
    },
    {
      condition: (non_resident_contribution) => ! non_resident_contribution.address,
      code: (index) => `filer_report.non_resident_contributions[${index}].address.incomplete`,
    },
    {
      condition: (non_resident_contribution) => ! non_resident_contribution.city,
      code: (index) => `filer_report.non_resident_contributions[${index}].city.incomplete`,
    },
    {
      condition: (non_resident_contribution) => ! non_resident_contribution.state,
      code: (index) => `filer_report.non_resident_contributions[${index}].state.incomplete`,
    },
    {
      condition: (non_resident_contribution) => non_resident_contribution.state === 'WA',
      code: (index) => `filer_report.non_resident_contributions[${index}].state.invalid`,
    },
    {
      condition: (non_resident_contribution) => ! non_resident_contribution.postcode,
      code: (index) => `filer_report.non_resident_contributions[${index}].postcode.incomplete`,
    },
    {
      condition: (non_resident_contribution) => ! non_resident_contribution.employer,
      code: (index) => `filer_report.non_resident_contributions[${index}].employer.incomplete`,
    },
    {
      condition: (non_resident_contribution) => ! non_resident_contribution.employer_city,
      code: (index) => `filer_report.non_resident_contributions[${index}].employer_city.incomplete`,
    },
    {
      condition: (non_resident_contribution) => ! non_resident_contribution.employer_state,
      code: (index) => `filer_report.non_resident_contributions[${index}].employer_state.incomplete`,
    },
    {
      condition: (non_resident_contribution) => ! non_resident_contribution.aggregate_total,
      code: (index) => `filer_report.non_resident_contributions[${index}].aggregate_total.incomplete`,
    },
    {
      condition: (non_resident_contribution) => ! non_resident_contribution.contribution_date,
      code: (index) => `filer_report.non_resident_contributions[${index}].contribution_date.incomplete`,
    },
    {
      condition: (non_resident_contribution) => ! non_resident_contribution.amount,
      code: (index) => `filer_report.non_resident_contributions[${index}].amount.incomplete`,
    },
    {
      condition: (non_resident_contribution) => parseFloat(non_resident_contribution.amount) > parseFloat(non_resident_contribution.aggregate_total),
      code: (index) => `filer_report.non_resident_contributions[${index}].amount.invalid`,
    },
  ]
  for (const validation of validations) {
    if (validation.condition()) {
      return_validations.push(validation.code)
    }
  }
  // candidate expenses
  let candidate_expense_total = 0
  for (const [index, candidate_expense] of report.candidate_expenses.entries()) {
    for (const validation of candidate_expense_validations) {
      if (validation.condition(candidate_expense)) {
        return_validations.push(validation.code(index))
      }
    }
    candidate_expense_total += parseFloat(candidate_expense.amount)
  }
  // committee expenses
  let committee_expense_total = 0
  for (const [index, committee_expense] of report.committee_expenses.entries()) {
    for (const validation of committee_expense_validations) {
      if (validation.condition(committee_expense)) {
        return_validations.push(validation.code(index))
      }
    }
    committee_expense_total += parseFloat(committee_expense.amount)
  }

  // get resident import rows out of the csv if it exists
  const resident_contribution_import_rows = []
  if (report.has_resident_contribution_import && validate_import) {
    const error = getImportRows(
      report.resident_contribution_import.compressed_csv,
      { name: 'Joe', address: '1234 street', city: 'Olympia', postcode: '98051', contribution_date: '02/05/2024', amount: '200.00', aggregate_total: '200.00' },
      { resident: true, name: null, address: null, city: null, state: 'WA', postcode: null, contribution_date: null, amount: null, aggregate_total: null },
      resident_contribution_import_rows
    )
    if (error) {
      return_validations.push('import csv to rows failed')
    }
  }

  const resident_contribution_rows = (report.has_resident_contribution_import)
        ? resident_contribution_import_rows
        : report.resident_contributions

  // resident contributions
  for (const [index, resident_contribution] of resident_contribution_rows.entries()) {
    // skip validation for import unless flag is set
    if (report.has_resident_contribution_import && ! validate_import) {
      break
    }
    for (const validation of resident_contribution_validations) {
      if (validation.condition(resident_contribution)) {
        return_validations.push(validation.code(index))
      }
    }
  }

  // get non-resident import rows out of the csv if it exists
  const non_resident_contribution_import_rows = []
  if (report.has_non_resident_contribution_import && validate_import) {
    const error = getImportRows(
      report.non_resident_contribution_import.compressed_csv,
      { name: 'Joe', address: '5678 street', city: 'Austin', state: 'TX', postcode: '73301', employer: 'Jane', employer_city: 'Olympia', employer_state: 'WA', contribution_date: '02/05/2024', amount: '4500.00', aggregate_total: '4500.00' },
      { resident: false, name: null, address: null, city: null, state: null, postcode: null, employer: null, employer_city: null, employer_state: null, contribution_date: null, amount: null, aggregate_total: null },
      non_resident_contribution_import_rows
    )
    if (error) {
      return_validations.push('import csv to rows failed')
    }
  }
  const non_resident_contribution_rows = (report.has_non_resident_contribution_import)
        ? non_resident_contribution_import_rows
        : report.non_resident_contributions

  // non-resident contributions
  for (const [index, non_resident_contribution] of non_resident_contribution_rows.entries()) {
    // skip validation for import unless flag is set
    if (report.has_non_resident_contribution_import && ! validate_import) {
      break
    }
    for (const validation of non_resident_contribution_validations) {
      if (validation.condition(non_resident_contribution)) {
        return_validations.push(validation.code(index))
      }
    }
  }

  // previous amount total aggregate
  const report_total = parseFloat(candidate_expense_total) + parseFloat(committee_expense_total)
  if (report_total > report.previous_amount) {
    return_validations.push('filer_report.previous_amount.invalid')
  }

  return return_validations
}

function fromHexString(hexString) {
  return Uint8Array.from(hexString.match(/.{1,2}/g).map((byte) => parseInt(byte, 16)))
}

export function getImportRows(import_string, sample_row, row_default, return_rows) {

  // string to buffer
  const buffer = Buffer.from(import_string, 'base64')
  // pako decompress
  const decompressed = pako.ungzip(buffer)
  // base64 decode
  const as_text = new TextDecoder().decode(decompressed.buffer)

  const temp_rows = []
  const columns = Object.keys(sample_row)
  Papa.parse(as_text, {
    header: true,
    skipEmptyLines:true,
    chunkSize: 10000,
    transformHeader: function(input_string) {
      return input_string
        .toLowerCase()
        .trim()
        .replace(/[_\s]+/g, '_')
    },
    chunk: function(results, parser) {
      for (let z = 0; z < results.data.length; ++z) {
        // store the row in temp_rows
        const temp_row = {}
        for (let column of columns) {
          temp_row[column] = results.data[z][column]
        }
        temp_rows.push({...row_default, ...temp_row})
      }
    },
    complete: function() {
      for (const row of temp_rows) {
        return_rows.push(row)
      }
      return false
    },
    error: function() {
      return 'import rows failed'
    }
  })
}
