import { query, fetch, execute } from '@wapdc/common/wapdc-db'

/**
 * This processor is used for tests and non-report related items
 **/

class C5Processor {

    // data entry -----------------------------------------------------------------------------------------------------

    /**
     * Gets list of all out-of-state committees.
     * */
    async getCommitteesList() {
        return await query(
            "SELECT c.committee_id, c.name, c.filer_id, c.election_code from committee c where committee_type = 'OS'  order by election_code desc"
        )
    }

    async getCommitteeLogs(committee_id) {
         return await query(
            "select * from committee_log where committee_id = $1 order by log_id desc", [committee_id]
        )
    }

    async getOfficeTypes() {
        return await query(`
        select o.offtitle, o.offcode
        from foffice o
        where offcode in
          (select jo.offcode
            from jurisdiction_office jo
              join jurisdiction j on j.jurisdiction_id = jo.jurisdiction_id
            where j.category not in (14, 15, 16))
          and o.on_off_code = true
        order by o.offtitle`
        )
    }

    async getPartyTypes() {
        return await query(`
        select name, party_id
        from party
        where valid_to is null
        order by name`
        )
    }

    /**
     * Grab the out-of-state committee information from the database and toss them all into one object,
     * to reduce more lambda calls
     */
    async getCommitteeInfo(committee_id) {
        const committee_info = await fetch(`
                                select c.*, c2.address, c2.city, c2.state, c2.postcode, c2.email from committee c 
                                left join committee_contact c2 on c.committee_id = c2.committee_id and c2.role = 'committee'
                                where c.committee_id = $1`, [committee_id])
        committee_info.committee_contacts = await query(`
            select cc.* from committee_contact cc where cc.committee_id = $1`, [committee_id])
        committee_info.reports = await query(`
                                select r.*,
                                       case WHEN r.ax_legacy_docid is not null
                                                then 'https://web.pdc.wa.gov/rptimg/default.aspx?docid=' || r.ax_legacy_docid ELSE
                                           'https://www.pdc.wa.gov/political-disclosure-reporting-data/browse-search-data/reports?filer_name='
                                           || r.filer_name || '&origin=C5%7CC5+AMENDED'
                                           END report_url from committee c left join fund f on f.committee_id = c.committee_id
                                join report r on r.fund_id = f.fund_id where c.committee_id = $1
                                order by r.submitted_at desc`, [committee_id])
        return committee_info
    }

    async deleteReport(report_id, username) {
        return await execute(`select from c5_delete_report($1, $2)`, [report_id, username]);
    }

    async getLatestCommitteeReportInfo(committee_id) {
        const report = {}
        const admin_data = {}

        report.committee = await fetch(`
                                select c.*, c2.address, c2.city, c2.state, c2.postcode, c2.email from committee c
                                left join committee_contact c2 on c.committee_id = c2.committee_id and c2.role = 'committee'
                                where c.committee_id = $1`, [committee_id])

        // if this committee was created from a Filer, get the user_data
        const filer_user_data = await fetch(`
          select c.*, r.registration_id, r.user_data::json from committee c
          join registration r on r.registration_id = c.registration_id
          where c.committee_id = $1
        `, [committee_id])

        if (filer_user_data?.user_data) {
          admin_data.committee_purpose = filer_user_data.committee_purpose
          admin_data.officers = filer_user_data.officers
          report.admin_data = filer_user_data.user_data
          return report
        }

        // if this committee was created from a Data Admin, get the admin_data
        const { committee_purpose = null } = await fetch(`
                                select coalesce(admin_data::json->>'committee_purpose', ' ')
                                from registration where committee_id = $1
                                                    and admin_data is not null
                                                    and submitted_at = (select max(submitted_at) from registration where committee_id = $1)`, [committee_id])
        admin_data.committee_purpose = committee_purpose
        admin_data.officers = await query(`select cc.* from committee_contact cc where role='officer' and cc.committee_id = $1`, [committee_id])

        report.admin_data = admin_data
        return report
    }

    validateReport(report) {
        const validations = [
            {
                condition: () => isNaN(report.election_year),
                code: 'report.election_year.invalid',
            },
            {
                condition: () => !report.committee.name,
                code: 'report.committee.name.incomplete',
            },
            {
                condition: () => !report.treasurer_name,
                code: 'report.treasurer_name.incomplete'
            }
        ]
        for (const validation of validations) {
            if (validation.condition()) {
                return validation.code
            }
        }
        return ''
    }

    /**
     * Submit a report with a new or existing committee
     * @param username is the logged-in user
     * @param committee_id optional
     * @param report_id optional
     * @param report
     */
    async submitReport(report, username, committee_id, report_id = null) {
      const errors = this.validateReport(report)
      if (errors !== '') {
        throw new Error(errors)
      }

      // see if the most recent submitted_at report is from the Filer side
      const committee_reports = await query(`
                                  select r.*,
                                        case WHEN r.ax_legacy_docid is not null
                                                  then 'https://web.pdc.wa.gov/rptimg/default.aspx?docid=' || r.ax_legacy_docid ELSE
                                            'https://www.pdc.wa.gov/political-disclosure-reporting-data/browse-search-data/reports?filer_name='
                                            || r.filer_name || '&origin=C5%7CC5+AMENDED'
                                            END report_url from committee c left join fund f on f.committee_id = c.committee_id
                                  join report r on r.fund_id = f.fund_id where c.committee_id = $1
                                  order by r.submitted_at desc limit 1`, [committee_id])

      let latest_submitted_at_is_admin = Boolean(committee_reports[0]?.admin_data)
      if (committee_reports.length === 0) {
        latest_submitted_at_is_admin = true
      }

      const {c5_submit_report} = await fetch(
        `select * from c5_submit_report($1, $2, $3, $4, $5)`,
        [report, username, committee_id, report_id, latest_submitted_at_is_admin]
      )

      return c5_submit_report
    }

    async getFilerRequestData(request_id) {
        const { payload } = await fetch(`select * from wapdc.public.filer_request where id = $1`,[request_id])
        return payload
    }

    async getReport(report_id) {
      //get base report information
      let report = await fetch(`
                select r2.admin_data::json, r.metadata, r.*
                from report r
                        join (select (admin_data::json ->> 'report')::int as report_id, admin_data
                              from registration
                              where admin_data::json->>'report' = ($1)::text and admin_data is not null
                                and source ilike 'c5') r2 on r2.report_id = r.report_id
                where r.report_id = cast($1 as int)`, [report_id])

      if (! report) {
        report = await fetch(`
                select r2.user_data::json, r.metadata, r.*
                from report r
                        join (select (user_data::json ->> 'report')::int as report_id, user_data
                              from registration
                              where user_data::json->>'report' = ($1)::text and user_data is not null
                                and source ilike 'c5') r2 on r2.report_id = r.report_id
                where r.report_id = cast($1 as int)`, [report_id])
      }

      report.committee = await fetch(`
              select r.address1 as address,
                    r.city,
                    r.state,
                    r.postcode,
                    r.filer_name as name,
                    f.committee_id,
                    cc.email
              from report r
              join fund f on f.fund_id = r.fund_id
              join wapdc.public.committee_contact cc on cc.committee_id = f.committee_id
              where r.report_id = $1 and cc.role = 'committee'`, [report_id])
      report.expenses = {}
      report.expenses.candidate = await query(`select *
                                                  from c5_expense
                                                  where report_id = $1
                                                    and type = 'CA'`, [report_id])
      report.expenses.ballot = await query(`select *
                                                from c5_expense
                                                where report_id = $1
                                                  and type = 'PROP'`, [report_id])
      report.expenses.other = await query(`select *
                                              from c5_expense
                                              where report_id = $1
                                                and type = 'OTH'`, [report_id])

      report.metadata = JSON.parse(report.metadata)
      report.metadata ??= {total_contributions_and_expenditures: null, aggregate_contributions: {}}
      return {report}
    }

    async updateFilerRequest(request_id, agent, action_date) {
      return await execute(`
        update filer_request
        set agent_user_name = $1, action_date = $2
        where id = $3
       `, [agent, action_date, request_id]
      )
    }

    // verifications ---------------------------------------------------------------------------------------------------

    async getUnverifiedCommittees() {
      return await query(`
         select * from wapdc.public.filer_request where target_type = 'c5_committee' order by submitted_at
      `)
    }

    async revertRequest(request_id) {
      return await execute(`delete from filer_request where id = $1`, [request_id])
    }

    async getMatchCommittees(p_name) {
      return await query(
      `
          select
          c.committee_id,
          c.name as committee_name,
          e.email as entity_email,
          e.address as entity_address,
          e.city as entity_city,
          e.state as entity_state,
          e.postcode as entity_postcode,
          e.state as entity_state,
          coalesce(r.admin_data, r.user_data) as admin_data,
          fuzzy_match($1, c.name) as similarity
          from committee c
          join registration r on c.registration_id = r.registration_id
          join entity e on c.person_id = e.entity_id
          join committee_contact cc on c.committee_id = cc.committee_id
          and cc.role='committee'
          and c.committee_type='OS'
          order by similarity desc
          limit 10
      `, [p_name]
      )
    }
}

const c5Processor = new C5Processor()
export { c5Processor }
