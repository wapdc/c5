import { deleteDraft, getDraft, createDraft, saveDraft } from '@wapdc/common/drafts'
import { execute, fetch, query} from '@wapdc/common/wapdc-db'
import { sendMessage } from '@wapdc/common/mailer'
import { validateFilerReport } from './ValidateFilerReport.js'

class FilerC5Processor {

  // registration ---------------------------------------------------------------------------------------------

  validateRegistration(registration) {
    const validations = [
      {
        condition: () => ! registration.committee?.name,
        code: 'registration.committee.name.incomplete',
      },
      {
        condition: () => ! registration.committee?.email,
        code: 'registration.committee.email.incomplete',
      },
      {
        condition: () => ! registration.committee?.address,
        code: 'registration.committee.address.incomplete',
      },
      {
        condition: () => ! registration.committee?.city,
        code: 'registration.committee.city.incomplete',
      },
      {
        condition: () => ! registration.committee?.state,
        code: 'registration.committee.state.incomplete',
      },
      {
        condition: () => ! registration.committee?.postcode,
        code: 'registration.committee.postcode.incomplete',
      },
      {
        condition: () => ! registration.committee_purpose,
        code: 'registration.committee_purpose.incomplete',
      },
      {
        condition: () => ! registration.officers || registration.officers?.length < 1,
        code: 'registration.officers.incomplete',
      },
      {
        condition: () => ! registration.reporting_states || registration.reporting_states?.length < 1,
        code: 'registration.reporting_states.incomplete',
      },
    ]
    for (const validation of validations) {
      if (validation.condition()) {
        return validation.code
      }
    }
    return ''
  }

  async submitAmendedCommittee(registration, committee_id, claims) {
    const errors = this.validateRegistration(registration)
    if (errors !== '') {
      throw new Error(errors)
    }
    const { amendedFilerCommittee } = await fetch(
      `select * from c5_amend_committee_registration($1, $2, $3)`,
      [registration, committee_id, claims]
    )
    return amendedFilerCommittee
  }

  async submitRequestRegistration(registration, claims) {
    const errors = this.validateRegistration(registration)
    if (errors !== '') {
      throw new Error(errors)
    }
    const id = await fetch(
      `select * from c5_save_filer_request($1, $2)`,
      [registration, claims]
    )
    return id
  }

  async sendVerificationEmail(user_name) {
    await sendMessage(user_name, 'd-9848aaef94464cc4b16e89bf7d6f2aca', {})
  }

  async submitRegistration(request_id, admin_claims) {
    const { payload: registration, uid, realm } = await fetch(`select payload, uid, realm from filer_request where id = $1`, [request_id])
    const { user_name } = await fetch('select user_name from pdc_user where uid = $1', [uid])
    const claims = { uid, realm }

    const errors = this.validateRegistration(registration)
    if (errors !== '') {
      throw new Error(errors)
    }

    const { c5_submit_filer_registration } = await fetch(
      `select * from c5_submit_filer_registration($1, $2, $3)`,
      [registration, claims, admin_claims]
    )

    await this.sendVerificationEmail(user_name)

    // delete the filer_request
    await execute(`delete from filer_request where id = $1`, [request_id])

    return c5_submit_filer_registration
  }

  async mergeRegistration(committee_id, request_id, admin_claims) {
    const { payload: registration, uid, realm } = await fetch(`select payload, uid, realm from filer_request where id = $1`, [request_id])
    const { user_name } = await fetch('select user_name from pdc_user where uid = $1', [uid])
    const claims = { uid, realm }

    const errors = this.validateRegistration(registration)
    if (errors !== '') {
      throw new Error(errors)
    }

    const { c5_merge_filer_registration } = await fetch(
      `select * from c5_merge_filer_registration($1, $2, $3, $4)`,
      [registration, committee_id, claims, admin_claims]
    )

    await this.sendVerificationEmail(user_name)

    // delete the filer_request
    await execute(`delete from filer_request where id = $1`, [request_id])

    return c5_merge_filer_registration
  }

  async createRegistration(registration_json, claims) {
    const { c5_save_filer_request } = await fetch(
      `select * from c5_save_filer_request($1, $2)`,
      [registration_json, claims]
    )
    return c5_save_filer_request
  }

  async getCommitteeData(committee_id) {
     return await fetch(`select c.*,r.registration_id, r.user_data::json from committee c
      join registration r on r.registration_id = c.registration_id
      where c.committee_id = $1`, [committee_id])
  }

  async getMyCommittees(claims) {
    const filer_requests = await query(`
            select
              w.id,
              w.payload->'committee'->>'name' as name,
              'filer_request' as type,
              'Request processing' as status
            from wapdc.public.filer_request w
            where w.uid = $1 and w.realm = $2
          `,[claims.uid, claims.realm]
        )

    const verified_committees = await query(`
            select
              c.committee_id as id,
              c.name as name,
              'verified_committee' as type,
              'Verified' as status
            from pdc_user_authorization pua
            join committee c on c.committee_id = pua.target_id
            where pua.target_type = 'committee' and pua.uid = $1 and pua.realm = $2
          `,[claims.uid, claims.realm]
        )

    return [...filer_requests, ...verified_committees]
  }

  async deleteUnverifiedRegistration(draft_id){
    return await execute(`select * from c5_delete_filer_request_registration($1)`, [draft_id])
  }

  // report ---------------------------------------------------------------------------------------------

  async saveEditedReport(payload, committee_id, report_id) {
    payload.committee_id = committee_id
    return await query(`update private.draft_document set user_data = $1, updated_at = now() where draft_id = $2`, [JSON.parse(JSON.stringify(payload)), report_id])
  }

  async getMyReports(committee_id) {

    committee_id = committee_id.toString()
    const draft_reports = await query(`
        select
          dd.draft_id as id,
          'draft' as type,
          'Draft' as status,
          dd.user_data->>'period_month' as period_month
        from private.draft_document dd
        where dd.user_data->>'committee_id' = $1
      `, [committee_id]
    )

    const submitted_reports = await query(`
                select distinct on (r.period_start)
                    r.filer_name as filer_name,
                    r.report_id as id,
                    'submitted' as type,
                    'Submitted' as status,
                    r.period_start as period_month,
                    r.submitted_at,
                    r.ax_legacy_docid as docid,
                    CASE WHEN r.user_data is not null then 'e'
                         else 'p'
                        END as how_filed
                from report r
                         join fund f on r.fund_id = f.fund_id
                where f.committee_id = $1
                order by r.period_start, r.submitted_at desc
        `, [committee_id]
    )

    submitted_reports.forEach(report => {
      report.period_month = new Date(report.period_month)
      return report
    })

    return [...draft_reports, ...submitted_reports]
  }

  async submitDraftReport(report, committee_id, draft_id, claims) {
    const errors = validateFilerReport(report, true)
    if (errors.length > 0) {
      throw new Error(errors)
    }

    // amend
    if (report.report_id) {
      const { c5_amend_filer_report } = await fetch(
        `select * from c5_amend_filer_report($1, $2, $3, $4)`,
        [report, report.report_id, committee_id, claims]
      )

      await this.deleteDraftReport(draft_id)
      return c5_amend_filer_report
    }

    // check to see if there is an existing report with the same period_start
    const reports_with_same_period_month = await query(`
      select r.report_id, r.period_start, f.committee_id
      from report r
        join fund f on r.fund_id = f.fund_id
      where f.committee_id = $1 and r.period_start = $2`,
      [committee_id, report.period_month]
    )
    if (reports_with_same_period_month.length > 0) {
      throw new Error('Existing report found with the same reporting period')
    }

    // submit
    const { c5_submit_filer_report } = await fetch(
      `select * from c5_submit_filer_report($1, $2, $3)`,
      [report, committee_id, claims]
    )

    await this.deleteDraftReport(draft_id)
    return c5_submit_filer_report
  }

  async createDraftReport(report_json, committee_id, report_id, claims) {
    // if an amendment
    if (report_id) {
      const report = await fetch(`
        select report_type, version, period_start, period_end, election_year, user_data from report
        where report_id = $1`,
        [report_id]
      )

      const candidate_expenses = await query(`
        select c5_expense_id, report_id, type, expense_date, amount, name, target_type, target_id, inkind, description, vendor_name, vendor_address, vendor_city, vendor_state, vendor_postcode from c5_expense
        where report_id = $1 and type = 'CA'`,
        [report_id]
      )

      const committee_expenses = await query(`
        select c5_expense_id, report_id, type, expense_date, amount, name, target_type, target_id, address, city, state, postcode, ballot_number, for_or_against, inkind, description, vendor_name, vendor_address, vendor_city, vendor_state, vendor_postcode from c5_expense
        where report_id = $1 and type = 'CO'`,
        [report_id]
      )

      const resident_contributions = await query(`
        select c5_contribution_id, report_id, contribution_date, amount, aggregate_total, name, address, city, state, postcode, resident from c5_contribution
        where report_id = $1 and resident = true`,
        [report_id]
      )

      const non_resident_contributions = await query(`
        select c5_contribution_id, report_id, contribution_date, amount, aggregate_total, name, address, city, state, postcode, resident, email, employer, employer_city, employer_state, employer_zip from c5_contribution
        where report_id = $1 and resident = false`,
        [report_id]
      )

      const period_array = (new Date(report.period_start)).toISOString("en-US", {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
      }).split('T')[0].split('-')

      const period_month = `${period_array[1]}-${period_array[2]}-${period_array[0]}`
      report.period_month = period_month
      report.report_id = report_id
      report.candidate_expenses = candidate_expenses
      report.committee_expenses = committee_expenses.map(
        committee_expense => ({...committee_expense, related_to_ballot: Boolean(committee_expense.ballot_number) })
      )
      report.resident_contributions = resident_contributions
      report.non_resident_contributions = non_resident_contributions
      report.has_candidate_expenses = candidate_expenses.length > 0
      report.has_committee_expenses = committee_expenses.length > 0
      report.has_resident_contributions = resident_contributions.length > 0
      report.has_non_resident_contributions = non_resident_contributions.length > 0

      let report_user_data = JSON.parse(report.user_data)
      report_user_data = {
        ...report_user_data,
        candidate_expenses,
        committee_expenses,
        resident_contributions,
        non_resident_contributions,
        period_month,
      }

      report.previous_amount = report_user_data.previous_amount
      report.previous_gt_twenty_percent = report_user_data.previous_gt_twenty_percent
      report.resident_contribution_import = report_user_data.resident_contribution_import
      report.has_resident_contribution_import = report_user_data.has_resident_contribution_import
      report.non_resident_contribution_import = report_user_data.non_resident_contribution_import
      report.has_non_resident_contribution_import = report_user_data.has_non_resident_contribution_import
      report.user_data = report_user_data
      report_json = report
    }

    report_json.committee_id = committee_id
    const { uid, realm } = claims

    return await createDraft({
      uid,
      realm,
      target_type: 'committee',
      target_id: committee_id,
      user_data: report_json,
      report_type: 'C5 report',
      report_key: `${report_json.period_month}`,
    })
  }

  async deleteDraftReport(draft_id) {
    return await deleteDraft(draft_id)
  }

  async getFilerRequestData(id) {
    const { payload } = await this.getFilerRegistrationRequestData(id)
    return payload
  }

  async getDraftReport(draft_id, committee_id) {
    const draft_report = await getDraft(draft_id)
    if (draft_report.target_id.toString() !== committee_id.toString()) {
      const error = new Error('Unauthorized')
      error.code = '403'
      throw error
    }
    draft_report.user_data.save_count = draft_report.save_count
    const validations = await validateFilerReport(draft_report.user_data)
    draft_report.user_data.validations = validations
    return draft_report.user_data
  }

  async getCommitteeHistory(committee_id) {
    let committee_history  = []
      committee_history = await query(`
          select *
          from committee c
            join registration r on r.committee_id = c.committee_id
          where c.committee_id = $1
          order by submitted_at desc`, [committee_id]
      )
    return committee_history
  }

  async getHistoricalCommitteeData(committee_id, registration_id) {
    let committee_history  = []
    committee_history = await query(`
          select *
          from registration committee
                     where committee.committee_id = $1 and committee.registration_id = $2
          order by submitted_at desc`, [committee_id, registration_id]
    )
    return committee_history
  }

  async getReportView(report_id) {
    const report = await this.getReport(report_id)

    return {
      submitted_at: report.submitted_at,
      period_start: report.period_start,
      period_end: report.period_end,
      election_year: report.election_year,
      user_data: JSON.parse(report.user_data),
    }
  }

  async deleteDraftRegistration(draft_id){
    return await execute(`select * from c5_delete_filer_request_registration($1)`, [draft_id])
  }

  async getFilerRegistrationRequestData(id) {
    const registration = await fetch(`
    SELECT payload FROM filer_request 
    WHERE id = $1`,
        [parseInt(id, 10)]);
      return registration ? registration : null;
  }

  async getReportHistory(report_id, period_month) {
    const period_array = period_month.split('-')
    const period_start = `${period_array[2]}-${period_array[0]}-${period_array[1]}`

    const { fund_id } = await fetch('select fund_id from report where report_id = $1', [report_id])

    return await query(`
        select report_id, superseded_id, submitted_at, period_start
        from report
        where fund_id = $1 and period_start = $2
        order by submitted_at desc
      `, [fund_id, period_start]
    )
  }

  async saveDraftReport(user_data, draft_id) {
    let save_count = user_data.save_count
    if (! user_data.save_count) {
      save_count = 0
    }
    user_data.validations = []
    save_count = await saveDraft({ user_data, draft_id, save_count })
    const validations = await validateFilerReport(user_data)
    return {save_count, validations}
  }

  async getReport(report_id) {
    const report = await fetch(`
      SELECT * FROM report
      WHERE report_id = $1`,
      [parseInt(report_id, 10)]
    );
    return report ? report : null;
  }

  async getAllCommittees() {
    const year = period_start.split('-')[0]
    return query(`
      select committee_id, name
      from committee
      where ((continuing and cast($1 as int) between start_year and coalesce(end_year, cast($1 as int)))
        or (election_code >= cast($1 as text)))
        and committee_type = 'CO'
        and filer_id is not null
      order by name`,
      [year]
    )
  }

  async getPeriodCommittees(period_start) {
    const year = period_start.split('-')[0]
    return query(`
      select committee_id, name
      from committee
      where ((continuing and cast($1 as int) between start_year and coalesce(end_year, cast($1 as int)))
        or (election_code >= cast($1 as text)))
        and committee_type = 'CO'
        and filer_id is not null
      order by name`,
      [year]
    )
  }

  async getPeriodCandidates(period_start) {
    const year = period_start.split('-')[0]
    return query(`
      select ca.candidacy_id,
        CASE
          WHEN ca.ballot_name <> e.name
            THEN concat(e.name, ' (', ca.ballot_name, ')')
          ELSE e.name
        END 
        || ' - ' || offtitle || 
        CASE WHEN j.jurisdiction_id<> 1000 
            THEN ' ' || j.name else '' 
        END
        || ' ' || substr(ca.election_code,1,4)
            || 
        CASE 
            WHEN exit_reason is null THEN ''
            WHEN exit_reason = '' THEN ''
            ELSE ' (' || exit_reason || ')' 
        END
            as name
      from entity e join candidacy ca on ca.person_id = e.entity_id
        join jurisdiction j on ca.jurisdiction_id = j.jurisdiction_id
        join foffice o on ca.office_code= o.offcode
      where election_code >= cast($1 as text)
      order by name`,
      [year]
    )
  }
}


const filerC5Processor = new FilerC5Processor()
export { filerC5Processor }
