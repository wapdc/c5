import { claims, getRestApi, waitForCompletion } from '@wapdc/common/rest-api'
import { fetch } from '@wapdc/common/wapdc-db'
import { getTemplates, setTestEmailAddress } from '@wapdc/common/mailer'
import { c5Processor } from '../C5Processor.js'
import { filerC5Processor } from '../FilerC5Processor.js'

// establish rest-api from @wapdc/common
const api = getRestApi()
let username
api.use('admin/*', async (req, res, next) => {
  if (!claims.admin) {
    return res.error({ type: "Access denied", message: "Access denied (admin)", statusCode: 403 })
  }
  const {user_name} = await fetch(`select user_name from pdc_user where uid = $1 and realm = $2`,[claims.uid, claims.realm])
  username = user_name

  setTestEmailAddress(claims.email ?? null)

  next()
})

// data entry -------------------------------------------------------------------------------------------------

api.get('admin/committees',  async (req, res) => {
  res.json(await c5Processor.getCommitteesList())
})

api.get('admin/committees/:committee_id', async (req, res) => {
  const {committee_id} = req.params
  res.json(await c5Processor.getCommitteeInfo(committee_id))
})

api.get('admin/committees/verify/:request_id', async (req, res) => {
  res.json(await c5Processor.getFilerRequestData(req.params.request_id))
})

api.get('admin/committees/:committee_id/report', async (req, res) => {
  res.json(await c5Processor.getLatestCommitteeReportInfo(req.params.committee_id))
})

api.get('admin/committees/:committee_id/reports/:report_id', async (req, res) => {
  res.json(await c5Processor.getReport(req.params.report_id))
})

api.post('admin/committees/:committee_id/report', async (req, res) => {
  const report_id = await c5Processor.submitReport(req.body, username, req.params.committee_id, null)
  res.json(report_id)
})

api.post('admin/committees/:committee_id/reports/:report_id', async (req, res) => {
  const report_id = await c5Processor.submitReport(req.body, username, req.params.committee_id, req.params.report_id)
  res.json(report_id)
})

api.get('admin/committees/:committee_id/logs', async (req, res) => {
  res.json(await c5Processor.getCommitteeLogs(req.params.committee_id))
})

api.post('admin/report', async (req, res) => {
  const report_id = await c5Processor.submitReport(req.body, username)
  res.json(report_id)
})

api.delete('admin/committees/:committee_id/reports/:report_id', async (req, res) => {
  const { report_id } = req.params
  res.json(await c5Processor.deleteReport(report_id, username))
})


// verifications ------------------------------------------------------------------------------------------

api.get('admin/committees/unverified-committees', async (req, res) => {
  res.json(await c5Processor.getUnverifiedCommittees())
})

api.post('admin/requests/:request_id/revert', async (req, res) => {
  await c5Processor.revertRequest(req.params.request_id)
  res.json({ success: true })
})

api.put('admin/requests/:request_id/update', async (req, res) => {
  const { agent_user_name, action_date } = req.body
  res.json(await c5Processor.updateFilerRequest(req.params.request_id, agent_user_name, action_date))
})

api.post('admin/requests/:request_id/verify-non-match', async (req, res) => {
  const registration_id = await filerC5Processor.submitRegistration(req.params.request_id, claims)
  res.json({ registration_id, success: true })
})

api.post('admin/requests/:request_id/verify-match', async (req, res) => {
  const registration_id = await filerC5Processor.mergeRegistration(req.body.committee_id, req.params.request_id, claims)
  res.json({ registration_id, success: true })
})

api.delete('/admin/requests/:request_id/delete', async (req, res) => {
  const { draft_id } = req.params
  res.json(await filerC5Processor.deleteDraftRegistration(draft_id))
})
api.post('admin/match-committees ', async (req, res) => {
  res.json(await c5Processor.getMatchCommittees(req.body.name))
})

api.get('admin/email-templates', async(req, res) => {
 res.json(await getTemplates())
})

/**
 * AWS Lambda handler. Runs the API with the provided event and context.
 *
 * @param {object} event - The AWS Lambda event object.
 * @param {object} context - The AWS Lambda context object.
 * @returns {Promise} - The promise to return a response.
 */
export const lambdaHandler = async (event, context) => {
  const result = await api.run(event, context)
  await waitForCompletion()
  return result
}
