import {claims, getRestApi, waitForCompletion} from '@wapdc/common/rest-api'
import { fetch } from "@wapdc/common/wapdc-db"
import { c5Processor } from '../C5Processor.js'
import { filerC5Processor } from '../FilerC5Processor.js'

/**
 * GET /user endpoint. Returns the claims attached to the request.
 *
 * @param {object} req - The HTTP request object.
 * @param {object} res - The HTTP response object.
 */

// establish rest-api from @wapdc/common
const api = getRestApi({compressed: true})

// permissions for the registration drafts
api.use('draft-registrations/*', async (req, res, next) => {
  const response = await fetch(
    `select id from filer_request where uid = $1 and realm = $2 and id = $3`,
    [claims.uid, claims.realm, req.params.draft_id]
  )
  if (response?.id) {
    next()
    return
  }
  return res.error({ type: "Access denied", message: "Access denied (not authorized)", statusCode: 403 })
})

// permissions for the committee
api.use('committees/*', async (req, res, next) => {
  const response = await fetch(
      `select target_id from pdc_user_authorization where uid = $1 and realm = $2 and target_id = $3 and target_type = 'committee' `,
      [claims.uid, claims.realm, req.params.committee_id]
  )
  // admins have GET privileges
  if (((req.method === 'GET' && claims.admin) || response?.target_id)) {
    next()
    return
  }
  return res.error({ type: "Access denied", message: "Access denied (not authorized)", statusCode: 403 })
})

api.get('/filer/user', async (req, res) => {
  await res.json({claims, success: true})
})

// used in admin data entry ----------------------------------------------------------------------------

// get the office type
api.get('/offices', async (req, res) => {
  const request_office_types = await c5Processor.getOfficeTypes()
  res.json({ request_office_types, success: true })
})

// get party type
api.get('/parties', async (req, res) => {
  const request_party_types = await c5Processor.getPartyTypes()
  res.json({ request_party_types, success: true })
})

// filer registration ---------------------------------------------------------------------------

api.get('/committees', async (req, res) => {
  res.json(await filerC5Processor.getMyCommittees(claims))
})

api.get('/committees/:committee_id', async (req, res) => {
  const committee = await filerC5Processor.getCommitteeData(req.params.committee_id)
  res.json({committee, success: true})
})

api.get('/committees/:committee_id/:registration_id', async (req, res) => {
  const committee = await filerC5Processor.getHistoricalCommitteeData(req.params.committee_id, req.params.registration_id)
  res.json({committee, success: true})
})

api.post('/committees/:committee_id/amend', async (req, res) => {
  const response = await filerC5Processor.submitAmendedCommittee(req.body, req.params.committee_id, claims)
  res.json({ response, success: true })
})

api.post('/draft-registrations', async (req, res) => {
  const registration_id = await filerC5Processor.submitRegistration(req.body, claims)
  res.json({ registration_id, success: true })
})

// No permissions required as this is not yet tied to a committee
api.post(`/create-request-registration`, async (req, res) => {
  const registration_id = await filerC5Processor.createRegistration(req.body, claims)
  res.json({ registration_id, success: true })
})

// filer reports ---------------------------------------------------------------------------------------

api.delete('/committees/:committee_id/draft-reports/:draft_id', async (req, res) => {
  const { draft_id } = req.params
  res.json(await filerC5Processor.deleteDraftReport(draft_id))
})

api.get('/committees/:committee_id/draft-reports', async (req, res) => {
  res.json(await filerC5Processor.getMyReports(req.params.committee_id))
})

api.get('/committees/:committee_id/draft-report/:report_id', async (req, res) => {
  res.json(await filerC5Processor.getDraftReport(req.params.report_id, req.params.committee_id))
})

api.get('/committees/:committee_id/view-report/:report_id', async (req, res) => {
  res.json(await filerC5Processor.getReportView(req.params.report_id))
})

api.get('/committees/:committee_id/history-report/:report_id/period-month/:period_month', async (req, res) => {
  res.json(await filerC5Processor.getReportHistory(req.params.report_id, req.params.period_month))
})

api.post('/committees/:committee_id/new-draft-report', async (req, res) => {
  res.json(await filerC5Processor.createDraftReport(req.body, req.params.committee_id, null, claims))
})

api.post('/committees/:committee_id/new-amend-draft-report/:report_id', async (req, res) => {
  res.json(await filerC5Processor.createDraftReport({}, req.params.committee_id, req.params.report_id, claims))
})

api.get('/committees/:committee_id/history', async (req, res) => {
  res.json(await filerC5Processor.getCommitteeHistory(req.params.committee_id))
})

api.get('/committees/:committee_id/period-committees/:period_start', async (req, res) => {
  res.json(await filerC5Processor.getPeriodCommittees(req.params.period_start))
})

api.get('/committees/:committee_id/period-candidates/:period_start', async (req, res) => {
  res.json(await filerC5Processor.getPeriodCandidates(req.params.period_start))
})

api.post('/committees/:committee_id/submit-draft-report/:report_id', async(req, res) => {
  res.json(await filerC5Processor.submitDraftReport(req.body, req.params.committee_id, req.params.report_id, claims))
})

api.put('/committees/:committee_id/save-draft-report/:report_id', async (req, res) => {
  try {
    res.json(await filerC5Processor.saveDraftReport(req.body.report, req.params.report_id))
  }
  catch (error) {
    if (error.toString().includes('Save count mismatch')) {
      res.error({type: 'Conflict', message: "Save count mismatch", statusCode: 409})
    }
    else {
      throw(error)
    }
  }
})

/**
 * AWS Lambda handler. Runs the API with the provided event and context.
 *
 * @param {object} event - The AWS Lambda event object.
 * @param {object} context - The AWS Lambda context object.
 * @returns {Promise} - The promise to return a response.
 */
export const lambdaHandler = async (event, context) => {
  const result = await api.run(event, context)
  await waitForCompletion()
  return result
}
