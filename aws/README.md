# c5 Aws Lambdas

This aws project provides the REST web services that allow out of state committees to file reports. It uses the [Lambda API](https://www.npmjs.com/package/lambda-api) package
for routing requests.

```bash
npm install
```

## Testing the backend using SAM containers

JWT authorize functions do not run in SAM development environment. Because of this we've modified the filer_requests lambda
to populate JWT claims base on the contents of a file (local/dev-jwt-claims.json).  In order to test permissions you should
copy the sample file from events/dev-jwt-claims.json to your aws/local folder prior to starting the development aws gateway
environment.

Use the following command to run an instance of the command locally.

```shell
npm run start-api
```

### Testing JWT authorization locally

The sam local environment does not invoke the authorizers that are defined in the SAM template, so if you want to test
the JWT decoding process you need to do that by manually invoking the lambda.  Use events/authorize.json as event as an example,
but create a local copy and modify the JWT authorizer token in the payload.

```bash
sam local invoke JwtAuthorizeFunction --event local/authorize.json --env-vars local/env.json
```

## Register the Lambda url

As described above, since the Rest API associated with this end point may move, relevant locations of the items are stored
in the wapdc_settings.  As part of the deployment and/or cloning processes we will invoke the registerEndpoints  lambda to register the
urls and configurations for the REST apis in the wapdc_settings table in the RDS.

To test the function responsible for registering this url:
```bash
sam local invoke RegisterGatewayURLFunction --event local/authorize.json --env-vars local/env.json
```
